import { mongooseUnixTimestamp } from '../../src';

describe('Mongoose unix timestamp property constructor', () => {
  it('Should create object with descired properties', () => {
    const now = Date.now();
    const expectedResult = {
      default: now,
      index: false,
      required: true,
      type: Number,
    };

    const createdObject = mongooseUnixTimestamp(true, now);

    expect(createdObject).toEqual(expectedResult);
  });
});
