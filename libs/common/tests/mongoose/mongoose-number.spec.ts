import { mongooseNumber } from '../../src';

describe('Mongoose number property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: 1337,
      index: false,
      max: Infinity,
      min: 0,
      required: true,
      type: Number,
      unique: false,
    };

    const createdObject = mongooseNumber(true, 0, Infinity, 1337);

    expect(createdObject).toEqual(expectedResult);
  });
});
