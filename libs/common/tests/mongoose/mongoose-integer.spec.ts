import { pick } from 'lodash';

import { mongooseInteger } from '../../src';

describe('Mongoose integer property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: 1337,
      index: false,
      max: Infinity,
      min: 0,
      required: true,
      type: Number,
      unique: false,
    };

    const createdObject = mongooseInteger(true, 0, Infinity, 1337);

    /* Just because of getters and setters references */
    expect(pick(createdObject, ['default', 'index', 'max', 'min', 'required', 'type', 'unique'])).toEqual(expectedResult);
  });
});
