import { values } from 'lodash';

import { Environment, mongooseEnum } from '../../src';

describe('Mongoose enum property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: null,
      enum: values(Environment),
      index: false,
      required: true,
      type: String,
      unique: false,
    };

    const createdObject = mongooseEnum(true, values(Environment));

    expect(createdObject).toEqual(expectedResult);
  });
});
