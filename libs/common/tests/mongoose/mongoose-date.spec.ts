import { mongooseDate } from '../../src';

describe('Mongoose date property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: Date.now,
      index: false,
      required: true,
      type: Date,
    };

    const createdObject = mongooseDate();

    expect(createdObject).toEqual(expectedResult);
  });
});
