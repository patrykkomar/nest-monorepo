import { mongooseBoolean } from '../../src';

describe('Mongoose boolean property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: false,
      index: false,
      required: true,
      type: Boolean,
    };

    const createdObject = mongooseBoolean(true, false, false);

    expect(createdObject).toEqual(expectedResult);
  });
});
