import { jwtRegex, mongooseString } from '../../src';

describe('Mongoose string property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: null,
      index: false,
      match: jwtRegex,
      required: true,
      trim: true,
      type: String,
      unique: false,
    };

    const createdObject = mongooseString(true, jwtRegex, false, false, true);

    expect(createdObject).toEqual(expectedResult);
  });
});
