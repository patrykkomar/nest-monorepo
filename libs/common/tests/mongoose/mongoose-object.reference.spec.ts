import { Types } from 'mongoose';

import { mongooseObjectReference } from '../../src';

describe('Mongoose object reference property constructor', () => {
  it('Should create object with descired properties', () => {
    const expectedResult = {
      default: null,
      index: true,
      required: true,
      type: Types.ObjectId,
      unique: true,
    };

    const createdObject = mongooseObjectReference(true, true, true);

    expect(createdObject).toEqual(expectedResult);
  });
});
