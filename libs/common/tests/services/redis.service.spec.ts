import { Environment, RedisService, setEnvironment } from '../../src';

describe('RedisService', () => {
  setEnvironment(Environment.TEST);
  const redisService = new RedisService({ host: 'localhost', port: 6379 });

  it('Should have all desired operations defined', () => {
    const operations = ['get', 'set', 'lpush', 'rpush', 'lrange'];

    operations.forEach(operation => expect(redisService[operation]).toBeInstanceOf(Function));
  });

  it('Should properly generate set options', () => {
    const optionsObject = {
      expireAfterSeconds: 10,
      newOnly: true,
      overwriteOnly: false,
    };
    const expectedOptions = ['EX', 10, 'NX'];

    const setOptions = redisService.generateSetOptions(optionsObject);

    expect(setOptions).toEqual(expectedOptions);
  });
});
