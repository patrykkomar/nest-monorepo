import { Logger } from '../../src';

describe('Logger', () => {
  const logger = new Logger();

  it('Should have all five levels defined', () => {
    const levels = ['debug', 'info', 'warning', 'error', 'critical'];

    levels.forEach(level => {
      expect(logger[level]).toBeInstanceOf(Function);
    });
  });
});
