import { every } from 'lodash';

import { jwtRegex, mongoIdRegex } from '../../src';

describe('Regular expressions', () => {
  it('Should recognize valid JWTs', () => {
    const validJwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZW50ZW5jZSI6IkkgYW0gcG90YXRvISJ9.WMKFh8yYrmTrUNX0T87S91L6CihXt8N_IssvRbnQjeo';
    const invalidJwts = [
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJzZW50ZW5jZSI6IkkgYW0gcG90YXRvISJ9WMKFh8yYrmTrUNX0T87S91L6CihXt8N_IssvRbnQjeo',
      'zażółć.gęślą.jaźń',
      'https://www.google.com',
      '',
      false,
      null
    ];

    expect(validJwt).toMatch(jwtRegex);
    expect(every(invalidJwts, invalidJwt => jwtRegex.test(invalidJwt) === false)).toEqual(true);
  });

  it('Should recognize valid Mongo IDs', () => {
    const validMongoId = '5e539be8d9f7d81895dd4e64';
    const invalidMongoIds = [
      '5e539be8d9f7d81895dd4e6',
      '5e539be8d9f7d81895dd4e640',
      '5g539be8d9f7d81895dd4e64',
      '5e539be8d9-7d81895dd4e64',
    ];

    expect(validMongoId).toMatch(mongoIdRegex);
    expect(every(invalidMongoIds, invalidJwt => mongoIdRegex.test(invalidJwt) === false)).toEqual(true);
  });
});
