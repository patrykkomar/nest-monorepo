import { Environment, isDevelopmentEnvironment, isProductionEnvironment, isTestEnvironment, setEnvironment } from '../../src';

describe('Environment management functions', () => {
  it('Should set development environment', () => {
    setEnvironment(Environment.DEVELOPMENT);

    expect(isDevelopmentEnvironment()).toEqual(true);
    expect(isProductionEnvironment()).toEqual(false);
    expect(isTestEnvironment()).toEqual(false);
  });

  it('Should set production environment', () => {
    setEnvironment(Environment.PRODUCTION);

    expect(isDevelopmentEnvironment()).toEqual(false);
    expect(isProductionEnvironment()).toEqual(true);
    expect(isTestEnvironment()).toEqual(false);
  });

  it('Should set test environment', () => {
    setEnvironment(Environment.TEST);

    expect(isDevelopmentEnvironment()).toEqual(false);
    expect(isProductionEnvironment()).toEqual(false);
    expect(isTestEnvironment()).toEqual(true);
  });
});
