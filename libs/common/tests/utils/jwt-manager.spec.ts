import { get } from 'lodash';

import { JwtManager, jwtRegex } from '../../src';

describe('JwtManager', () => {
  const jwtManager = new JwtManager();

  const payload = { sentence: 'I am potato!' };
  const secret = 'Secret';
  const fakeSecret = 'Fake secret';

  it('Should generate signed token', () => {
    const signedToken = jwtManager.generateToken(payload, secret);

    expect(signedToken).toMatch(jwtRegex);
  });

  it('Should generate signed token and be able to verify it correctly', () => {
    const signedToken = jwtManager.generateToken(payload, secret);

    const isValidWithSecret = jwtManager.verifyToken(signedToken, secret);
    const isValidWithFakeSecret = jwtManager.verifyToken(signedToken, fakeSecret);

    expect(isValidWithSecret).toEqual(true);
    expect(isValidWithFakeSecret).toEqual(false);
  });

  it('Should generate signed token and be able to decode its payload', () => {
    const signedToken = jwtManager.generateToken(payload, secret);

    const decodedJwt = jwtManager.decodePayload(signedToken);

    expect(get(decodedJwt, 'sentence')).toEqual('I am potato!');
  });

  it('Should be able to compare JWT payload and request body', () => {
    const signedToken = jwtManager.generateToken(payload, secret);

    const validComparisonResult = jwtManager.comparePayloadWithRequestBody(signedToken, ['sentence'], { sentence: 'I am potato!' });
    const invalidComparisonResult = jwtManager.comparePayloadWithRequestBody(signedToken, ['sentence'], { sentence: 'Kocham bekon!' });

    expect(validComparisonResult).toEqual(true);
    expect(invalidComparisonResult).toEqual(false);
  });
});
