import { AbstractWorker } from '../../src';

class CountingWorker extends AbstractWorker {
  private counter: number;

  public constructor(delay: number) {
    super(delay);
    this.counter = 0;
  }

  public work() {
    this.counter += 1;
  }

  public getCounter(): number {
    return this.counter;
  }
}

describe('AbstractWorker', () => {
  const interval = 500;
  const worker = new CountingWorker(interval);

  it('Should work in desired intervals', async () => {
    worker.start();
    await new Promise(resolve => setTimeout(resolve, 5 * interval + 100));
    worker.stop();

    expect(worker.getCounter()).toEqual(5);
  });
});
