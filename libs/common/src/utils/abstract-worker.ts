export abstract class AbstractWorker {
  protected enabled: boolean;
  protected nextDelay: number;

  protected constructor(nextDelay: number) {
    this.enabled = true;
    this.nextDelay = nextDelay;
  }

  protected async wait(time: number) {
    await new Promise(resolve => setTimeout(resolve, time));
  }

  abstract async work();

  public async start(): Promise<void> {
    while (this.enabled) {
      await this.wait(this.nextDelay);
      await this.work();
    }

    return;
  }

  public stop(): void {
    this.enabled = false;
  }

  public sleep(sleepTime: number): void {
    this.stop();
    setTimeout(() => {
      this.enabled = true;
      this.start();
    }, sleepTime);
  }
}
