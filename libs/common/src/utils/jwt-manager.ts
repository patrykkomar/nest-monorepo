import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as jws from 'jws';
import { every, get } from 'lodash';
import * as moment from 'moment';

interface JwtGenerationOptions {
  expiresIn: number;
}

@Injectable()
export class JwtManager {
  decodePayload(token: string): unknown {
    const { payload } = jws.decode(token);

    return payload;
  }

  generateToken(payload: object, secret?: string, options?: JwtGenerationOptions): string {
    return jwt.sign(payload, secret, options);
  }

  verifyToken(token: string, secret: string): boolean {
    const decodedJwt = jws.decode(token);

    if (!decodedJwt) {
      return false;
    }

    const expired = moment().unix() > decodedJwt.payload.exp;
    const algorithm = get(decodedJwt, 'header.alg', 'none').toString().toUpperCase();
    if (expired || algorithm === 'none') {
      return false;
    }

    return jws.verify(token, algorithm, secret);
  }

  comparePayloadWithRequestBody(token: string, properties: string[], requestBody: object) {
    const { payload } = jws.decode(token);

    return every(properties, property => get(payload, property) === get(requestBody, property));
  }
}
