import { Environment } from '..';

export function setEnvironment(environment: Environment): void {
  process.env.environment = environment;
}

export function isDevelopmentEnvironment(environment: string = process.env.environment): boolean {
  return environment === Environment.DEVELOPMENT;
}

export function isProductionEnvironment(environment: string = process.env.environment): boolean {
  return environment === Environment.PRODUCTION;
}

export function isTestEnvironment(environment: string = process.env.environment): boolean {
  return environment === Environment.TEST;
}
