import * as moment from 'moment';
import * as winston from 'winston';

interface ILogger {
  debug(message: string, jsonPayload?: object);
  info(message: string, jsonPayload?: object);
  warning(message: string, jsonPayload?: object);
  error(message: string, jsonPayload?: object);
  critical(message: string, jsonPayload?: object);
}

const winstonLevels = {
  colors: {
    debug: 'blue',
    info: 'green',
    warning: 'yellow',
    error: 'red',
    critical: 'cyan',
  },
  levels: {
    debug: 7,
    info: 6,
    warning: 4,
    error: 3,
    critical: 2,
  },
};

function generateCustomConsoleTransport() {
  return new winston.transports.Console({
    format: winston.format.combine(
      winston.format.align(),
      winston.format.colorize(),
      winston.format.json(),
      winston.format.timestamp(),
      winston.format.printf(info => {
        let message = info.message;
        if (info.jsonPayload) {
          message = `${message}             ${JSON.stringify(info.jsonPayload)}`;
        }

        return `${moment(info.timestamp).format('DD.MM.YYYY, HH:mm:ss')}    ${message}     [${info.level}]`;
      }),
    ),
  });
}

export class Logger implements ILogger {
  private logger;

  constructor() {
    this.logger = winston.createLogger({
      format: winston.format.json(),
      level: 'debug',
      levels: winstonLevels.levels,
      transports: [
        generateCustomConsoleTransport(),
      ],
    });
    winston.addColors(winstonLevels.colors);
  }

  public critical(message: string, jsonPayload?: object) {
    this.logger.log('critical', { message, jsonPayload });
  }

  public error(message: string, jsonPayload?: object) {
    this.logger.log('error', { message, jsonPayload });
  }

  public warning(message: string, jsonPayload?: object) {
    this.logger.log('warning', { message, jsonPayload });
  }

  public info(message: string, jsonPayload?: object) {
    this.logger.log('info', { message, jsonPayload });
  }

  public debug(message: string, jsonPayload?: object) {
    this.logger.log('debug', { message, jsonPayload });
  }
}
