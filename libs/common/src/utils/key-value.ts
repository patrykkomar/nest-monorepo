export interface KeyValue {
  [key: string]: unknown;
}
