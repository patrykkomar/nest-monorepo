interface MongooseDate {
  type: DateConstructor;
  required: boolean;
  default: unknown;
  index: boolean;
}

export function mongooseDate(required = true, defaultValue = Date.now, index = false): MongooseDate {
  return {
    type: Date,
    required,
    default: defaultValue,
    index,
  };
}
