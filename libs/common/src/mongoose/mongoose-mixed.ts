import { Mixed } from 'mongoose';

interface MongooseMixed {
  type: unknown;
  required: boolean;
  default: unknown;
  index: boolean;
}

export function mongooseMixed(required = true, defaultValue = [], index = false): MongooseMixed {
  return {
    type: Mixed,
    required,
    default: defaultValue,
    index,
  };
}
