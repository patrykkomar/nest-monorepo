interface MongooseArray<T> {
  type: [T];
  required: boolean;
  default: (string | number)[];
  index: boolean;
}

export function mongooseArray(type: unknown, required = true, defaultValue = [], index = false): MongooseArray<unknown> {
  return {
    type: [type],
    required,
    default: defaultValue,
    index,
  };
}
