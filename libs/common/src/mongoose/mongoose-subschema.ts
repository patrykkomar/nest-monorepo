import { Schema } from 'mongoose';

interface MongooseSubschema {
  type: Schema;
  required: boolean;
}

export function mongooseSubschema(subschema: Schema, required = true): MongooseSubschema {
  return {
    type: subschema,
    required,
  };
}
