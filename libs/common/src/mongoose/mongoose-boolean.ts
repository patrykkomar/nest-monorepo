interface MongooseBoolean {
  type: BooleanConstructor;
  required: boolean;
  default: boolean;
  index: boolean;
}

export function mongooseBoolean(required = true, defaultValue = false, index = false): MongooseBoolean {
  return {
    type: Boolean,
    required,
    default: defaultValue,
    index,
  };
}
