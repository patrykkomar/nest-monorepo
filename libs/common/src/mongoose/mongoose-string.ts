interface MongooseString {
  type: StringConstructor;
  required: boolean;
  match: RegExp;
  default: string;
  index: boolean;
  unique: boolean;
  trim: boolean;
}

export function mongooseString(required = true, match = /^.*$/, index = false, unique = false, trim = true): MongooseString {
  return {
    type: String,
    required,
    match,
    default: null,
    index,
    unique,
    trim,
  };
}
