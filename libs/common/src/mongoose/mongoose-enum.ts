interface MongooseEnum {
  type: StringConstructor;
  required: boolean;
  enum: string[];
  index: boolean;
  unique: boolean;
  default: string;
}

/* tslint:disable-next-line:max-line-length */
export function mongooseEnum(required = true, enumValues: string[] = [], defaultValue = null, index = false): MongooseEnum {
  return {
    type: String,
    required,
    enum: enumValues,
    index,
    unique: false,
    default: defaultValue,
  };
}
