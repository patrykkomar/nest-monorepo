import { Types } from 'mongoose';

interface MongooseObjectReference {
  type: Types;
  required: boolean;
  index: boolean;
  unique: boolean;
  default: Types.ObjectId;
}

export function mongooseObjectReference(required = true, index = false, unique = false): MongooseObjectReference {
  return {
    type: Types.ObjectId,
    required,
    index,
    unique,
    default: null,
  };
}
