interface MongooseInteger {
  type: NumberConstructor;
  required: boolean;
  index: boolean;
  unique: boolean;
  min: number;
  max: number;
  default: number;
  get(value: number): number;
  set(value: number): number;
}

/* tslint:disable-next-line:max-line-length */
export function mongooseInteger(required = true, min = 0, max = Infinity, defaultValue = 0, index = false, unique = false): MongooseInteger {
  return {
    type: Number,
    required,
    index,
    unique,
    min,
    max,
    default: defaultValue,
    get: value => Math.round(value),
    set: value => Math.round(value),
  };
}
