interface MongooseNumber {
  type: NumberConstructor;
  required: boolean;
  index: boolean;
  unique: boolean;
  min: number;
  max: number;
  default: number;
}

/* tslint:disable-next-line:max-line-length */
export function mongooseNumber(required = true, min = 0, max = Infinity, defaultValue = 0, index = false, unique = false): MongooseNumber {
  return {
    type: Number,
    required,
    index,
    unique,
    min,
    max,
    default: defaultValue,
  };
}
