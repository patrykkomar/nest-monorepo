interface MongooseUnixTimestamp {
  type: NumberConstructor;
  required: boolean;
  default: number;
  index: boolean;
}

export function mongooseUnixTimestamp(required = true, defaultValue = Math.floor(Date.now() / 1000), index = false): MongooseUnixTimestamp {
  return {
    type: Number,
    required,
    default: defaultValue,
    index,
  };
}
