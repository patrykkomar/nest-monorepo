interface MongooseEnumArray {
  type: [StringConstructor];
  enum: string[],
  required: boolean;
  default: (string | number)[];
  index: boolean;
}

export function mongooseEnumArray(enumValues: string[], required = true, defaultValue = [], index = false): MongooseEnumArray {
  return {
    type: [String],
    enum: enumValues,
    required,
    default: defaultValue,
    index,
  };
}
