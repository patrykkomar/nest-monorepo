export const mongooseSchemaOptions = {
  skipVersioning: true,
  timestamps: false,
  useNestedStrict: true,
  versionKey: false,
};
