import * as Redis from 'ioredis';
import { isNil } from 'lodash';

import { Environment } from '../';

interface RegisConfig {
  host: string;
  port: number;
}

interface RedisInstance {
  get(key: string): Promise<unknown>;
  set(key: string, value: unknown, ...options: unknown[]): Promise<void>;
  del(key: string): Promise<void>;
  lpush(key: string, value: unknown): Promise<void>;
  rpush(key: string, value: unknown): Promise<void>;
  lrange(key: string, startIndex: number, stopIndex: number): Promise<unknown[]>
  ttl(key: string): Promise<number>;
  expire(key: string, seconds: number): Promise<void>;
}

interface RedisSetOptions {
  expireAfterSeconds?: number;
  newOnly?: boolean;
  overwriteOnly?: boolean;
}

export class RedisService {
  redisInstance: RedisInstance;

  constructor({ host, port }: RegisConfig) {
    if (process.env.environment !== Environment.TEST) {
      this.redisInstance = new Redis({
        host,
        port,
      });
    }
  }

  generateSetOptions(redisSetOptions: RedisSetOptions): unknown[] {
    const { expireAfterSeconds, newOnly, overwriteOnly } = redisSetOptions;
    const options = [];

    if (!isNil(expireAfterSeconds)) {
      options.push('EX');
      options.push(expireAfterSeconds);
    }

    if (newOnly) {
      options.push('NX');
    }

    if (overwriteOnly) {
      options.push('XX');
    }

    return options;
  }

  async get(key: string): Promise<unknown> {
    return this.redisInstance.get(key);
  }

  async set(key: string, value: unknown, options: RedisSetOptions = {}): Promise<void> {
    await this.redisInstance.set(key, value, ...this.generateSetOptions(options));
  }

  async del(key: string): Promise<void> {
    await this.redisInstance.del(key);
  }

  async lpush(key: string, value: unknown): Promise<void> {
    await this.redisInstance.lpush(key, value);
  }

  async rpush(key: string, value: unknown): Promise<void> {
    await this.redisInstance.rpush(key, value);
  }

  async lrange(key: string, startIndex = 0, stopIndex = -1): Promise<unknown[]> {
    return this.redisInstance.lrange(key, startIndex, stopIndex);
  }

  async ttl(key: string): Promise<number> {
    return this.redisInstance.ttl(key);
  }

  async expire(key: string, seconds: number): Promise<void> {
    return this.redisInstance.expire(key, seconds);
  }
}
