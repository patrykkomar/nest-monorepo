import { DashReplacementPipe } from '../../src';

describe('DashReplacementPipe', () => {
  it('Should defaultly use whitespace in order to replace dashes', () => {
    const dashReplacementPipe = new DashReplacementPipe();
    const valuesBeforeTransformation = [
      'parameter',
      'single-dash',
      'multiple-damn-dashes',
    ];
    const expectedValuesAfterTransformation = [
      'parameter',
      'single dash',
      'multiple damn dashes',
    ];

    const valuesAfterTransformation = valuesBeforeTransformation.map(value => dashReplacementPipe.transform(value));

    expect(valuesAfterTransformation).toEqual(expectedValuesAfterTransformation);
  });

  it('Should use desired character in order to replace dashes when it\'s provided', () => {
    const dashReplacementPipe = new DashReplacementPipe('*');
    const valuesBeforeTransformation = [
      'parameter',
      'single-dash',
      'multiple-damn-dashes',
    ];
    const expectedValuesAfterTransformation = [
      'parameter',
      'single*dash',
      'multiple*damn*dashes',
    ];

    const valuesAfterTransformation = valuesBeforeTransformation.map(value => dashReplacementPipe.transform(value));

    expect(valuesAfterTransformation).toEqual(expectedValuesAfterTransformation);
  });
});
