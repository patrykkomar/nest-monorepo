import { LowercasePipe } from '../../src';

describe('LowercasePipe', () => {
  const lowercasePipe = new LowercasePipe();

  it('Should lowercase string values', () => {
    const valuesBeforeTransformation = [
      'lowercased parameter',
      'UPPERCASED PARAMETER',
      'PoKeMoNcAsEd PaRaMeTeR',
    ];
    const expectedValuesAfterTransformation = [
      'lowercased parameter',
      'uppercased parameter',
      'pokemoncased parameter',
    ];

    const valuesAfterTransformation = valuesBeforeTransformation.map(value => lowercasePipe.transform(value));

    expect(valuesAfterTransformation).toEqual(expectedValuesAfterTransformation);
  });

  it('Shouldn\'t transfort values of other types', () => {
    const valuesBeforeTransformation = [
      undefined,
      true,
      1337,
      { message: 'I am potato!' },
      ['Hello, world!', {}, null],
    ];

    const valuesAfterTransformation = valuesBeforeTransformation.map(value => lowercasePipe.transform(value));

    expect(valuesAfterTransformation).toEqual(valuesBeforeTransformation);
  });
});
