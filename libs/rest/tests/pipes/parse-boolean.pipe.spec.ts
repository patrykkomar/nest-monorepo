import { ParseBooleanPipe } from '../../src';

describe('ParseBooleanPipe', () => {
  const parseBooleanPipe = new ParseBooleanPipe();

  it('Should parse \'true\' and \'1\' values to true', () => {
    const valuesBeforeTransformation = ['true', '1'];
    const expectedValuesAfterTransformation = [true, true];

    const valuesAfterTransformation = valuesBeforeTransformation.map(value => parseBooleanPipe.transform(value));

    expect(valuesAfterTransformation).toEqual(expectedValuesAfterTransformation);
  });

  it('Should parse \'false\' and \'0\' values to false', () => {
    const valuesBeforeTransformation = ['false', '0'];
    const expectedValuesAfterTransformation = [false, false];

    const valuesAfterTransformation = valuesBeforeTransformation.map(value => parseBooleanPipe.transform(value));

    expect(valuesAfterTransformation).toEqual(expectedValuesAfterTransformation);
  });

  it('Should parse empty value to null', () => {
    const valueBeforeTransformation = undefined;

    const valueAfterTransformation = parseBooleanPipe.transform(valueBeforeTransformation);

    expect(valueAfterTransformation).toEqual(null);
  });
});
