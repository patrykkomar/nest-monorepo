import { Reflector } from '@nestjs/core';

import { JwtManager } from '@nestjs-monorepo/common';

import { JwtSignatureGuard } from '../../src';

describe('JwtSignatureGuard', () => {
  const jwtManager = new JwtManager();
  const jwtSignatureGuard = new JwtSignatureGuard(new Reflector(), jwtManager);

  it('Should have canActivate method defined', () => {
    expect(jwtSignatureGuard.canActivate).toBeInstanceOf(Function);
  });

  it('Should verify token signature', () => {
    const secret = 'Supersecret';
    const authorization = jwtManager.generateToken({ message: 'I am potato!' }, secret);

    const verificationResult = jwtSignatureGuard.isJwtValid(authorization, secret);

    expect(verificationResult).toEqual(true);
  });
});
