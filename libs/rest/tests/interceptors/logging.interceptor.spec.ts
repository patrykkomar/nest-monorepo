import { Logger } from '@nestjs-monorepo/common';

import { LoggingInterceptor } from '../../src';

describe('LoggingInterceptor', () => {
  const logger = new Logger();
  logger.info = jest.fn();

  const loggingInterceptor = new LoggingInterceptor(logger);

  it('Should have intercept method defined', () => {
    expect(loggingInterceptor.intercept).toBeInstanceOf(Function);
  });

  it('Should call logger while intercepting HTTP request', () => {
    loggingInterceptor.logHttpRequest({ method: 'GET', originalUrl: '', startTimestamp: 1337, statusCode: 200 });

    expect(logger.info).toHaveBeenCalledTimes(1);
  });
});
