import { ResponseMappingInterceptor } from '../../src';

describe('ResponseMappingInterceptor', () => {
  const responseMappingInterceptor = new ResponseMappingInterceptor();

  it('Should have intercept method defined', () => {
    expect(responseMappingInterceptor.intercept).toBeInstanceOf(Function);
  });

  it('Should correctly format response with boolean value', () => {
    const response = true;
    const expectedResult = { success: true };

    const mappingResult = responseMappingInterceptor.formatResponse(response);

    expect(mappingResult).toEqual(expectedResult);
  });

  it('Should correctly format response with string value', () => {
    const response = 'Hello, world!';
    const expectedResult = { result: 'Hello, world!' };

    const mappingResult = responseMappingInterceptor.formatResponse(response);

    expect(mappingResult).toEqual(expectedResult);
  });

  it('Should correctly format response with JWT', () => {
    const response = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZW50ZW5jZSI6IkkgYW0gcG90YXRvISJ9.WMKFh8yYrmTrUNX0T87S91L6CihXt8N_IssvRbnQjeo';
    const expectedResult = { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZW50ZW5jZSI6IkkgYW0gcG90YXRvISJ9.WMKFh8yYrmTrUNX0T87S91L6CihXt8N_IssvRbnQjeo' };

    const mappingResult = responseMappingInterceptor.formatResponse(response);

    expect(mappingResult).toEqual(expectedResult);
  });

  it('Should correctly format response with object value', () => {
    const response = { message: 'I am potato!' };
    const expectedResult = { message: 'I am potato!' };

    const mappingResult = responseMappingInterceptor.formatResponse(response);

    expect(mappingResult).toEqual(expectedResult);
  });
});
