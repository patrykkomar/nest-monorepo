import { SetMetadata } from '@nestjs/common';

export function UseJwtSecret(jwtSecret: string) {
  return SetMetadata('secret', jwtSecret);
}
