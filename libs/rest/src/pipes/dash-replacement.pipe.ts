import { PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class DashReplacementPipe implements PipeTransform<string, string> {
  private replacementCharacter: string;

  constructor(replacementCharacter?: string) {
    this.replacementCharacter = replacementCharacter || ' ';
  }

  transform(value: string): string {
    return typeof value === 'string' ? value.replace(/-/g, this.replacementCharacter) : value;
  }
}
