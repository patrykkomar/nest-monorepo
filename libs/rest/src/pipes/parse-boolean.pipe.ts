import { PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class ParseBooleanPipe implements PipeTransform<unknown, unknown> {
  transform(value: unknown): boolean | null {
    if (value === undefined) return null;
    else if (value === 'true' || value.toString() === '1') return true
    else if (value === 'false' || value.toString() === '0') return false;
    else return null;
  }
}
