import { PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class LowercasePipe implements PipeTransform<unknown, unknown> {
  transform(value: unknown): unknown {
    return typeof value === 'string' ? value.toLowerCase() : value;
  }
}
