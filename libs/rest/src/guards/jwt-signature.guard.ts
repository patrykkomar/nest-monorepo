import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { JwtManager, jwtRegex } from '@nestjs-monorepo/common';

@Injectable()
export class JwtSignatureGuard implements CanActivate {
  constructor(private readonly reflector: Reflector, private readonly jwtManager: JwtManager) {}

  canActivate(context: ExecutionContext): boolean {
    const secret = this.reflector.get<string>('secret', context.getClass()) || this.reflector.get<string>('secret', context.getHandler());

    const { authorization } = context.switchToHttp().getRequest().headers;

    return this.isJwtValid(authorization, secret);
  }

  isJwtValid(authorization: string, secret: string): boolean {
    return jwtRegex.test(authorization) && this.jwtManager.verifyToken(authorization, secret);
  }
}
