import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
// import { get } from 'lodash';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Logger } from '@nestjs-monorepo/common';

interface HttpRequestEntry {
  method: string;
  originalUrl: string;
  startTimestamp: number;
  statusCode: number;
}

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(private logger: Logger) { }

  intercept(context: ExecutionContext, next: CallHandler): Observable<void> {
    const httpContext = context.switchToHttp();
    // const { connection, headers, method, path } = httpContext.getRequest();
    const { method, originalUrl } = httpContext.getRequest();
    const { statusCode } = httpContext.getResponse();
    /* FIXME: use this connection IP (now it points to ::1 so it makes no sens to use it ^^) */
    // const connectionIp = get(headers, 'x-forwarded-for') || get(connection, 'remoteAddress');

    const startTimestamp = Date.now();

    return next
      .handle()
      .pipe(
        tap(() => this.logHttpRequest({ method, originalUrl, startTimestamp, statusCode })),
      );
  }

  logHttpRequest(httpRequestEntry: HttpRequestEntry): void {
    const { method, originalUrl, startTimestamp, statusCode } = httpRequestEntry;

    this.logger.info(`${method}  ${originalUrl}  ${statusCode}  (${Date.now() - startTimestamp}ms)`)
  }
}
