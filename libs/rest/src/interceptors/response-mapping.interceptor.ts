import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { jwtRegex } from '@nestjs-monorepo/common';

interface Response<T> {
  data: T;
}

@Injectable()
export class ResponseMappingInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
    return next.handle().pipe(
      map(this.formatResponse as (response: unknown) => Response<T>),
    );
  }

  formatResponse(response: unknown): object {
    if (typeof response === 'object') {
      return response;
    }

    if (typeof response === 'boolean') {
      return { success: response };
    }

    return typeof response === 'string' && jwtRegex.test(response as string)
      ? { token: response }
      : { result: response };
  }
}
