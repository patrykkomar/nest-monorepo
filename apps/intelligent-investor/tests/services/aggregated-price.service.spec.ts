import { assign } from 'lodash';
import * as moment from 'moment';

import { AggregatedPriceService } from '../../src/modules/database/services';
import { AggregationPeriod, Asset, Class, Currency } from '../../src/shared/enums';
import { AggregatedPrice, SpotPrice } from '../../src/shared/interfaces';

describe('AggregatedPriceService', () => {
  const currentMomentDate = moment().subtract(1, 'day');
  const aggregationDate = {
    day: currentMomentDate.date(),
    month: currentMomentDate.month() + 1,
    year: currentMomentDate.year(),
  };
  const priceBase = {
    asset: Asset.SILVER,
    assetClass: Class.METALS,
    createdAt: new Date(),
    currency: Currency.USD,
  };
  const fakePrices = [
    {
      ...priceBase,
      price: 18.10,
    },
    {
      ...priceBase,
      price: 13.90,
    },
    {
      ...priceBase,
      price: 16.00,
    },
  ];

  function spotPriceModel(spotPrice: SpotPrice) {
    assign(this, spotPrice);
  }
  spotPriceModel.find = jest.fn();

  function aggregatedPriceModel(aggregatedPrice: AggregatedPrice) {
    assign(this, aggregatedPrice);
  }
  aggregatedPriceModel.findOne = jest.fn(() => null);

  const aggregatedPriceService = new AggregatedPriceService(spotPriceModel, aggregatedPriceModel);

  it('Should call findOne() method of aggregated price model while selecting aggregated price for specific asset and date', async () => {
    const hasBeenAggregatedAlready = await aggregatedPriceService.checkIfPriceHasBeenAggregatedAlready(Asset.GOLD, aggregationDate);

    expect(aggregatedPriceModel.findOne).toHaveBeenCalledWith({ asset: Asset.GOLD, date: { $eq: aggregationDate }});
    expect(hasBeenAggregatedAlready).toEqual(false);
  });

  it('Should call find() method of spot price model while selecting spot prices', async () => {
    const year = currentMomentDate.year();
    let month = (currentMomentDate.month() + 1).toString();
    if (month.length === 1) { month = `0${month}`; }
    let day = (currentMomentDate.date()).toString();
    if (day.length === 1) { day = `0${day}`; }
    const startDate = moment(`${year}-${month}-${day}T00:00:00.000`);
    const endDate = moment(`${year}-${month}-${day}T23:59:59.999`);

    await aggregatedPriceService.getSpotPricesByDay(Asset.GOLD, currentMomentDate);

    expect(spotPriceModel.find).toHaveBeenCalledWith({
      asset: Asset.GOLD,
      createdAt: {
        $gte: startDate,
        $lte: endDate,
      },
    });
  });

  it('Should correctly calculate aggregated price', async () => {
    const calculatedAggregatedPrice = await aggregatedPriceService.calculateAggregatedPrice(Asset.SILVER, aggregationDate, fakePrices);

    expect(calculatedAggregatedPrice).toEqual({
      asset: Asset.SILVER,
      assetClass: Class.METALS,
      currency: Currency.USD,
      date: aggregationDate,
      prices: {
        average: 16,
        closing: 16.00,
        maximum: 18.10,
        minimum: 13.90,
        opening: 18.10,
      },
    });
  });

  it('Should create correct aggregation date objects', () => {
    const yesterday = moment().subtract(1, 'day');
    const today = moment();
    const nextMonth = moment().add(1, 'month');
    const inputs = [
      { date: yesterday, period: AggregationPeriod.YEAR },
      { date: today, period: AggregationPeriod.MONTH },
      { date: nextMonth, period: AggregationPeriod.DAY },
    ];
    const expectedAggregationDates = [
      { year: yesterday.year() },
      { year: today.year(), month: today.month() + 1 },
      { year: nextMonth.year(), month: nextMonth.month() + 1, day: nextMonth.date() },
    ];

    const createdAggregationDates = inputs.map(({ date, period }) => aggregatedPriceService.getAggregationDate(period, date));

    expect(createdAggregationDates).toEqual(expectedAggregationDates);
  });
});
