import { assign, omit } from 'lodash';
import * as moment from 'moment';

import { SpotPriceService } from '../../src/modules/database/services';
import { Asset, Class, Currency } from '../../src/shared/enums';
import { SpotPrice } from '../../src/shared/interfaces';

describe('SpotPriceService', () => {
  const priceBase = {
    assetClass: Class.METALS,
    createdAt: new Date(),
    currency: Currency.USD,
  };
  const fakePrices = [
    {
      ...priceBase,
      asset: Asset.PALLADIUM,
      price: 2350.99,
    },
    {
      ...priceBase,
      asset: Asset.PLATINUM,
      price: 969.83,
    },
    {
      ...priceBase,
      asset: Asset.GOLD,
      price: 1648.70,
    },
    {
      ...priceBase,
      asset: Asset.SILVER,
      price: 18.26,
    },
    {
      ...priceBase,
      asset: Asset.SILVER,
      price: 18.27,
    },
    {
      asset: Asset.BRENT_OIL,
      assetClass: Class.COMMODITIES,
      currency: Currency.USD,
      createdAt: new Date(),
      price: 52.46,
    },
  ];

  function spotPriceModel(spotPrice: SpotPrice) {
    assign(this, spotPrice);
  }

  spotPriceModel.find = jest.fn(({ assetsClass }) => fakePrices.filter(price => price.assetClass === assetsClass));
  spotPriceModel.findOne = jest.fn(({ asset }) => fakePrices.find(price => price.asset === asset));
  spotPriceModel.insertMany = jest.fn();

  const spotPriceService = new SpotPriceService(spotPriceModel);

  const currentDate = moment();
  let month = (currentDate.month() + 1).toString();
  if (month.length === 1) month = `0${month}`;
  let day = (currentDate.date()).toString();
  if (day.length === 1) day = `0${day}`;
  const openingDate = moment(`${currentDate.year()}-${month}-${day}T00:00:00.000`);

  it('Should call findOne() method of spot price model while selecting spot price for single asset', async () => {
    const asset = Asset.GOLD;

    const selectedPrice = await spotPriceService.getRecentPriceForAsset(asset, true);

    expect(selectedPrice).toEqual(omit(fakePrices.find(price => price.asset === asset), 'assetClass'));
    expect(spotPriceModel.findOne).toHaveBeenCalledTimes(1);
    expect(spotPriceModel.findOne).toHaveBeenCalledWith({ asset, createdAt: { $gt: openingDate } }, {}, { sort: { createdAt: 1 } });
  });

  it('Should call find() method of spot price model while selecting spot prices for asset class', async () => {
    const assetClass = Class.METALS;

    const pricesForAssetClass = await spotPriceService.getRecentPricesForAssetClass(assetClass, true);

    expect(pricesForAssetClass.length).toEqual(4);
    expect(spotPriceModel.find).toHaveBeenCalledTimes(1);
    expect(spotPriceModel.find).toHaveBeenCalledWith({ assetClass, createdAt: { $gt: openingDate } });
  });

  it('Should call insertMany model method while saving multiple spot prices', async () => {
    await spotPriceService.saveMultipleSpotPrices(fakePrices);

    expect(spotPriceModel.insertMany).toHaveBeenCalledTimes(1);
    expect(spotPriceModel.insertMany).toHaveBeenCalledWith(fakePrices);
  });
});
