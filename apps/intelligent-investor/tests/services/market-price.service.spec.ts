import { assign } from 'lodash';

import { MarketPriceService } from '../../src/modules/database/services';
import { Asset, Class, Currency, Market } from '../../src/shared/enums';
import { MarketPrice } from '../../src/shared/interfaces';

describe('MarketPriceService', () => {
  const priceBase = {
    assetClass: Class.METALS,
    createdAt: new Date(),
    currency: Currency.USD,
    market: Market.METALE_LOKACYJNE,
  };
  const fakePrices = [
    {
      ...priceBase,
      asset: Asset.GOLD,
      name: 'Krugerrand (1 uncja srebra)',
      price: 1648.70,
    },
    {
      ...priceBase,
      asset: Asset.SILVER,
      name: 'Kanadyjski Liść Klonowy (1 uncja srebra)',
      price: 18.26,
    },
  ];

  function marketPriceModel(marketPrice: MarketPrice) {
    assign(this, marketPrice);
  }

  marketPriceModel.find = jest.fn(({ assetsClass }) => fakePrices.filter(price => price.assetClass === assetsClass));
  marketPriceModel.findOne = jest.fn(({ asset }) => fakePrices.find(price => price.asset === asset));
  marketPriceModel.insertMany = jest.fn();

  const marketPriceService = new MarketPriceService(marketPriceModel);

  it('Should call insertMany model method while saving multiple market prices', async () => {
    await marketPriceService.saveMultiplePrices(fakePrices);

    expect(marketPriceModel.insertMany).toHaveBeenCalledTimes(1);
    expect(marketPriceModel.insertMany).toHaveBeenCalledWith(fakePrices);
  });
});
