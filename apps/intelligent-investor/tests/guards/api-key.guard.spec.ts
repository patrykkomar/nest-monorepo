import { HttpException, HttpStatus } from '@nestjs/common';

import { JwtManager } from '@nestjs-monorepo/common';

import { ApiKeyGuard } from '../../src/modules/api/guards/api-key.guard';
import { config } from '../../src/setup/config';

describe('ApiKeyGuard', () => {
  const jwtManager = new JwtManager();
  const apiKeyGuard = new ApiKeyGuard(jwtManager);

  it('Should generate correct connection Redis key', () => {
    const remoteAddress = '127.0.0.1';

    const connectionKey = apiKeyGuard.generateConnectionKey(remoteAddress);

    expect(connectionKey).toEqual('connection-127.0.0.1');
  });

  it('Should properly throw 429', async () => {
    expect(() => {
      apiKeyGuard.throwTooManyRequests(true);
    }).toThrow(new HttpException(`You exceeded requests limit equal 3 requests per minute.`, HttpStatus.TOO_MANY_REQUESTS));
    expect(() => {
      apiKeyGuard.throwTooManyRequests(false);
    }).toThrow(new HttpException(`You exceeded requests limit equal 10 requests per minute.`, HttpStatus.TOO_MANY_REQUESTS));
  });

  it('Should properly validate JWT', () => {
    const apiKey = 'My API key';
    const validJwt = jwtManager.generateToken({ apiKey }, config.jwt.secret);
    const invalidJwt = jwtManager.generateToken({ apiKey: 'Another API key' }, config.jwt.secret);
    const anotherInvalidJwt = jwtManager.generateToken({ apiKey }, 'Fake secret');

    expect(apiKeyGuard.isTokenValid(validJwt, apiKey)).toEqual(true);
    expect(apiKeyGuard.isTokenValid(invalidJwt, apiKey)).toEqual(false);
    expect(apiKeyGuard.isTokenValid(anotherInvalidJwt, apiKey)).toEqual(false);
  });
});
