import { keys, values } from 'lodash';
import * as moment from 'moment';

import { AggregationPeriod, Asset, Class, Market } from '../../src/shared/enums';
import { assetsClassesMapping, getAssetClass, isDateTodayOrLater, marketsValueParsers, parseAggregationDate, isWeekend } from '../../src/shared/helpers';

describe('Helpers', () => {
  it('Should recognize assets classes', () => {
    const assets = [
      Asset.GOLD,
      Asset.SILVER,
      Asset.PLATINUM,
      Asset.PALLADIUM,
      Asset.CRUDE_OIL_WTI,
      Asset.BRENT_OIL,
      Asset.NATURAL_GAS,
      Asset.HEATING_OIL,
      Asset.GASOLINE,
      Asset.BITCOIN,
      Asset.ETHEREUM,
      Asset.LITECOIN,
      Asset.RIPPLE,
      Asset.EOS,
      Asset.NEO,
    ];
    const expectedClasses = [
      Class.METALS,
      Class.METALS,
      Class.METALS,
      Class.METALS,
      Class.COMMODITIES,
      Class.COMMODITIES,
      Class.COMMODITIES,
      Class.COMMODITIES,
      Class.COMMODITIES,
      Class.CRYPTO,
      Class.CRYPTO,
      Class.CRYPTO,
      Class.CRYPTO,
      Class.CRYPTO,
      Class.CRYPTO,
    ];

    const result = assets.map(getAssetClass);

    expect(result).toEqual(expectedClasses);
    expect(result.length).toEqual(values(Asset).length);
  });

  it('Should correctly parse received prices considering requested market', () => {
    const prices = {
      [Market.BINANCE]: [2.34, 84.00, 11299.99],
      [Market.INVESTING]: ['2.34', '84.00', '11,299.99'],
      [Market.MENNICA_SKARBOWA]: ['2,34 zł', '84,00 zł', '11299,99 zł'],
      [Market.METALE_LOKACYJNE]: ['2,34 zł', '84,00 zł', '11 299,99 zł'],
    };
    const exprectedPrices = {
      [Market.BINANCE]: [2.34, 84.00, 11299.99],
      [Market.INVESTING]: [2.34, 84, 11299.99],
      [Market.MENNICA_SKARBOWA]: [2.34, 84, 11299.99],
      [Market.METALE_LOKACYJNE]: [2.34, 84, 11299.99],
    };

    const parsedPrices = {};
    keys(prices).forEach(market => {
      parsedPrices[market] = prices[market].map(marketsValueParsers[market]);
    });

    expect(keys(marketsValueParsers)).toEqual(values(Market));
    expect(parsedPrices).toEqual(exprectedPrices);
  });

  it('Should list all assets from specific class', () => {
    const classes = [
      Class.METALS,
      Class.COMMODITIES,
      Class.CRYPTO,
    ];

    const assets = classes.map(assetsClass => assetsClassesMapping[assetsClass]);
    const assetsNumber = assets.reduce((acc, curr) => acc + curr.length, 0);

    expect(assetsNumber).toEqual(values(Asset).length);
  });

  it('Should correctly parse aggregation dates', () => {
    const inputs = [
      { year: 2010, month: '12' },
      { year: '1996', month: '11', day: '6' },
    ]
    const expectedResults = [
      { year: 2010, month: 12 },
      { year: 1996, month: 11, day: 6 },
    ];

    const parsedDates = inputs.map(parseAggregationDate);

    expect(parsedDates).toEqual(expectedResults);
  });

  it('Should recognize dates from the future', () => {
    const yesterday = moment().subtract(1, 'day');
    const today = moment();
    const tomorrow = moment().add(1, 'day');
    const aggregationDates = [
      { year: yesterday.year(), month: yesterday.month() + 1, day: yesterday.date() },
      { year: yesterday.year(), month: yesterday.month() + 1, day: yesterday.date() },
      { year: today.year() },
      { year: today.year(), month: today.month() + 1 },
      { year: today.year(), month: today.month() + 1, day: today.date() },
      { year: tomorrow.year(), month: tomorrow.month() + 1, day: tomorrow.date() },
    ]
    const expectedResults = [false, false, true, true, true, true];

    const areDatesFromTheFuture = aggregationDates.map(isDateTodayOrLater);

    expect(areDatesFromTheFuture).toEqual(expectedResults);
  });

  it('Should recognize weekend by date', () => {
    const workingDays = [
      moment().year(1996).month(10).date(6),
      moment().year(1967).month(2).date(29),
      moment().year(1965).month(1).date(10),
      moment().year(1969).month(6).date(24),
      moment().year(1879).month(2).date(14),
      moment().year(1985).month(1).date(5),
      moment().year(1997).month(11).date(8),
    ];
    const weekendDays = [
      moment().year(1988).month(2).date(13),
      moment().year(1996).month(8).date(22),
      moment().year(1945).month(8).date(1),
    ];
    const expectedResultsForWorkindDays = Array(workingDays.length).fill(false);
    const expectedResultsForWeekendDays = Array(weekendDays.length).fill(true);

    const resultsForWorkindDays = workingDays.map(isWeekend);
    const resultsForWeekendDays = weekendDays.map(isWeekend);

    expect(resultsForWorkindDays).toEqual(expectedResultsForWorkindDays);
    expect(resultsForWeekendDays).toEqual(expectedResultsForWeekendDays);
  });
});
