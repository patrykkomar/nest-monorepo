import { MarketWorker } from '../../src/modules/market-price-monitor/setup/market-worker';
import { MarketPriceService } from '../../src/modules/database/services';
import { MARKET_PRICE } from '../../src/shared/constants';
import { Asset, Class, Currency, DataSource, Market } from '../../src/shared/enums';
import { MarketWorkerProduct } from '../../src/shared/interfaces';

describe('MarketWorker', () => {
  let marketPriceService: MarketPriceService;
  let marketWorker: MarketWorker;
  const basicMarketWorkerOptions = {
    assetClass: Class.METALS,
    currency: Currency.USD,
    longDelay: null,
    market: Market.METALE_LOKACYJNE,
    normalDelay: null,
    shortDelay: null,
    url: null,
  };
  const goldName = 'Krugerrand (1 uncja złota)';
  const silverName = 'Kanadyjski Liść Klonowy (1 uncja srebra)';
  const url = null;
  const goldProduct = {
    name: goldName,
    url,
  };
  const silverProduct = {
    name: silverName,
    url,
  };
  const expectedGoldPriceObject = { name: goldName, price: 2020.45 };
  const expectedSilverPriceObject = { name: silverName, price: 25.39 };

  beforeAll(() => {
    marketPriceService = new MarketPriceService(MARKET_PRICE);
  });

  it('Should get correct price from JSON API response', async () => {
    marketWorker = new MarketWorker({
      ...basicMarketWorkerOptions,
      apiDataSelector: 'metalPrice',
      dataSource: DataSource.API_OBJECT,
      assets: [{
        asset: Asset.GOLD,
        products: [goldProduct],
      }, {
        asset: Asset.SILVER,
        products: [silverProduct],
      }],
    }, marketPriceService);
    marketWorker.performRequest = jest.fn(async (product: MarketWorkerProduct) => ({ data: { metalPrice: product.name === goldName ? '2020,45 zł' : '25,39 zł' }}));

    const goldResult = await marketWorker.getSpecificProductPrice(goldProduct);
    const silverResult = await marketWorker.getSpecificProductPrice(silverProduct);

    expect(goldResult).toEqual(expectedGoldPriceObject);
    expect(silverResult).toEqual(expectedSilverPriceObject);
  });

  it('Should get correct price from HTML document response', async () => {
    marketWorker = new MarketWorker({
      ...basicMarketWorkerOptions,
      domElementSelector: '~metal-price',
      dataSource: DataSource.HTML,
      assets: [{
        asset: Asset.GOLD,
        products: [goldProduct],
      }, {
        asset: Asset.SILVER,
        products: [silverProduct],
      }],
    }, marketPriceService);
    marketWorker.performRequest = jest.fn(async (product: MarketWorkerProduct) => ({ data: `<!DOCTYPE HTML>
    <html>
      <head></head>
      <body>
        <div id="metal-price">${product.name === goldName ? '2020,45 zł' : '25,39 zł'}</div>
      </body>
    </html>`}));

    const goldResult = await marketWorker.getSpecificProductPrice(goldProduct);
    const silverResult = await marketWorker.getSpecificProductPrice(silverProduct);

    expect(goldResult).toEqual(expectedGoldPriceObject);
    expect(silverResult).toEqual(expectedSilverPriceObject);
  });

  it('Should call proper methods in JSON API response scenario', async () => {
    marketPriceService.saveMultiplePrices = jest.fn();
    marketWorker = new MarketWorker({
      ...basicMarketWorkerOptions,
      apiDataSelector: 'metal-price',
      dataSource: DataSource.API_OBJECT,
      assets: [{
        asset: Asset.GOLD,
        products: [goldProduct],
      }],
    }, marketPriceService);
    marketWorker.performRequest = jest.fn(async () => ({ data: {} }));
    marketWorker.getPricesFromExternalApi = jest.fn(() => '2020,45 zł');
    marketWorker.getPricesFromExternalWebsite = jest.fn(() => null);

    await marketWorker.work();

    expect(marketWorker.performRequest).toHaveBeenCalledTimes(1);
    expect(marketWorker.getPricesFromExternalApi).toHaveBeenCalledTimes(1);
    expect(marketWorker.getPricesFromExternalWebsite).toHaveBeenCalledTimes(0);
    expect(marketPriceService.saveMultiplePrices).toHaveBeenCalledTimes(1);
    expect(marketPriceService.saveMultiplePrices).toHaveBeenCalledWith([
      {
        asset: Asset.GOLD,
        assetClass: Class.METALS,
        currency: Currency.USD,
        market: Market.METALE_LOKACYJNE,
        name: goldName,
        price: 2020.45,
      },
    ]);
  });

  it('Should call proper methods in JSON API response scenario', async () => {
    marketPriceService.saveMultiplePrices = jest.fn();
    marketWorker = new MarketWorker({
      ...basicMarketWorkerOptions,
      apiDataSelector: 'goldPrice',
      dataSource: DataSource.HTML,
      assets: [{
        asset: Asset.SILVER,
        products: [silverProduct],
      }],
    }, marketPriceService);
    marketWorker.performRequest = jest.fn(async () => ({ data: {} }));
    marketWorker.getPricesFromExternalApi = jest.fn(() => null);
    marketWorker.getPricesFromExternalWebsite = jest.fn(() => '25,39 zł');

    await marketWorker.work();

    expect(marketWorker.performRequest).toHaveBeenCalledTimes(1);
    expect(marketWorker.getPricesFromExternalApi).toHaveBeenCalledTimes(0);
    expect(marketWorker.getPricesFromExternalWebsite).toHaveBeenCalledTimes(1);
    expect(marketPriceService.saveMultiplePrices).toHaveBeenCalledTimes(1);
    expect(marketPriceService.saveMultiplePrices).toHaveBeenCalledWith([
      {
        asset: Asset.SILVER,
        assetClass: Class.METALS,
        currency: Currency.USD,
        market: Market.METALE_LOKACYJNE,
        name: silverName,
        price: 25.39,
      },
    ]);
  });
});
