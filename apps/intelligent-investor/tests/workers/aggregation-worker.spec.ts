import * as moment from 'moment';

import { AggregatedPriceService } from '../../src/modules/database/services';
import { AggregationWorker } from '../../src/modules/price-aggregation/setup/aggregation-worker';
import { AGGREGATED_PRICE, SPOT_PRICE } from '../../src/shared/constants';
import { AggregationPeriod, Asset, Class, Currency } from '../../src/shared/enums';
import { logger } from '../../src/shared/helpers';
import { SpotPrice } from '../../src/shared/interfaces';

describe('AggregationWorker', () => {
  let aggregatedPriceService: AggregatedPriceService;
  let aggregationWorker: AggregationWorker;

  const currentMomentDate = moment();
  const expectedAggregationDate = {
    day: currentMomentDate.subtract(1, 'day').date(),
    month: currentMomentDate.subtract(1, 'day').month() + 1,
    year: currentMomentDate.subtract(1, 'day').year(),
  };
  const newDate = new Date();
  const mockSpotPrices: SpotPrice[] = [
    {
      asset: Asset.GOLD,
      assetClass: Class.METALS,
      currency: Currency.USD,
      createdAt: newDate,
      price: 1600,
    },
    {
      asset: Asset.GOLD,
      assetClass: Class.METALS,
      currency: Currency.USD,
      createdAt: newDate,
      price: 1700,
    },
  ];

  beforeAll(() => {
    logger.info = jest.fn();
    aggregatedPriceService = new AggregatedPriceService(SPOT_PRICE, AGGREGATED_PRICE);
  });

  it('Should call aggregated price service when setting moment date for the first time', async () => {
    aggregatedPriceService.getOldestSpotPriceCreationDate = jest.fn(async () => moment().subtract('1', 'year'));
    aggregationWorker = new AggregationWorker(aggregatedPriceService, AggregationPeriod.DAY);

    await aggregationWorker.setNewMomentDate();

    expect(aggregatedPriceService.getOldestSpotPriceCreationDate).toHaveBeenCalledTimes(1);
  });

  it('Should call proper aggregated price methods while working if price has been aggregated already', async () => {
    aggregatedPriceService.checkIfPriceHasBeenAggregatedAlready = jest.fn(async () => true);
    aggregatedPriceService.getSpotPricesByDay = jest.fn();
    aggregatedPriceService.getSpotPricesByMonth = jest.fn();
    aggregatedPriceService.getSpotPricesByYear = jest.fn();
    aggregationWorker = new AggregationWorker(aggregatedPriceService, AggregationPeriod.DAY);

    await aggregationWorker.work();

    expect(aggregatedPriceService.checkIfPriceHasBeenAggregatedAlready).toHaveBeenCalledWith(Asset.GOLD, expectedAggregationDate);
    expect(aggregatedPriceService.getSpotPricesByDay).not.toHaveBeenCalled();
    expect(aggregatedPriceService.getSpotPricesByMonth).not.toHaveBeenCalled();
    expect(aggregatedPriceService.getSpotPricesByYear).not.toHaveBeenCalled();
  });

  it('Should call proper aggregated price methods while working if price hasn\'t been aggregated yet', async () => {
    jest.spyOn(aggregatedPriceService, 'calculateAggregatedPrice')
    aggregatedPriceService.checkIfPriceHasBeenAggregatedAlready = jest.fn(async () => false);
    aggregatedPriceService.getSpotPricesByDay = jest.fn(async () => mockSpotPrices);
    aggregatedPriceService.getSpotPricesByMonth = jest.fn();
    aggregatedPriceService.getSpotPricesByYear = jest.fn();
    aggregatedPriceService.saveAggregatedPrice = jest.fn();
    aggregationWorker = new AggregationWorker(aggregatedPriceService, AggregationPeriod.DAY);
    const expectedAggregatedPriceObject = {
      asset: Asset.GOLD,
      assetClass: Class.METALS,
      currency: Currency.USD,
      date: expectedAggregationDate,
      prices: {
        average: 1650,
        closing: 1700,
        maximum: 1700,
        minimum: 1600,
        opening: 1600,
      },
    };

    await aggregationWorker.work();

    expect(aggregatedPriceService.getSpotPricesByMonth).not.toHaveBeenCalled();
    expect(aggregatedPriceService.getSpotPricesByYear).not.toHaveBeenCalled();

    expect(aggregatedPriceService.checkIfPriceHasBeenAggregatedAlready).toHaveBeenCalledWith(Asset.GOLD, expectedAggregationDate);
    expect(aggregatedPriceService.getSpotPricesByDay).toHaveBeenCalled();
    expect(aggregatedPriceService.calculateAggregatedPrice).toHaveBeenCalledWith(Asset.GOLD, expectedAggregationDate, mockSpotPrices);
    expect(aggregatedPriceService.saveAggregatedPrice).toHaveBeenCalledWith(expectedAggregatedPriceObject);
  });
});
