import { SpotPriceService } from '../../src/modules/database/services';
import { SpotWorker } from '../../src/modules/spot-price-monitor/setup/spot-worker';
import { SPOT_PRICE } from '../../src/shared/constants';
import { Asset, Class, Currency, DataSource, Market } from '../../src/shared/enums';

describe('SpotWorker', () => {
  let spotPriceService: SpotPriceService;
  let spotWorker: SpotWorker;
  const basicSpotWorkerOptions = {
    assetClass: Class.METALS,
    currency: Currency.USD,
    longDelay: null,
    shortDelay: null,
    url: null,
  };
  const expectedPrices = [
    { asset: 'gold',
      assetClass: 'metals',
      currency: 'usd',
      price: 2020.45,
    },
    { asset: 'silver',
      assetClass: 'metals',
      currency: 'usd',
      price: 25.39,
    },
  ];

  beforeAll(() => {
    spotPriceService = new SpotPriceService(SPOT_PRICE);
  });

  it('Should parse prices from JSON API response', () => {
    spotWorker = new SpotWorker({
      ...basicSpotWorkerOptions,
      assets: [
        {
          asset: Asset.GOLD,
          apiDataSelector: 'goldPrice',
        },
        {
          asset: Asset.SILVER,
          apiDataSelector: 'silverPrice',
        },
      ],
      dataSource: DataSource.API_OBJECT,
      market: Market.BINANCE,
    }, spotPriceService);

    const receivedPrices = spotWorker.getPricesFromExternalApi({ goldPrice: 2020.45, silverPrice: 25.39 });

    expect(receivedPrices).toEqual(expectedPrices);
  });

  it('Should parse prices from HTML document response', () => {
    spotWorker = new SpotWorker({
      ...basicSpotWorkerOptions,
      assets: [
        {
          asset: Asset.GOLD,
          domElementSelector: '.gold-price',
        },
        {
          asset: Asset.SILVER,
          domElementSelector: '.silver-price',
        },
      ],
      dataSource: DataSource.HTML,
      market: Market.INVESTING,
    }, spotPriceService);

    const receivedPrices = spotWorker.getPricesFromExternalWebsite(`<!DOCTYPE HTML>
    <html>
      <head></head>
      <body>
        <div class="gold-price">2,020.45</div>
        <div class="silver-price">25.39</div>
      </body>
    </html>`);

    expect(receivedPrices).toEqual(expectedPrices);
  });

  it('Should call proper methods in JSON API response scenario', async () => {
    spotPriceService.saveMultipleSpotPrices = jest.fn();
    spotWorker = new SpotWorker({
      ...basicSpotWorkerOptions,
      assets: [
        {
          asset: Asset.GOLD,
          domElementSelector: '.gold-price',
        },
        {
          asset: Asset.SILVER,
          domElementSelector: '.silver-price',
        },
      ],
      dataSource: DataSource.API_OBJECT,
      market: Market.METALE_LOKACYJNE,
    }, spotPriceService);
    spotWorker.performRequest = jest.fn(async () => ({ data: {} }));
    spotWorker.getPricesFromExternalApi = jest.fn(() => []);
    spotWorker.getPricesFromExternalWebsite = jest.fn(() => []);

    await spotWorker.work();

    expect(spotWorker.performRequest).toHaveBeenCalledTimes(1);
    expect(spotWorker.getPricesFromExternalApi).toHaveBeenCalledTimes(1);
    expect(spotPriceService.saveMultipleSpotPrices).toHaveBeenCalledTimes(1);
    expect(spotWorker.getPricesFromExternalWebsite).toHaveBeenCalledTimes(0);
  });

  it('Should call proper methods in HTML document response scenario', async () => {
    spotPriceService.saveMultipleSpotPrices = jest.fn();
    spotWorker = new SpotWorker({
      ...basicSpotWorkerOptions,
      assets: [
        {
          asset: Asset.GOLD,
          domElementSelector: '.gold-price',
        },
        {
          asset: Asset.SILVER,
          domElementSelector: '.silver-price',
        },
      ],
      dataSource: DataSource.HTML,
      market: Market.METALE_LOKACYJNE,
    }, spotPriceService);
    spotWorker.performRequest = jest.fn(async () => ({ data: {} }));
    spotWorker.getPricesFromExternalApi = jest.fn(() => []);
    spotWorker.getPricesFromExternalWebsite = jest.fn(() => []);

    await spotWorker.work();

    expect(spotWorker.performRequest).toHaveBeenCalledTimes(1);
    expect(spotWorker.getPricesFromExternalWebsite).toHaveBeenCalledTimes(1);
    expect(spotPriceService.saveMultipleSpotPrices).toHaveBeenCalledTimes(1);
    expect(spotWorker.getPricesFromExternalApi).toHaveBeenCalledTimes(0);
  });
});
