import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { WsAdapter } from '@nestjs/platform-ws';

import { DashReplacementPipe, LoggingInterceptor, LowercasePipe } from '@nestjs-monorepo/rest';

import { RootModule } from './root.module';
import { logger } from './shared/helpers';

async function bootstrap() {
  const app = await NestFactory.create(RootModule);

  app.setGlobalPrefix('/api');
  app.useGlobalInterceptors(new LoggingInterceptor(logger));
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalPipes(new DashReplacementPipe());
  app.useGlobalPipes(new LowercasePipe());
  app.useWebSocketAdapter(new WsAdapter(app));

  app.enableCors();

  await app.listen(3000);
}
bootstrap();
