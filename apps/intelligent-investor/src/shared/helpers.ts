import { Logger } from '@nestjs-monorepo/common';
import * as moment from 'moment';

import { Asset, Class, Market } from './enums';
import { PriceAggregationDate } from './interfaces';

export function getAssetClass(asset: Asset): Class {
  switch (asset) {
    case Asset.GOLD:
    case Asset.PALLADIUM:
    case Asset.PLATINUM:
    case Asset.SILVER:
      return Class.METALS;
    case Asset.CRUDE_OIL_WTI:
    case Asset.BRENT_OIL:
    case Asset.NATURAL_GAS:
    case Asset.HEATING_OIL:
    case Asset.GASOLINE:
      return Class.COMMODITIES;
    case Asset.BITCOIN:
    case Asset.ETHEREUM:
    case Asset.LITECOIN:
    case Asset.RIPPLE:
    case Asset.EOS:
    case Asset.NEO:
      return Class.CRYPTO;
    default:
      return null;
  }
}

export const assetsClassesMapping = {
  [Class.METALS]: [Asset.GOLD, Asset.SILVER, Asset.PLATINUM, Asset.PALLADIUM],
  [Class.COMMODITIES]: [Asset.CRUDE_OIL_WTI, Asset.BRENT_OIL, Asset.NATURAL_GAS, Asset.HEATING_OIL, Asset.GASOLINE],
  [Class.CRYPTO]: [Asset.BITCOIN, Asset.ETHEREUM, Asset.LITECOIN, Asset.RIPPLE, Asset.EOS, Asset.NEO],
}

export const logger = new Logger();

export class WorkerRequestError extends Error {
  metadata: object;

  constructor(metadata: object) {
    super('Worker failed while requesting external software for asset price.');
    this.metadata = metadata;
  }
}

interface MarketsValueParsers {
  [Market.BINANCE]: (value: any) => number;
  [Market.INVESTING]: (value: any) => number;
  [Market.MENNICA_SKARBOWA]: (value: any) => number;
  [Market.METALE_LOKACYJNE]: (value: any) => number;
}

export const marketsValueParsers: MarketsValueParsers = {
  [Market.BINANCE]: (value: number): number => value,
  [Market.INVESTING]: (value: string | number): number => typeof value === 'string' ? parseFloat(value.replace(/,/g, '')) : value,
  [Market.MENNICA_SKARBOWA]: (value: string): number => parseFloat(value.substr(0, value.indexOf(',') + 3).replace(',', '.')),
  [Market.METALE_LOKACYJNE]: (value: string): number => parseFloat(value.replace(/[ ]/g, '').substr(0, value.indexOf(',') + 3).replace(',', '.')),
};

export function parseAggregationDate(aggregationDate: PriceAggregationDate): PriceAggregationDate {
  const parsedAggregationDate: PriceAggregationDate = {
    year: typeof aggregationDate.year === 'string' ? +aggregationDate.year : aggregationDate.year,
  };
  if (aggregationDate.day) {
    parsedAggregationDate.day = typeof aggregationDate.day === 'string' ? +aggregationDate.day : aggregationDate.day;
  }
  if (aggregationDate.month) {
    parsedAggregationDate.month = typeof aggregationDate.month === 'string' ? +aggregationDate.month : aggregationDate.month;
  }

  return parsedAggregationDate;
}

export function isDateTodayOrLater(aggregationDate: PriceAggregationDate): boolean {
  const currentMomentDate = moment();

  const currentYear = currentMomentDate.year();
  if (currentYear > aggregationDate.year) {
    return false;
  } else if (currentYear < aggregationDate.year) {
    return true;
  }

  const currentMonth = currentMomentDate.month() + 1;
  if (!aggregationDate.month) {
    return true;
  } else if (currentMonth > aggregationDate.month) {
    return false;
  } else if (currentMonth < aggregationDate.month) {
    return true;
  }

  const currentDay = currentMomentDate.date();
  if (!aggregationDate.day) {
    return true;
  } else if (currentDay > aggregationDate.day) {
    return false;
  } else {
    return true;
  }
}

export function isWeekend(time: moment.Moment = moment()): boolean {
  const today = time.day();

  return today === 0 || today === 6;
}

export function prepareRedisKeyForSpotPrice(asset: Asset): string {
  return `SPOT_PRICE:${asset.toUpperCase()}`;
}
