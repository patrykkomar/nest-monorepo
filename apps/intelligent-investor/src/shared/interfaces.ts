import { Types } from 'mongoose';

import { Asset, Class, Currency, DataSource, Market } from './enums'

/* Mongo collections */
export interface MarketPrice {
  _id?: Types.ObjectId;
  asset: Asset;
  assetClass: Class;
  createdAt?: Date;
  currency: Currency;
  name: string;
  market: Market;
  price: number;
}

export interface Opportunity {
  _id?: Types.ObjectId;
  priceId?: Types.ObjectId;
  occuredAt?: Date;
}

export interface SpotPrice {
  _id?: Types.ObjectId;
  asset: Asset;
  assetClass: Class;
  createdAt: Date;
  currency: Currency;
  price: number;
}

export interface PriceAggregation {
  average: number;
  closing: number;
  maximum: number;
  minimum: number;
  opening: number;
}

export interface PriceAggregationDate {
  day?: number | string;
  month?: number | string;
  year: number | string;
}

export interface AggregatedPrice {
  _id?: Types.ObjectId;
  asset: Asset;
  assetClass: Class;
  currency: Currency;
  date: PriceAggregationDate;
  prices: PriceAggregation;
}

/* Spot workers */
export interface SpotWorkerAsset {
  asset: Asset;
  apiDataSelector?: string;
  domElementSelector?: string;
  apiArrayFilterFieldValue?: string;
}

export interface SpotWorkerOptions {
  apiArrayFilterFieldName?: string;
  apiArrayValueField?: string;
  assetClass: Class;
  assets: SpotWorkerAsset[];
  currency: Currency;
  dataSource: DataSource;
  longDelay: number;
  market: Market;
  shortDelay: number;
  url: string;
}

/* Market workers */
export interface MarketWorkerProduct {
  name: string;
  url: string;
}

export interface MarketWorkerAsset {
  asset: Asset;
  products: MarketWorkerProduct[];
}

export interface MarketWorkerOptions {
  market: Market;
  assetClass: Class;
  currency: Currency;
  dataSource: DataSource;
  apiDataSelector?: string;
  domElementSelector?: string;
  longDelay: number;
  normalDelay: number;
  shortDelay: number;
  assets: MarketWorkerAsset[];
}

export interface ProductPrice {
  asset?: Asset;
  name: string;
  price: number;
}

export interface AxiosResponse {
  data: string | object;
}
