export enum Asset {
  /* Metals */
  GOLD = 'gold',
  SILVER = 'silver',
  PLATINUM = 'platinum',
  PALLADIUM = 'palladium',

  /* Commodities */
  CRUDE_OIL_WTI = 'crude oil wti',
  BRENT_OIL = 'brent oil',
  NATURAL_GAS = 'natural gas',
  HEATING_OIL = 'heating oil',
  GASOLINE = 'gasoline',

  /* Cryptocurrencies */
  BITCOIN = 'bitcoin',
  ETHEREUM = 'ethereum',
  LITECOIN = 'litecoin',
  RIPPLE = 'ripple',
  EOS = 'eos',
  NEO = 'neo',
}

export enum Class {
  METALS = 'metals',
  COMMODITIES = 'commodities',
  CRYPTO = 'crypto',
}

export enum Currency {
  PLN = 'pln',
  USD = 'usd',
}

export enum DataSource {
  API_ARRAY = 'apiArray',
  API_OBJECT = 'apiObject',
  HTML = 'html',
}

export enum Price {
  MARKET = 'market',
  SPOT = 'spot',
}

export enum Market {
  BINANCE = 'Binance',
  INVESTING = 'investing.com',
  MENNICA_SKARBOWA = 'Mennica Skarbowa',
  METALE_LOKACYJNE = 'Metale Lokacyjne',
}

export enum AggregationPeriod {
  DAY = 'day',
  MONTH = 'month',
  YEAR = 'year',
}
