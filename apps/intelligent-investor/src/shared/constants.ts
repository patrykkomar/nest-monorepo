/* Mongo models */
export const AGGREGATED_PRICE = 'AggregatedPrice';
export const MARKET_PRICE = 'MarketPrice';
export const OPPORTUNITY = 'Opportunity';
export const SPOT_PRICE = 'SpotPrice';

/* Workers */
export const DEFAULT_WORKER_LONG_DELAY = 20 * 60 * 1000;
export const DEFAULT_WORKER_SHORT_DELAY = 5 * 1000;
export const DEAFULT_WORKER_INITIATION_DELAY = 1 * 1000;
