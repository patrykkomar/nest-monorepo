import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as path from 'path';

interface ApiLimits {
  anonymous: number;
  standard: number;
}

export enum Environment {
  DEVELOPMENT = 'development',
  PRODUCTION = 'production',
  TEST = 'test',
}

interface Jwt {
  secret: string;
}

interface Mongo {
  url: string;
}

interface Redis {
  host: string;
  port: number;
}

interface AggregationWorkers {
  enable: boolean;
}

interface MarketWorkers {
  enable: boolean;
}

interface SpotWorker {
  enable: boolean;
  saveInRedis: boolean;
}

interface Workers {
  aggregation: AggregationWorkers;
  market: MarketWorkers;
  spot: SpotWorker;
}

export interface Config {
  apiLimits: ApiLimits;
  environment: Environment;
  jwt: Jwt;
  mongo: Mongo;
  redis: Redis;
  workers: Workers;
}

const mainConfigFilePath = path.join(__dirname, '../../../resources/intelligent-investor/config.yaml');
const testConfigFilePath = path.join(__dirname, '../../tests/test-config.yaml');

export const config: Config = fs.existsSync(mainConfigFilePath)
  ? yaml.safeLoad(fs.readFileSync(mainConfigFilePath))
  : yaml.safeLoad(fs.readFileSync(testConfigFilePath));

