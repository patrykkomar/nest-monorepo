import { Module } from '@nestjs/common';

import { ApiModule } from './modules/api/api.module';
import { DatabaseModule } from './modules/database/database.module';
import { EventsModule } from './modules/events/events.module';
import { MarketPriceMonitorModule } from './modules/market-price-monitor/market-price-monitor.module';
import { SpotPriceMonitorModule } from './modules/spot-price-monitor/spot-price-monitor.module';
import { PriceAggregationModule } from './modules/price-aggregation/price-aggregation.module';

@Module({
  imports: [DatabaseModule, EventsModule, MarketPriceMonitorModule, SpotPriceMonitorModule, PriceAggregationModule, ApiModule],
})
export class RootModule { }
