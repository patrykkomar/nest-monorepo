import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { SpotPriceService } from '../database/services';
import { config } from '../../setup/config';
import { startSpotWorkers } from './setup/start-spot-workers';

@Module({
  imports: [DatabaseModule],
})
export class SpotPriceMonitorModule {
  /* TODO: Start three workers - metals, commodities, crypto */
  constructor(private readonly spotPriceService: SpotPriceService) {
    if (config.workers.spot.enable) {
      startSpotWorkers(this.spotPriceService);
    }
  }
}
