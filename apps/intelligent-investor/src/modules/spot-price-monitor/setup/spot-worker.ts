import axios from 'axios';
import * as DOMParser from 'dom-parser';
import { find, get, head, map } from 'lodash';

import { AbstractWorker, isTestEnvironment, RedisService } from '@nestjs-monorepo/common';

import { config } from '../../../setup/config';
import { DEAFULT_WORKER_INITIATION_DELAY } from '../../../shared/constants';
import { DataSource } from '../../../shared/enums';
import { isWeekend, logger, marketsValueParsers, prepareRedisKeyForSpotPrice, WorkerRequestError } from '../../../shared/helpers';
import { AxiosResponse, SpotWorkerAsset, SpotPrice, SpotWorkerOptions } from '../../../shared/interfaces';
import { SpotPriceService } from '../../database/services';


export class SpotWorker extends AbstractWorker{
  private parser: DOMParser;
  private redisService: RedisService;

  constructor(private options: SpotWorkerOptions,
              private spotPriceService: SpotPriceService) {
    super(DEAFULT_WORKER_INITIATION_DELAY);
    this.options = options;
    this.parser = new DOMParser();
    if (config.redis) {
      this.redisService = new RedisService(config.redis);
    }
  }

  performRequest(): Promise<AxiosResponse | WorkerRequestError> {
    const { assetClass, url } = this.options;

    return axios.get(url).catch((error) => new WorkerRequestError({ assetClass, error, url }));
  }

  getPricesFromExternalApi(response: object): SpotPrice[] {
    const { assetClass, assets, currency, market } = this.options;
    const valueParser = marketsValueParsers[market];

    const pricesObjects = map(assets, (spotWorkerAsset: SpotWorkerAsset) => {
      const { apiDataSelector, asset } = spotWorkerAsset;

      const priceInString = get(response, apiDataSelector);
      const price = valueParser(priceInString);

      return {
        asset,
        assetClass,
        currency,
        price,
      };
    });

    return pricesObjects;
  }

  getPricesFromExternalApiArray(response: object): SpotPrice[] {
    const { apiArrayFilterFieldName, apiArrayValueField, assetClass, assets, currency, market } = this.options;
    const valueParser = marketsValueParsers[market];

    const pricesObjects = map(assets, (spotWorkerAsset: SpotWorkerAsset) => {
      const { apiArrayFilterFieldValue, asset } = spotWorkerAsset;

      const priceBeforeParsing = get(find(response, priceObject => get(priceObject, apiArrayFilterFieldName) === apiArrayFilterFieldValue), apiArrayValueField);

      const price = valueParser(priceBeforeParsing);

      return {
        asset,
        assetClass,
        currency,
        price,
      };
    });

    return pricesObjects;
  }

  getPricesFromExternalWebsite(response: string): SpotPrice[] {
    const { assetClass, assets, currency, market } = this.options;
    const valueParser = marketsValueParsers[market];

    const externalHtmlDocument = this.parser.parseFromString(response);

    const pricesObjects = map(assets, (spotWorkerAsset: SpotWorkerAsset) => {
      const { asset, domElementSelector } = spotWorkerAsset;

      const priceInString = (domElementSelector.startsWith('~')
        ? externalHtmlDocument.getElementById(domElementSelector.replace('~', ''))
        : head(externalHtmlDocument.getElementsByClassName(domElementSelector.replace('.', '')))
      ).innerHTML;

      const price = valueParser(priceInString);

      return {
        asset,
        assetClass,
        currency,
        price,
      };
    });

    return pricesObjects;
  }

  async saveMultipleSpotPricesInRedis(spotPrices: SpotPrice[]): Promise<void> {
    spotPrices.forEach(async (spotPrice) => {
      const key = prepareRedisKeyForSpotPrice(spotPrice.asset);

      await this.redisService.set(key, spotPrice.price);
    });
  }

  async work(): Promise<void> {
    const { dataSource, longDelay, shortDelay } = this.options;

    // if (isWeekend() && !isTestEnvironment(config.environment)) {
    //   this.nextDelay = longDelay;

    //   return;
    // }

    const response = await this.performRequest();

    if (response instanceof WorkerRequestError) {
      logger.warning(response.message, response.metadata)
      this.nextDelay = longDelay;

      return;
    }

    let spotPriceObjects;

    switch (dataSource) {
      case DataSource.API_ARRAY:
        spotPriceObjects = this.getPricesFromExternalApiArray(response.data as object);
        break;
      case DataSource.API_OBJECT:
        spotPriceObjects = this.getPricesFromExternalApi(response.data as object);
        break;
      case DataSource.HTML:
        spotPriceObjects = this.getPricesFromExternalWebsite(response.data as string);
        break;
    }

    this.nextDelay = shortDelay;

    await this.spotPriceService.saveMultipleSpotPrices(spotPriceObjects);

    if (this.redisService && config.workers.spot.saveInRedis) {
      await this.saveMultipleSpotPricesInRedis(spotPriceObjects);
    }
  }
}
