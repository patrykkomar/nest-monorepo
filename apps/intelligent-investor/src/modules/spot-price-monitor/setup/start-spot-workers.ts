import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { forEach } from 'lodash';
import * as path from 'path';

import { logger } from '../../../shared/helpers';
import { SpotWorkerOptions } from '../../../shared/interfaces';
import { SpotPriceService } from '../../database/services';
import { SpotWorker } from './spot-worker';

export function startSpotWorkers(spotPriceService: SpotPriceService): void {
  const spotWorkersFile = path.join(__dirname, '../../../resources/intelligent-investor/workers/spot.yaml');

  if (fs.existsSync(spotWorkersFile)) {
    const spotWorkers = yaml.safeLoad(fs.readFileSync(spotWorkersFile));
    forEach(spotWorkers, (spotWorkerOptions: SpotWorkerOptions) => {
      if (spotWorkerOptions) {
        const spotWorker = new SpotWorker(spotWorkerOptions, spotPriceService);
        logger.info(`Starting spot worker for ${spotWorkerOptions.assetClass}...`)
        spotWorker.start();
      }
    });
  }
}
