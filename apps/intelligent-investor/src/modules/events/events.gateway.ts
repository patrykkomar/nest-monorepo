import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
  OnGatewayDisconnect,
  OnGatewayConnection,
} from '@nestjs/websockets';
import { values } from 'lodash';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Server } from 'ws';

import { RedisService } from '@nestjs-monorepo/common';

import { config } from '../../setup/config';
import { Asset } from '../../shared/enums';
import { logger, prepareRedisKeyForSpotPrice } from '../../shared/helpers';

@WebSocketGateway(8080)
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  private recentSpotPrices = {};
  private redisService: RedisService;

  constructor() {
    this.initializeRecentSpotPrices();
    if (config.redis) {
      this.redisService = new RedisService(config.redis);
    }
    values(Asset).forEach((asset: Asset) => {
      setInterval(() => {
        this.refreshAssetPrice(asset);
      }, 5000);
    });
  }

  initializeRecentSpotPrices(): void {
    for (const asset of values(Asset)) {
      this.recentSpotPrices[asset] = 0;
    }
  }

  handleConnection(client) {
    logger.info('Websocket client connected.', { openConnections: this.server.clients.size });
    // this.broadcast();
    // client.emit('connection', 'Successfully connected to server');
  }

  handleDisconnect(client) {
    logger.info('Websocket client disconnected.', { openConnections: this.server.clients.size });
  }

  @SubscribeMessage('events')
  onEvent(client: any, data: any): Observable<WsResponse<number>> {
    return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  }

  broadcastSpotPrice(asset: Asset, price: number): void {
    for (const client of this.server.clients) {
      client.send(`${asset}-${price}`);
    }
  }

  async refreshAssetPrice(asset: Asset): Promise<void> {
    if (!this.redisService) {
      return;
    }

    const key = prepareRedisKeyForSpotPrice(asset);
    const price = await this.redisService.get(key);

    if (price !== this.recentSpotPrices[asset]) {
      this.recentSpotPrices[asset] = price;
      this.broadcastSpotPrice(asset, +price);
    }
  }
}
