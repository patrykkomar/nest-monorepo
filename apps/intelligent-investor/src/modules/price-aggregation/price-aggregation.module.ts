import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { AggregatedPriceService } from '../database/services';
import { config } from '../../setup/config';
import { startAggregationWorkers } from './setup/start-aggregation-workers';

@Module({
  imports: [DatabaseModule],
})
export class PriceAggregationModule {
  constructor(private readonly aggregatedPriceService: AggregatedPriceService) {
    if (config.workers.aggregation.enable) {
      startAggregationWorkers(aggregatedPriceService);
    }
  }
}
