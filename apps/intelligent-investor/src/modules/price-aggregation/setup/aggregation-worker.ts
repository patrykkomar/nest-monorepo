import { AbstractWorker } from '@nestjs-monorepo/common';
import { values } from 'lodash';
import * as moment from 'moment';

import { DEAFULT_WORKER_INITIATION_DELAY } from '../../../shared/constants';
import { AggregationPeriod, Asset } from '../../../shared/enums';
import { logger } from '../../../shared/helpers';
import { AggregatedPriceService } from '../../database/services';

export class AggregationWorker extends AbstractWorker {
  private aggregatedAssets: object = {};
  private minimumDate: moment.Moment;
  private momentDate: moment.Moment;

  constructor(private aggregatedPriceService: AggregatedPriceService, private aggregationPeriod: AggregationPeriod) {
    super(DEAFULT_WORKER_INITIATION_DELAY);
    this.checkAssetsAsUnaggregated();
    this.setMinimumDate();
  }

  checkAssetsAsUnaggregated(): void {
    for (const asset of values(Asset)) {
      this.aggregatedAssets[asset] = false;
    }
  }

  async setMinimumDate(): Promise<void> {
    this.minimumDate = await this.aggregatedPriceService.getOldestSpotPriceCreationDate();
  }

  setNewMomentDate(): void {
    if (!this.momentDate) {
      this.momentDate = moment();
    }

    switch (this.aggregationPeriod) {
      case AggregationPeriod.DAY:
        this.momentDate = this.momentDate.subtract(1, 'day');
        break;
      case AggregationPeriod.MONTH:
        this.momentDate = this.momentDate.subtract(1, 'month');
        break;
      case AggregationPeriod.YEAR:
        this.momentDate = this.momentDate.subtract(1, 'year');
        break;
      default:
        break;
    }

    if (this.momentDate.isBefore(this.minimumDate)) {
      logger.info(`${this.aggregationPeriod.toUpperCase()} aggregation worker has exceeded minimum date, stopping.`);
      this.stop();
    }
  }

  public async work() {
    if (!this.momentDate) {
      this.setNewMomentDate();
    }

    const aggregationDate = this.aggregatedPriceService.getAggregationDate(this.aggregationPeriod, this.momentDate);

    let asset: Asset;

    for (const someAsset of values(Asset)) {
      if (this.aggregatedAssets[someAsset] === false) {
        asset = someAsset;
        break;
      }
    }

    if (!asset) {
      logger.info(`No assets left to aggregate for ${this.aggregationPeriod.toUpperCase()} aggregation worker, setting aggregation date to one ${this.aggregationPeriod} earlier.`, { aggregationDate });

      this.checkAssetsAsUnaggregated();
      this.setNewMomentDate();
      this.nextDelay = 60000;

      return;
    }

    this.nextDelay = 5000;
    this.aggregatedAssets[asset] = true;

    if (await this.aggregatedPriceService.checkIfPriceHasBeenAggregatedAlready(asset, aggregationDate)) {
      logger.info('Price has been already aggregated.', { asset, aggregationDate });

      return;
    }

    let spotPrices;

    switch (this.aggregationPeriod) {
      case AggregationPeriod.DAY:
        spotPrices = await this.aggregatedPriceService.getSpotPricesByDay(asset, this.momentDate);
        break;
      case AggregationPeriod.MONTH:
        spotPrices = await this.aggregatedPriceService.getSpotPricesByMonth(asset, this.momentDate);
        break;
      case AggregationPeriod.YEAR:
        spotPrices = await this.aggregatedPriceService.getSpotPricesByYear(asset, this.momentDate);
        break;
      default:
        break;
    }

    const aggregatedPriceObject = await this.aggregatedPriceService.calculateAggregatedPrice(asset, aggregationDate, spotPrices);

    if (!aggregatedPriceObject) {
      logger.warning(`No spot prices found by ${this.aggregationPeriod.toUpperCase()} aggregation worker.`, { asset, aggregationDate });
    } else {
      await this.aggregatedPriceService.saveAggregatedPrice(aggregatedPriceObject);
    }

    return;
  }
}
