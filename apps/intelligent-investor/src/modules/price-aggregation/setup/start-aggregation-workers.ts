import { forEach, values } from 'lodash';

import { AggregationPeriod } from '../../../shared/enums';
import { logger } from '../../../shared/helpers';
import { AggregatedPriceService } from '../../database/services';
import { AggregationWorker } from './aggregation-worker';

export function startAggregationWorkers(aggregatedPriceService: AggregatedPriceService): void {
  forEach(values(AggregationPeriod), (aggregationPeriod: AggregationPeriod) => {
    const aggregationWorker = new AggregationWorker(aggregatedPriceService, aggregationPeriod);
    logger.info(`Starting price aggregation worker for ${aggregationPeriod.toUpperCase()} period...`)
    aggregationWorker.start();
  });
}
