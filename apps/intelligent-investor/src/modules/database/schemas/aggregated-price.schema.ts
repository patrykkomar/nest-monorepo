import { Schema } from 'mongoose';

import {
  mongooseEnum,
  mongooseInteger,
  mongooseNumber,
  mongooseSchemaOptions,
} from '@nestjs-monorepo/common';

import { Asset, Class, Currency } from '../../../shared/enums';
import { AggregatedPrice, PriceAggregation } from '../../../shared/interfaces';

const priceAggregationObjectSchema = {
  average: mongooseNumber(),
  closing: mongooseNumber(),
  maximum: mongooseNumber(),
  minimum: mongooseNumber(),
  opening: mongooseNumber(),
};

const aggregationDateObjectSchema = {
  day: mongooseInteger(false, 1, 31),
  month: mongooseInteger(false, 1, 12),
  year: mongooseInteger(true, 2000),
};

const priceAggregationSchema = new Schema<PriceAggregation>(priceAggregationObjectSchema, { ...mongooseSchemaOptions, _id: false });

const aggregatedPriceObjectSchema = {
  asset: mongooseEnum(true, Object.values(Asset)),
  assetClass: mongooseEnum(true, Object.values(Class)),
  currency: mongooseEnum(true, Object.values(Currency)),
  date: {
    type: aggregationDateObjectSchema,
    required: true,
  },
  prices: {
    type: priceAggregationSchema,
    required: true,
  },
};

export const aggregatedPriceSchema = new Schema<AggregatedPrice>(aggregatedPriceObjectSchema, mongooseSchemaOptions);
