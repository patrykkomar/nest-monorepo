import { Schema } from 'mongoose';

import {
  mongooseDate,
  mongooseNumber,
  mongooseSchemaOptions,
  mongooseEnum,
} from '@nestjs-monorepo/common';

import { Asset, Class, Currency } from '../../../shared/enums';
import { SpotPrice } from '../../../shared/interfaces';

const spotPriceObjectSchema = {
  asset: mongooseEnum(true, Object.values(Asset)),
  createdAt: mongooseDate(true, Date.now, true),
  assetClass: mongooseEnum(true, Object.values(Class)),
  currency: mongooseEnum(true, Object.values(Currency)),
  price: mongooseNumber(),
};

export const spotPriceSchema = new Schema<SpotPrice>(spotPriceObjectSchema, mongooseSchemaOptions);
