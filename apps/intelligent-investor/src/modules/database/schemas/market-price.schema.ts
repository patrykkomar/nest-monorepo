import { Schema } from 'mongoose';

import {
  mongooseDate,
  mongooseNumber,
  mongooseSchemaOptions,
  mongooseString,
  mongooseEnum,
} from '@nestjs-monorepo/common';

import { Asset, Class, Currency, Market } from '../../../shared/enums';
import { MarketPrice } from '../../../shared/interfaces';

const marketPriceObjectSchema = {
  asset: mongooseEnum(true, Object.values(Asset)),
  createdAt: mongooseDate(),
  assetClass: mongooseEnum(true, Object.values(Class)),
  currency: mongooseEnum(true, Object.values(Currency)),
  market: mongooseEnum(true, Object.values(Market)),
  name: mongooseString(),
  price: mongooseNumber(),
};

export const marketPriceSchema = new Schema<MarketPrice>(marketPriceObjectSchema, mongooseSchemaOptions);
