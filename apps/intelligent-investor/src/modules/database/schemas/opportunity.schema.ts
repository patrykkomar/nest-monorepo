import { Schema } from 'mongoose';

import {
  mongooseDate,
  mongooseObjectReference,
  mongooseSchemaOptions,
} from '@nestjs-monorepo/common';

import { Opportunity } from '../../../shared/interfaces';

const opportunityObjectSchema = {
  priceId: mongooseObjectReference(true, true, true),
  occuredAt: mongooseDate(),
};

export const opportunitySchema = new Schema<Opportunity>(opportunityObjectSchema, { ...mongooseSchemaOptions, collection: 'opportunities' });
