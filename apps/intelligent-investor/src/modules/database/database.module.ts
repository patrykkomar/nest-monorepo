import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { config } from '../../setup/config';
import { AGGREGATED_PRICE, MARKET_PRICE, OPPORTUNITY, SPOT_PRICE } from '../../shared/constants';
import { aggregatedPriceSchema, marketPriceSchema, opportunitySchema, spotPriceSchema } from './schemas';
import { services } from './services';
import {  } from './schemas/aggregated-price.schema';

@Module({
  imports: [
    MongooseModule.forRoot(config.mongo.url, { useCreateIndex: true, useFindAndModify: false, useNewUrlParser: true, useUnifiedTopology: true }),
    MongooseModule.forFeature([
      {
        name: AGGREGATED_PRICE,
        schema: aggregatedPriceSchema,
      },
      {
        name: MARKET_PRICE,
        schema: marketPriceSchema,
      },
      {
        name: OPPORTUNITY,
        schema: opportunitySchema,
      },
      {
        name: SPOT_PRICE,
        schema: spotPriceSchema,
      },
    ]),
  ],
  providers: services,
  exports: services,
})
export class DatabaseModule {}
