import { AggregatedPriceService } from './services/aggregated-price.service';
import { MarketPriceService } from './services/market-price.service';
import { OpportunityService } from './services/opportunity.service';
import { SpotPriceService } from './services/spot-price.service';

const services = [
  AggregatedPriceService,
  MarketPriceService,
  OpportunityService,
  SpotPriceService,
];

export {
  AggregatedPriceService,
  MarketPriceService,
  OpportunityService,
  SpotPriceService,
  services,
};
