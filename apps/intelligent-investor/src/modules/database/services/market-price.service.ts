import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { map } from 'lodash';
import { Model } from 'mongoose';

import { MARKET_PRICE } from '../../../shared/constants';
import { MarketPrice } from '../../../shared/interfaces';

@Injectable()
export class MarketPriceService {
  constructor(@InjectModel(MARKET_PRICE) private readonly marketPriceModel: Model<MarketPrice>) {}

  async saveMultiplePrices(marketPrices: MarketPrice[]): Promise<MarketPrice[]> {
    const marketPriceObjects = map(marketPrices, spotPriceObject => new this.marketPriceModel(spotPriceObject));

    return this.marketPriceModel.insertMany(marketPriceObjects);
  }
}
