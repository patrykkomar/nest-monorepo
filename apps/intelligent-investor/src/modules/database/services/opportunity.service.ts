import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { OPPORTUNITY } from '../../../shared/constants';
import { MarketPrice, Opportunity } from '../../../shared/interfaces';

@Injectable()
export class OpportunityService {
  constructor(@InjectModel(OPPORTUNITY) private readonly opportunityModel: Model<Opportunity>) {}

  async saveOpportunity(marketPrice: MarketPrice): Promise<void> {
    const opportunity = new this.opportunityModel({ priceId: marketPrice._id });
    await opportunity.save();

    return;
  }
}
