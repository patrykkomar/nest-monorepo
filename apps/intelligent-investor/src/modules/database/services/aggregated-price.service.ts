import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { head, last, sum } from 'lodash';
import * as moment from 'moment';
import { Model } from 'mongoose';

import { AGGREGATED_PRICE, SPOT_PRICE } from '../../../shared/constants';
import { Asset, AggregationPeriod } from '../../../shared/enums';
import { getAssetClass } from '../../../shared/helpers';
import { AggregatedPrice, PriceAggregationDate, SpotPrice } from '../../../shared/interfaces';

@Injectable()
export class AggregatedPriceService {
  constructor(@InjectModel(SPOT_PRICE) private readonly spotPriceModel: Model<SpotPrice>,
              @InjectModel(AGGREGATED_PRICE) private readonly aggregatedPriceModel: Model<AggregatedPrice>) {}

  async calculateAggregatedPrice(asset: Asset, aggregationDate: PriceAggregationDate, spotPrices: SpotPrice[]): Promise<AggregatedPrice> {
    const prices = spotPrices.map((spotPriceObject: SpotPrice) => spotPriceObject.price);

    if (prices.length === 0) {
      return null;
    }

    const average = +((sum(prices) / prices.length).toFixed(2));
    const closing = last(prices);
    const maximum = Math.max(...prices);
    const minimum = Math.min(...prices);
    const opening = head(prices);

    const aggregatedPriceObject: AggregatedPrice = {
      asset,
      assetClass: getAssetClass(asset),
      currency: head(spotPrices).currency,
      date: aggregationDate,
      prices: {
        average,
        closing,
        maximum,
        minimum,
        opening,
      },
    };

    return aggregatedPriceObject;
  }

  async saveAggregatedPrice(aggregatedPriceObject: AggregatedPrice): Promise<void> {
    const aggregatedPriceModel = new this.aggregatedPriceModel(aggregatedPriceObject);

    await aggregatedPriceModel.save();
  }

  async getOldestSpotPriceCreationDate(): Promise<moment.Moment> {
    const oldestSpotPrice: SpotPrice = await this.spotPriceModel.findOne();

    if (!oldestSpotPrice) {
      return null;
    }

    return moment(oldestSpotPrice.createdAt);
  }

  getAggregationDate(aggregationPeriod: AggregationPeriod, momentDate: moment.Moment): PriceAggregationDate {
    const aggregationDate: PriceAggregationDate = { year: momentDate.year() };

    if (aggregationPeriod === AggregationPeriod.DAY) {
      aggregationDate.day = momentDate.date();
      aggregationDate.month = momentDate.month() + 1;
    } else if (aggregationPeriod === AggregationPeriod.MONTH) {
      aggregationDate.month = momentDate.month() + 1;
    }

    return aggregationDate;
  }

  async getAggreagatedPrice(asset: Asset, aggregationDate: PriceAggregationDate): Promise<AggregatedPrice> {
    return this.aggregatedPriceModel.findOne({ asset, date: { $eq: aggregationDate } });
  }

  async checkIfPriceHasBeenAggregatedAlready(asset: Asset, aggregationDate: PriceAggregationDate): Promise<boolean> {
    const existingAggregatedPrice = await this.getAggreagatedPrice(asset, aggregationDate);

    return existingAggregatedPrice !== null;
  }

  async getSpotPricesByDay(asset: Asset, momentDate: moment.Moment): Promise<SpotPrice[]> {
    const year = momentDate.year();
    let month = (momentDate.month() + 1).toString();
    if (month.length === 1) { month = `0${month}`; }
    let day = (momentDate.date()).toString();
    if (day.length === 1) { day = `0${day}`; }

    const startDate = moment(`${year}-${month}-${day}T00:00:00.000`);
    const endDate = moment(`${year}-${month}-${day}T23:59:59.999`);

    const spotPrices = await this.spotPriceModel.find({ asset, createdAt: { $gte: startDate, $lte: endDate } });

    return spotPrices;
  }

  async getSpotPricesByMonth(asset: Asset, momentDate: moment.Moment): Promise<SpotPrice[]> {
    const year = momentDate.year();
    let month = (momentDate.month() + 1).toString();
    if (month.length === 1) { month = `0${month}`; }
    let lastDay = (momentDate.add(1, 'month').date(0).date()).toString();
    if (lastDay.length === 1) { lastDay = `0${lastDay}`; }

    const startDate = moment(`${year}-${month}-01T00:00:00.000`);
    const endDate = moment(`${year}-${month}-${lastDay}T23:59:59.999`);

    const spotPrices = await this.spotPriceModel.find({ asset, createdAt: { $gte: startDate, $lte: endDate } });

    return spotPrices;
  }

  async getSpotPricesByYear(asset: Asset, momentDate: moment.Moment): Promise<SpotPrice[]> {
    const year = momentDate.year();

    const startDate = moment(`${year}-01-01T00:00:00.000`);
    const endDate = moment(`${year}-12-31T23:59:59.999`);

    const spotPrices = await this.spotPriceModel.find({ asset, createdAt: { $gte: startDate, $lte: endDate } });

    return spotPrices;
  }
}
