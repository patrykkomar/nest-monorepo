import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { groupBy, head, map, pick, reverse, sortBy, values } from 'lodash';
import * as moment from 'moment';
import { Model } from 'mongoose';

import { SPOT_PRICE } from '../../../shared/constants';
import { Asset, Class } from '../../../shared/enums';
import { assetsClassesMapping } from '../../../shared/helpers';
import { SpotPrice } from '../../../shared/interfaces';

@Injectable()
export class SpotPriceService {
  constructor(@InjectModel(SPOT_PRICE) private readonly spotPriceModel: Model<SpotPrice>) {}

  private transformSpotPriceObject(priceObject: SpotPrice): Partial<SpotPrice> {
    return pick(priceObject, ['asset', 'createdAt', 'currency', 'price']);
  }

  async saveMultipleSpotPrices(spotPrices: SpotPrice[]): Promise<SpotPrice[]> {
    const spotPriceObjects = map(spotPrices, spotPriceObject => new this.spotPriceModel(spotPriceObject));

    return this.spotPriceModel.insertMany(spotPriceObjects);
  }

  async getAllSpotPrices(selectOpening = false): Promise<Partial<SpotPrice>[]> {
    const momenteDate = this.prepareDayOpeningTimestamp(selectOpening);
    const assets = values(Asset);

    const spotPrices = await this.spotPriceModel.find({ createdAt: { $gt: momenteDate } });
    const groupedSpotPrices = groupBy(spotPrices, 'asset');

    const assetsSpotPrices = assets
      .map((asset: Asset) => head(selectOpening
        ? sortBy(groupedSpotPrices[asset], 'createdAt')
        : reverse(sortBy(groupedSpotPrices[asset], 'createdAt'))))
      .map(this.transformSpotPriceObject);

    return assetsSpotPrices;
  }

  async getRecentPriceForAsset(asset: Asset, selectOpening = false): Promise<Partial<SpotPrice>> {
    const momenteDate = this.prepareDayOpeningTimestamp(selectOpening);
    const query = { asset, createdAt: { $gt: momenteDate } };

    const sort = { createdAt: selectOpening ? 1 : -1 };

    const spotPrice = await this.spotPriceModel.findOne(
      query,
      {},
      { sort });

    return spotPrice ? this.transformSpotPriceObject(spotPrice): null;
  }

  async getRecentPricesForAssetClass(assetClass: Class, selectOpening = false):  Promise<Partial<SpotPrice>[]> {
    const momenteDate = this.prepareDayOpeningTimestamp(selectOpening);
    const query = { assetClass, createdAt: { $gt: momenteDate } };

    const assets = assetsClassesMapping[assetClass];

    const spotPrices = await this.spotPriceModel.find(query);
    const groupedSpotPrices = groupBy(spotPrices, 'asset');

    const assetsSpotPrices = assets
      .map((asset: Asset) => head(selectOpening
        ? sortBy(groupedSpotPrices[asset], 'createdAt')
        : reverse(sortBy(groupedSpotPrices[asset], 'createdAt'))))
      .map(this.transformSpotPriceObject);

    return assetsSpotPrices;
  }

  prepareDayOpeningTimestamp(selectOpening: boolean): moment.Moment {
    if (!selectOpening) {
      return moment().subtract(1, 'hours');
    } else {
      const currentDate = moment();
      let month = (currentDate.month() + 1).toString();
      if (month.length === 1) month = `0${month}`;
      let day = (currentDate.date()).toString();
      if (day.length === 1) day = `0${day}`;
      const openingDate = moment(`${currentDate.year()}-${month}-${day}T00:00:00.000`);

      return openingDate;
    }
  }
}
