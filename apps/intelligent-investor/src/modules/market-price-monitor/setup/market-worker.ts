import axios from 'axios';
import * as DOMParser from 'dom-parser';
import { concat, find, get, head, some } from 'lodash';

import { AbstractWorker } from '@nestjs-monorepo/common';

import { DEAFULT_WORKER_INITIATION_DELAY } from '../../../shared/constants';
import { DataSource } from '../../../shared/enums';
import { logger, marketsValueParsers, WorkerRequestError } from '../../../shared/helpers';
import { AxiosResponse, MarketWorkerOptions, MarketWorkerAsset, MarketWorkerProduct, ProductPrice } from '../../../shared/interfaces';
import { MarketPriceService } from '../../database/services';

export class MarketWorker extends AbstractWorker {
  private lastPrices: ProductPrice[];
  private parser: DOMParser;
  private valueParser: (value: string | number) => number;

  constructor(private options: MarketWorkerOptions,
              private marketPriceService: MarketPriceService) {
    super(DEAFULT_WORKER_INITIATION_DELAY);
    this.parser = new DOMParser();
    this.valueParser = marketsValueParsers[options.market];
  }

  async work(): Promise<void> {
    const { getWholeAssetPrices } = this;
    const { assets, assetClass, currency, market } = this.options;

    const pricesForAssets = await Promise.all(assets.map(getWholeAssetPrices.bind(this)));

    const priceObjects = concat(...pricesForAssets)
      .map((productPrice: ProductPrice) => ({
        ...productPrice,
        assetClass,
        currency,
        market,
      }));

    this.setNextDelay(priceObjects);

    await this.marketPriceService.saveMultiplePrices(priceObjects);
  }

  performRequest(marketWorkerProduct: MarketWorkerProduct): Promise<AxiosResponse | WorkerRequestError> {
    const { market } = this.options;
    const { name, url } = marketWorkerProduct;

    return axios.get(url).catch((error) => new WorkerRequestError({ market, name, error, url }));
  }

  async getWholeAssetPrices(marketAsset: MarketWorkerAsset): Promise<ProductPrice[]> {
    const getSpecificProductPrice = this.getSpecificProductPrice.bind(this);
    const { asset, products } = marketAsset;

    const prices = (await Promise.all(products.map(getSpecificProductPrice)))
      .map(productPrice => ({ ...(productPrice as ProductPrice), asset }))
      .filter(price => price !== null);

    return prices;
  }

  getPricesFromExternalWebsite(response: string): string {
    const { domElementSelector } = this.options;
    const externalHtmlDocument = this.parser.parseFromString(response);
    return (domElementSelector.startsWith('~')
      ? externalHtmlDocument.getElementById(domElementSelector.replace('~', ''))
      : head(externalHtmlDocument.getElementsByClassName(domElementSelector.replace('.', '')))
    ).innerHTML;
  }

  getPricesFromExternalApi(response: object): string | number {
    return get(response, this.options.apiDataSelector);
  }

  async getSpecificProductPrice(product: MarketWorkerProduct): Promise<ProductPrice> {
    const { dataSource, longDelay } = this.options;
    const { name } = product;

    const response = await this.performRequest(product);

    if (response instanceof WorkerRequestError) {
      logger.warning(response.message, response.metadata)
      this.nextDelay = longDelay;

      return;
    }

    const marketPrice = dataSource === DataSource.API_OBJECT
      ? this.getPricesFromExternalApi(response.data as object)
      : this.getPricesFromExternalWebsite(response.data as string);

    const price = this.valueParser(marketPrice);

    return {
      name,
      price,
    };
  }

  setNextDelay(priceObjects: ProductPrice[]): void {
    const { normalDelay, shortDelay } = this.options;
    const anyValueDidntChange = some(priceObjects, priceObject => {
      const previousItemValue = find(this.lastPrices, price => price.name === priceObject.name);

      return previousItemValue && previousItemValue.price === priceObject.price;
    });

    this.nextDelay = anyValueDidntChange
      ? normalDelay
      : shortDelay;
  }
}
