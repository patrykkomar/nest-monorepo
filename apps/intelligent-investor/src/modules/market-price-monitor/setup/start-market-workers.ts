import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { forEach, isArray } from 'lodash';
import * as path from 'path';

import { MarketWorkerOptions } from '../../../shared/interfaces';
import { logger } from '../../../shared/helpers';
import { MarketPriceService } from '../../database/services';
import { MarketWorker } from './market-worker';

export function startMarketWorkers(marketPriceService: MarketPriceService): void {
  const marketWorkersFile = path.join(__dirname, '../../../resources/intelligent-investor/workers/market.yaml');

  if (fs.existsSync(marketWorkersFile)) {
    const marketWorkers: MarketWorkerOptions[] = yaml.safeLoad(fs.readFileSync(marketWorkersFile));
    if (isArray(marketWorkers)) {
      forEach(marketWorkers, (marketWorkerOptions: MarketWorkerOptions) => {
        const marketWorker = new MarketWorker(marketWorkerOptions, marketPriceService);
        logger.info(`Starting market worker for ${marketWorkerOptions.market}...`)
        marketWorker.start();
      });
    }
  }
}
