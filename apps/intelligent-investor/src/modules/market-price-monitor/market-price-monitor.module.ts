import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { MarketPriceService } from '../database/services/market-price.service';
import { config } from '../../setup/config';
import { startMarketWorkers } from './setup/start-market-workers';

@Module({
  imports: [DatabaseModule],
})
export class MarketPriceMonitorModule {
  constructor(private readonly marketPriceService: MarketPriceService) {
    if (config.workers.market.enable) {
      startMarketWorkers(this.marketPriceService);
    }
  }
}
