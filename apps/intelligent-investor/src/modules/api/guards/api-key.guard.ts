import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { get } from 'lodash';

import { JwtManager, RedisService } from '@nestjs-monorepo/common';

import { config } from '../../../setup/config';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  private redisService: RedisService;

  constructor(private readonly jwtManager: JwtManager) {
    if (config.redis) {
      this.redisService = new RedisService(config.redis);
    }
  }

  generateConnectionKey(rawKey: string): string {
    return `connection-${rawKey}`;
  }

  isTokenValid(authorizationToken: string, apiKey: string): boolean {
    return apiKey
      && authorizationToken
      && this.jwtManager.verifyToken(authorizationToken, config.jwt.secret)
      && apiKey === get(this.jwtManager.decodePayload(authorizationToken), 'apiKey');
  }

  throwTooManyRequests(anonymousConnections: boolean): void {
    const { anonymous, standard } = config.apiLimits;
    const message = `You exceeded requests limit equal ${anonymousConnections ? anonymous : standard} requests per minute.`;
    throw new HttpException(message, HttpStatus.TOO_MANY_REQUESTS);
  }

  async canConnect(redisQueueKey: string, anonymousConnection: boolean): Promise<boolean> {
    const { anonymous, standard } = config.apiLimits;
    const connectionsInLastMinute = (await this.redisService.lrange(redisQueueKey))
      .filter(timestamp => timestamp > Date.now() - 60 * 1000);

    if (connectionsInLastMinute.length >= (anonymousConnection ? anonymous : standard)) {
      return false;
    }

    await this.redisService.lpush(redisQueueKey, Date.now());

    return true;
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const { connection: { remoteAddress }, query: { key } } = context.switchToHttp().getRequest();
    // const { connection: { remoteAddress }, headers: { authorization }, query: { key } } = context.switchToHttp().getRequest();

    if (!key && !await this.canConnect(this.generateConnectionKey(remoteAddress), true)) {
      this.throwTooManyRequests(true);
    }

    // if (!this.isTokenValid(authorization, key)) {
    //   throw new HttpException('API key not provided or invalid.', HttpStatus.UNAUTHORIZED);
    // }

    if (!await this.canConnect(this.generateConnectionKey(key), false)) {
      this.throwTooManyRequests(false);
    }

    return true;
  }
}
