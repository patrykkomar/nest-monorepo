import { Controller, Get, HttpException, HttpStatus, Param, Query, UseGuards } from '@nestjs/common';
import { pick, values } from 'lodash';

import { Asset } from '../../../shared/enums';
import { isDateTodayOrLater, parseAggregationDate } from '../../../shared/helpers';
import { AggregatedPrice } from '../../../shared/interfaces';
import { AggregatedPriceService } from '../../database/services';
import { AggregationDateDto } from '../dtos/aggregation-date.dto';
import { ApiKeyGuard } from '../guards/api-key.guard';

@UseGuards(ApiKeyGuard)
@Controller('/aggregated-price')
export class AggregatedPriceController {
  private assets: Asset[];

  constructor(private readonly aggregatedPriceService: AggregatedPriceService) {
    this.assets = values(Asset);
  }

  @Get('/:asset')
  async getPricesForAssetClass(@Param('asset') asset: Asset, @Query() aggregationDate: AggregationDateDto): Promise<AggregatedPrice> {
    if (!this.assets.includes(asset)) {
      throw new HttpException(`Cannot find any records for ${asset}.`, HttpStatus.BAD_REQUEST);
    }

    const parsedAggregationDate = parseAggregationDate(aggregationDate);

    if (isDateTodayOrLater(parsedAggregationDate)) {
      throw new HttpException(`We're sorry, but we can't really predict the future.`, HttpStatus.BAD_REQUEST);
    }

    const aggregatedPrice = await this.aggregatedPriceService.getAggreagatedPrice(asset, parsedAggregationDate);

    if (!aggregatedPrice) {
      throw new HttpException(`Unfortunately, there are no records for ${asset} and requested date.`, HttpStatus.NOT_FOUND);
    }

    return pick(aggregatedPrice, ['asset', 'currency', 'prices']);
  }
}
