import { Controller, Get, HttpException, HttpStatus, Param, Query, UseGuards } from '@nestjs/common';
import { values } from 'lodash';

import { ParseBooleanPipe } from '@nestjs-monorepo/rest';

import { Asset, Class } from '../../../shared/enums';
import { SpotPrice } from '../../../shared/interfaces';
import { SpotPriceService } from '../../database/services';
import { ApiKeyGuard } from '../guards/api-key.guard';

@UseGuards(ApiKeyGuard)
@Controller('/spot-price')
export class SpotPriceController {
  private assets: Asset[];
  private classes: Class[];

  constructor(private readonly spotPriceService: SpotPriceService) {
    this.assets = values(Asset);
    this.classes = values(Class);
  }

  @Get()
  async getAllSpotPrices(@Query('opening', new ParseBooleanPipe()) opening: boolean): Promise<Partial<SpotPrice>[]> {
    return this.spotPriceService.getAllSpotPrices(opening);
  }

  @Get('/:assetOrClass')
  async getPricesForAssetOrClass(
    @Param('assetOrClass') assetOrClass: Asset | Class,
    @Query('opening', new ParseBooleanPipe()) opening: boolean,
): Promise<Partial<SpotPrice> | Partial<SpotPrice>[]> {
    if (this.classes.includes(assetOrClass as Class)) {
      return this.spotPriceService.getRecentPricesForAssetClass(assetOrClass as Class, opening);
    }

    if (this.assets.includes(assetOrClass as Asset)) {
      return this.spotPriceService.getRecentPriceForAsset(assetOrClass as Asset, opening);
    }

    throw new HttpException(`Cannot find any records for ${assetOrClass}.`, HttpStatus.BAD_REQUEST);
  }

  @Get('/:assetOrClass/opening')
  async getOpeningPricesForAssetOrClass(@Param('assetOrClass') assetOrClass: Asset | Class): Promise<Partial<SpotPrice> | Partial<SpotPrice>[]> {
    if (this.classes.includes(assetOrClass as Class)) {
      return this.spotPriceService.getRecentPricesForAssetClass(assetOrClass as Class, true);
    }

    if (this.assets.includes(assetOrClass as Asset)) {
      return this.spotPriceService.getRecentPriceForAsset(assetOrClass as Asset, true);
    }

    throw new HttpException(`Cannot find any records for ${assetOrClass}.`, HttpStatus.BAD_REQUEST);
  }
}
