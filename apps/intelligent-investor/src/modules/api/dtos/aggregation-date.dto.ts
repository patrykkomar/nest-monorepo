import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export class AggregationDateDto {
  @IsNumber()
  @Type(() => Number)
  year: number;

  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  month: number;

  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  day: number;
}
