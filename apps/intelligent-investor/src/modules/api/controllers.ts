import { AggregatedPriceController } from './controllers/aggregated-price.controller';
import { SpotPriceController } from './controllers/spot-price.controller';

export const controllers = [
  AggregatedPriceController,
  SpotPriceController,
];
