import { Module } from '@nestjs/common';

import { ApiModule } from './modules/api/api.module';
import { DatabaseModule } from './modules/database/database.module';
import { WebsocketModule } from './modules/websocket/websocket.module';

@Module({
  imports: [DatabaseModule, ApiModule, WebsocketModule],
  controllers: [],
  providers: [],
})
export class RootModule {}
