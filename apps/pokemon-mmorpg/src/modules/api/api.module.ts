import { Module } from '@nestjs/common';

import { JwtManager } from '@nestjs-monorepo/common';

import { DatabaseModule } from '../database/database.module';
import { controllers } from './controllers';

@Module({
  controllers,
  imports: [DatabaseModule],
  providers: [JwtManager],
})
export class ApiModule {}
