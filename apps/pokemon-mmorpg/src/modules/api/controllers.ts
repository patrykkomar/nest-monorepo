import { PokemonController } from './controllers/pokemon.controller';
import { TrainerController } from './controllers/trainer.controller';
import { UserController } from './controllers/user.controller';

export const controllers = [
  PokemonController,
  TrainerController,
  UserController,
];
