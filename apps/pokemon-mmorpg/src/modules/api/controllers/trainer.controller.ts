import { Body, Controller, Delete, Get, Headers, Post, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';

import { JwtSignatureGuard, UseJwtSecret } from '@nestjs-monorepo/rest';

import { config } from '../../../setup/config';
import { Trainer } from '../../../shared/interfaces';
import { TrainerService, UserService } from '../../database/services';
import { TrainerCreateDto, TrainerRemoveDto } from '../dtos';

@Controller('/trainer')
@UseJwtSecret(config.jwt.secret)
export class TrainerController {
  constructor(private trainerService: TrainerService, private readonly userService: UserService) {}

  @Post('/create')
  @UseGuards(JwtSignatureGuard)
  async createTrainer(@Body() trainerCreateDto: TrainerCreateDto, @Headers('authorization') token: string): Promise<Partial<Trainer>> {
    const userId = this.userService.getUserIdFromAuthorizationToken(token);
    const { trainerId } = await this.trainerService.createTrainer(trainerCreateDto, userId);
    await this.userService.addTrainer(userId, trainerId);

    return { trainerId };
  }

  @Delete('/remove')
  @UseGuards(JwtSignatureGuard)
  async removeTrainer(@Body() { trainerId }: TrainerRemoveDto, @Headers('authorization') token: string, @Res() response: Response): Promise<void> {
    const userId = this.userService.getUserIdFromAuthorizationToken(token);
    await this.userService.removeTrainer(userId, trainerId);
    await this.trainerService.removeTrainer(trainerId);

    return response.status(204).end();
  }

  /* FIXME: Actually it should be done by websockets */
  @Get('/catch-pokemon')
  async addCaughtPokemon(): Promise<void> {
    return this.trainerService.addCaughtPokemon('5f2335d1b848d642b72ac5b8', 16);
  }
}
