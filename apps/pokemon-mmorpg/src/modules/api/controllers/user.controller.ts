import { Body, Controller, Delete, Headers, Post, Put, UseGuards, Res } from '@nestjs/common';
import { Response } from 'express';

import { JwtSignatureGuard, UseJwtSecret } from '@nestjs-monorepo/rest';

import { config } from '../../../setup/config';
import { User } from '../../../shared/interfaces';
import { PokemonService, UserService } from '../../database/services';
import { UserLoginDto, UserRegisterDto } from '../dtos';

@Controller('/user')
@UseJwtSecret(config.jwt.secret)
export class UserController {
  constructor(private userService: UserService, private pokemonService: PokemonService) {}

  @Post('/register')
  async register(@Body() userRegisterDto: UserRegisterDto): Promise<Partial<User>> {
    return this.userService.register(userRegisterDto);
  }

  @Put('/login')
  async login(@Body() userLoginDto: UserLoginDto): Promise<string> {
    return this.userService.login(userLoginDto);
  }

  @Delete('/logout')
  @UseGuards(JwtSignatureGuard)
  async logout(@Headers('authorization') authorizationToken: string, @Res() response: Response): Promise<void> {
    await this.userService.logout(authorizationToken);

    return response.status(204).end();
  }
}
