import { Controller, Get, Query, Param, Post } from '@nestjs/common';

import { Pokemon } from '../../../shared/interfaces';
import { PokemonService } from '../../database/services';

@Controller('/pokemon')
export class PokemonController {
  constructor(private pokemonService: PokemonService) {}

  @Post('/supersecret-reset-path')
  async resetPokemonDatabase(): Promise<void> {
    await this.pokemonService.resetPokemonDatabase();
  }

  @Get()
  async getPokemons(@Query('type') type: string): Promise<Partial<Pokemon>[]> {
    /* FIXME: Fix 'any' type - create an interface for that */
    const query: any = {};
    if (type) {
      query.type = { $eq: type };
    }

    return this.pokemonService.getPokemons(query, { moves: 0 });
  }

  @Get('/:number')
  async getPokemonByNumber(@Param('number') number: number): Promise<Partial<Pokemon>> {
    return this.pokemonService.getPokemonByNumber(number, { moves: 0 });
  }
}
