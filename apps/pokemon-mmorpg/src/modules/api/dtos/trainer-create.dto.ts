import { IsEnum, IsString } from 'class-validator';

import { Class, Gender } from '../../../shared/enums';

export class TrainerCreateDto {
  @IsString()
  readonly name: string;

  @IsString()
  @IsEnum(Gender)
  readonly gender: string;

  @IsString()
  @IsEnum(Class)
  readonly chosenClass: string;
}
