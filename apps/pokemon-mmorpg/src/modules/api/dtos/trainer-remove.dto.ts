import { IsMongoId, IsString } from 'class-validator';

export class TrainerRemoveDto {
  @IsString()
  @IsMongoId()
  readonly trainerId: string;
}
