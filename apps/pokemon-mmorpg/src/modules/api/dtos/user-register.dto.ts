import { IsBoolean, IsEmail, IsMongoId, IsOptional, IsString, Matches } from 'class-validator';

export class UserRegisterDto {
  @IsEmail()
  readonly email: string;

  @IsString()
  @Matches(/^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[.!@#$%^&*-]).{8,}$/)
  readonly password: string;

  @IsBoolean()
  @IsOptional()
  readonly communityMember: boolean;

  @IsString()
  @IsMongoId()
  @IsOptional()
  readonly referralId?: string;
}
