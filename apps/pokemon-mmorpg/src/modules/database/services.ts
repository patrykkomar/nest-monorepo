import { LocationService } from './services/location.service';
import { UserService } from './services/user.service';
import { PokemonService } from './services/pokemon.service';
import { OwnedPokemonService } from './services/owned-pokemon.service';
import { TrainerService } from './services/trainer.service';

const services = [
  PokemonService,
  OwnedPokemonService,
  TrainerService,
  UserService,
  LocationService,
];

export {
  LocationService,
  PokemonService,
  OwnedPokemonService,
  TrainerService,
  UserService,
  services,
};
