import { values } from 'lodash';
import { Schema } from 'mongoose';

import { mongooseArray, mongooseEnumArray, mongooseInteger, mongooseSchemaOptions, mongooseSubschema, mongooseBoolean } from '@nestjs-monorepo/common';

import { Type } from '../../../shared/enums';
import { Pokemon } from '../../../shared/interfaces';
import { statsSubschema, evolutionSubschema, moveSubschema, nameSubschema } from '../subschemas';

const pokemonObjectSchema = {
  _id: mongooseInteger(true, 1, Infinity, null, true, true),
  // number: mongooseInteger(),
  name: mongooseSubschema(nameSubschema),
  type: mongooseEnumArray(values(Type)),
  baseStats: mongooseSubschema(statsSubschema),
  moves: mongooseArray(mongooseSubschema(moveSubschema)),
  evolutions: mongooseArray(mongooseSubschema(evolutionSubschema)),
  legendary: mongooseBoolean(true, false),
};

export const pokemonSchema = new Schema<Pokemon>(pokemonObjectSchema, mongooseSchemaOptions);
