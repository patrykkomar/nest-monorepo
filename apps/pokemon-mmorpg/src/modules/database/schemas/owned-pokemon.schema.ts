import { values } from 'lodash';
import { Schema } from 'mongoose';

import {
  mongooseDate,
  mongooseEnum,
  mongooseInteger,
  mongooseObjectReference,
  mongooseSchemaOptions,
  mongooseSubschema,
} from '@nestjs-monorepo/common';

import { Gender } from '../../../shared/enums';
import { OwnedPokemon } from '../../../shared/interfaces';
import { statsSubschema } from '../subschemas';

const ownedPokemonObjectSchema = {
  pokemonId: mongooseInteger(),
  trainerId: mongooseObjectReference(true, true),
  gender: mongooseEnum(true, values(Gender)),
  level: mongooseInteger(true, 1, Infinity, 1),
  experience: mongooseInteger(true, 0, Infinity, 0),
  /* TODO: nextLevelExperience - different than trainers' ones */
  stats: mongooseSubschema(statsSubschema),
  health: mongooseInteger(),
  stamina: mongooseInteger(),
  caughtAt: mongooseDate(),
};

export const ownedPokemonSchema = new Schema<OwnedPokemon>(ownedPokemonObjectSchema, { ...mongooseSchemaOptions, collection: 'ownedPokemons' });
