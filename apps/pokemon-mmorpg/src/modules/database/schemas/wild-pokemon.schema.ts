import { values } from 'lodash';
import { Schema } from 'mongoose';

import {
  mongooseDate,
  mongooseEnum,
  mongooseInteger,
  mongooseObjectReference,
  mongooseSchemaOptions,
  mongooseSubschema,
} from '@nestjs-monorepo/common';

import { Gender, WildPokemonStatus } from '../../../shared/enums';
import { WildPokemon } from '../../../shared/interfaces';
import { locationSubschema } from '../subschemas';

const wildPokemonObjectSchema = {
  pokemonId: mongooseObjectReference(true, true),
  gender: mongooseEnum(true, values(Gender)),
  level: mongooseInteger(true, 1, Infinity, 1),
  status: mongooseEnum(true, values(WildPokemonStatus)),
  position: mongooseSubschema(locationSubschema),
  spawnedAt: mongooseDate(),
  defeatedAt: mongooseDate(false, null),
};

export const wildPokemonSchema = new Schema<WildPokemon>(wildPokemonObjectSchema, { ...mongooseSchemaOptions, collection: 'wildPokemons' });
