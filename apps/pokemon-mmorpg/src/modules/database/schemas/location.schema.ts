import { Schema } from 'mongoose';

import { mongooseArray, mongooseInteger, mongooseSchemaOptions, mongooseString, mongooseSubschema } from '@nestjs-monorepo/common';

import { Location } from '../../../shared/interfaces';
import { locationSubschema } from '../subschemas';

const locationObjectSchema = {
  name: mongooseString(true, /^.*$/, true, true),
  width: mongooseInteger(),
  height: mongooseInteger(),
  neighbours: mongooseArray(mongooseSubschema(locationSubschema)),
};

export const locationSchema = new Schema<Location>(locationObjectSchema, mongooseSchemaOptions);
