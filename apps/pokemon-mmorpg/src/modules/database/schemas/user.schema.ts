import { Schema, Types } from 'mongoose';

import { mongooseBoolean, mongooseDate, mongooseObjectReference, mongooseSchemaOptions, mongooseString, mongooseArray } from '@nestjs-monorepo/common';

import { User } from '../../../shared/interfaces';

const userObjectSchema = {
  email: mongooseString(),
  password: mongooseString(),
  trainersIds: mongooseArray(Types.ObjectId),
  premiumAccountExpiresAt: mongooseDate(false, null),
  communityMember: mongooseBoolean(true, false),
  referralId: mongooseObjectReference(false, true),
  registeredAt: mongooseDate(),
  lastLoginAt: mongooseDate(false, null),
};

export const userSchema = new Schema<User>(userObjectSchema, mongooseSchemaOptions);
