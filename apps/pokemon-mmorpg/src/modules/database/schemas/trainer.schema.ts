import { values } from 'lodash';
import { Document, Schema, Types } from 'mongoose';

import {
  mongooseArray,
  mongooseDate,
  mongooseEnum,
  mongooseInteger,
  mongooseObjectReference,
  mongooseSchemaOptions,
  mongooseString,
  mongooseSubschema,
} from '@nestjs-monorepo/common';

import { trainersExperienceThresholds } from '../../../shared/constants';
import { Class, Gender } from '../../../shared/enums';
import { Trainer } from '../../../shared/interfaces';
import { locationSubschema, skillSubschema } from '../subschemas';

const trainerObjectSchema = {
  name: mongooseString(true, /^.*$/, true, true, true),
  userId: mongooseObjectReference(true, true),
  gender: mongooseEnum(true, values(Gender)),
  class: mongooseEnum(true, values(Class)),
  level: mongooseInteger(true, 1, Infinity, 1),
  experience: mongooseInteger(true, 0, Infinity, 0),
  nextLevelExperience: mongooseInteger(true, 0, Infinity, trainersExperienceThresholds[2]),
  skills: mongooseArray(mongooseSubschema(skillSubschema)),
  skillPoints: mongooseInteger(),
  pokemonsInPocket: mongooseArray(Types.ObjectId),
  pokemonsInBackpack: mongooseArray(Types.ObjectId),
  pokemonsInCenter: mongooseArray(Types.ObjectId),
  inventory: mongooseArray(Types.ObjectId),
  badges: mongooseArray(Types.ObjectId),
  currentPosition: mongooseSubschema(locationSubschema),
  createdAt: mongooseDate(),
};

const trainerSchema = new Schema<Trainer>(trainerObjectSchema, mongooseSchemaOptions);

trainerSchema.post('findOneAndUpdate', async function(document: Trainer) {
  // console.log(document);
});

export { trainerSchema };
