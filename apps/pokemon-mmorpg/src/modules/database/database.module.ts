import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { JwtManager } from '@nestjs-monorepo/common';

import { config } from '../../setup/config';
import { Collection } from '../../shared/enums';
import { locationSchema, ownedPokemonSchema, pokemonSchema, trainerSchema, userSchema, wildPokemonSchema } from './schemas';
import { services } from './services';

@Module({
  imports: [
    MongooseModule.forRoot(config.mongo.url, { useCreateIndex: true, useFindAndModify: false, useNewUrlParser: true, useUnifiedTopology: true }),
    MongooseModule.forFeature([
      {
        name: Collection.USER,
        schema: userSchema,
      },
      {
        name: Collection.TRAINER,
        schema: trainerSchema,
      },
      {
        name: Collection.POKEMON,
        schema: pokemonSchema,
      },
      {
        name: Collection.LOCATION,
        schema: locationSchema,
      },
      {
        name: Collection.WILD_POKEMON,
        schema: wildPokemonSchema,
      },
      {
        name: Collection.OWNED_POKEMON,
        schema: ownedPokemonSchema,
      },
    ]),
  ],
  providers: [...services, JwtManager],
  exports: services,
})
export class DatabaseModule {}
