import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Collection } from '../../../shared/enums';
import { WildPokemon } from '../../../shared/interfaces';

@Injectable()
export class WildPokemonService {
  constructor(@InjectModel(Collection.WILD_POKEMON) private readonly wildPokemonModel: Model<WildPokemon>) { }
}
