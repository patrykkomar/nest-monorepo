import { ConflictException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { caughtPokemonLimits, totalPokemonsNumber, trainersExperienceThresholds } from '../../../shared/constants';
import { Class, Collection, Gender } from '../../../shared/enums';
import { CurrentPosition, Trainer } from '../../../shared/interfaces';
import { TrainerCreateDto } from '../../api/dtos';
import { LocationService, OwnedPokemonService, PokemonService, UserService } from '../services';

@Injectable()
export class TrainerService {
  constructor(@InjectModel(Collection.TRAINER) private readonly trainerModel: Model<Trainer>,
    private readonly userService: UserService, private readonly locationService: LocationService,
    private readonly pokemonService: PokemonService, private readonly ownedPokemonService: OwnedPokemonService) { }

  async findTrainerById(trainerId: string | Types.ObjectId): Promise<Trainer> {
    return this.trainerModel.findOne({ _id: trainerId });
  }

  async trainerExists(name: string): Promise<boolean> {
    const trainer = await this.trainerModel.findOne({ name });

    return trainer !== null;
  }

  async createTrainer({ name, gender, chosenClass }: TrainerCreateDto, userId: string | Types.ObjectId): Promise<Partial<Trainer>> {
    if (await this.trainerExists(name)) {
      throw new ConflictException(`Trainer ${name} already exists.`);
    }

    const startingLocation = await this.locationService.getStartingLocationForClass(chosenClass as Class);

    const trainer = new this.trainerModel({
      userId,
      name,
      gender,
      class: chosenClass,
      currentPosition: startingLocation,
    });

    const createdTrainer = await trainer.save();

    return { trainerId: createdTrainer._id };
  }

  async updateTrainerPosition(trainerId: string | Types.ObjectId, newPosition: CurrentPosition): Promise<void> {
    console.log('Updatuję pozycję trenera', newPosition);
    newPosition.locationId = Types.ObjectId(newPosition.locationId);
    await this.trainerModel.updateOne({ _id: trainerId }, { $set: { currentPosition: newPosition } });
  }

  /* FIXME: Fix typings - void */
  async addCaughtPokemon(trainerId: string | Types.ObjectId, pokemonId: number): Promise<any> {
    if (pokemonId < 1 || pokemonId > totalPokemonsNumber) {
      return;
    }

    const trainer = await this.findTrainerById(trainerId);

    if (!trainer) {
      return;
    }

    const pocketFreeSlot = trainer.pokemonsInPocket.length < caughtPokemonLimits.pocket.base;
    const backpackFreeSlot = trainer.pokemonsInBackpack.length < caughtPokemonLimits.backpack.base;

    if (!pocketFreeSlot && !backpackFreeSlot) {
      return;
    }

    const freeSlotProperty = backpackFreeSlot ? 'pokemonsInBackpack' : 'pokemonsInPocket';
    const ownedPokemonId = await this.ownedPokemonService.createNewOwnedPokemon({ pokemonId: 4, gender: Gender.MALE }, Types.ObjectId(trainerId));
    await this.trainerModel.findOneAndUpdate({ _id: trainerId }, { $push: { [freeSlotProperty]: Types.ObjectId(ownedPokemonId) } }).exec();

    return;
  }

  async addExperience(trainerId: string | Types.ObjectId, experienceToAdd: number): Promise<void> {
    await this.trainerModel.updateOne({ _id: trainerId }, { $inc: { experience: experienceToAdd } });
  }

  async updateTrainerObject(trainerId: string | Types.ObjectId, trainerObject: Partial<Trainer>): Promise<void> {
    await this.trainerModel.updateOne({ _id: trainerId }, { $set: trainerObject });
  }

  async removeTrainer(trainerId: string | Types.ObjectId): Promise<void> {
    await this.trainerModel.deleteOne({ _id: trainerId });
  }
}
