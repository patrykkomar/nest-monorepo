import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { Collection } from '../../../shared/enums';
import { OwnedPokemon, WildPokemon } from '../../../shared/interfaces';
import { PokemonService } from '../services';

@Injectable()
export class OwnedPokemonService {
  constructor(@InjectModel(Collection.OWNED_POKEMON) private readonly ownedPokemonModel: Model<OwnedPokemon>,
    private readonly pokemonService: PokemonService) { }

  async createNewOwnedPokemon(defeatedPokemon: Partial<WildPokemon>, trainerId: Types.ObjectId): Promise<string | Types.ObjectId> {
    const pokemon = await this.pokemonService.getPokemonByNumber(defeatedPokemon.pokemonId);

    const ownedPokemon = new this.ownedPokemonModel({
      pokemonId: defeatedPokemon.pokemonId,
      trainerId,
      gender: defeatedPokemon.gender,
      level: 1,
      experience: 0,
      /* TODO: nextLevelExperience */
      stats: pokemon.baseStats,
      health: 0,
      stamina: 0,
    });

    const createdOwnedPokemon = await ownedPokemon.save();

    return createdOwnedPokemon._id;
  }
}
