import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Collection } from '../../../shared/enums';
import { Pokemon } from '../../../shared/interfaces';
import { pokemons } from '../collections/pokemon.collection';

@Injectable()
export class PokemonService {
  constructor(@InjectModel(Collection.POKEMON) private readonly pokemonModel: Model<Pokemon>) { }

  async resetPokemonDatabase(): Promise<void> {
    await this.pokemonModel.deleteMany({});
    await this.pokemonModel.insertMany(pokemons);

    return;
  }

  async getAllPokemons(projection = {}): Promise<Partial<Pokemon>[]> {
    return this.pokemonModel.find({}, projection);
  }

  async getPokemons(query = {}, projection = {}): Promise<Partial<Pokemon>[]> {
    return this.pokemonModel.find(query, projection);
  }

  async getPokemonByNumber(number: number, projection = {}): Promise<Partial<Pokemon>> {
    return this.pokemonModel.findOne({ _id: number }, projection);
  }
}
