import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Class, Collection, LocationName } from '../../../shared/enums';
import { CurrentPosition, Location } from '../../../shared/interfaces';

@Injectable()
export class LocationService {
  constructor(@InjectModel(Collection.LOCATION) private readonly locationModel: Model<Location>) { }

  async getStartingLocationForClass(trainerClass: Class): Promise<CurrentPosition> {
    const mapping = {
      [Class.HUNTER]: LocationName.ACCUMULA_TOWN,
      [Class.TRAVELER]: LocationName.AMBRETTE_TOWN,
      [Class.COLLECTOR]: LocationName.ANISTAR_CITY,
    };

    const name = mapping[trainerClass];
    const location = await this.locationModel.findOne({ name });

    return {
      locationId: location._id,
      x: 50,
      y: 50,
    };
  }
}
