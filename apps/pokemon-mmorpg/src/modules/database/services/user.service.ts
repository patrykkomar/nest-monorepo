import { Injectable, ConflictException, BadRequestException, UnauthorizedException, ForbiddenException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { get, isNil, set } from 'lodash';
import { Model, Types } from 'mongoose';

import { JwtManager, RedisService } from '@nestjs-monorepo/common';

import { UserLoginDto, UserRegisterDto } from '../../api/dtos';
import { config } from '../../../setup/config';
import { Collection } from '../../../shared/enums';
import { User } from '../../../shared/interfaces';

@Injectable()
export class UserService {
  private redisService: RedisService;

  constructor(@InjectModel(Collection.USER) private readonly userModel: Model<User>,
    private readonly jwtManager: JwtManager) {
      if (config.redis) {
        this.redisService = new RedisService(config.redis);
      }
  }

  async findUserById(_id: string): Promise<User> {
    return this.userModel.findOne({ _id });
  }

  async userExists(userId: string): Promise<boolean> {
    const user = await this.findUserById(userId);

    return !isNil(user);
  }

  async validateNewUserPassword(password: string): Promise<void> {
    /* FIXME: Improve password validation */
    if (!password) {
      throw new BadRequestException('Password strength is too weak.');
    }

    return;
  }

  async validateNewUserEmail(email: string): Promise<void> {
    const existingUser = await this.userModel.findOne({ email });

    if (existingUser) {
      throw new ConflictException('User for this e-mail address is already registered.');
    }
  }

  async register({ email, password, communityMember, referralId }: UserRegisterDto): Promise<Partial<User>> {
    await this.validateNewUserPassword(password);
    await this.validateNewUserEmail(email);

    const passwordHash = await bcrypt.hash(password, 10);

    const userObject: User = {
      email,
      password: passwordHash,
      communityMember,
    };

    if (referralId) {
      const referralUser = await this.findUserById(referralId);
      if (referralUser) {
        set(userObject, 'referralId', referralId);
      }
    }

    const user = new this.userModel(userObject);

    const createdUser = await user.save();

    return { userId: createdUser._id };
  }

  getUserIdFromAuthorizationToken(authorizationToken: string): string {
    return get(this.jwtManager.decodePayload(authorizationToken), 'userId');
  }

  generateRedisKeyForAuthorizationToken(userId: string): string {
    return `token:${userId}`;
  }

  async login({ email, password }: UserLoginDto): Promise<string> {
    const { expiresIn, expirationRefresh } = config.jwt;
    const user = await this.userModel.findOne({ email });

    if (!user || !await bcrypt.compare(password, user.password)) {
      throw new UnauthorizedException('Incorrect email or password.');
    }

    const authorizationTokenRedisKey = this.generateRedisKeyForAuthorizationToken(user._id);

    let authorizationToken = (await this.redisService.get(authorizationTokenRedisKey)) as string;

    if (!authorizationToken) {
      authorizationToken = this.jwtManager.generateToken({ userId: user._id }, config.jwt.secret, { expiresIn });
      await this.redisService.set(authorizationTokenRedisKey, authorizationToken, { expireAfterSeconds: expiresIn });
    } else if (await this.redisService.ttl(authorizationTokenRedisKey) < expirationRefresh) {
      await this.redisService.expire(authorizationTokenRedisKey, config.jwt.expiresIn);
    }

    return authorizationToken;
  }

  async logout(authorizationToken: string): Promise<void> {
    const userId = this.getUserIdFromAuthorizationToken(authorizationToken);
    const authorizationTokenRedisKey = this.generateRedisKeyForAuthorizationToken(userId);
    await this.redisService.del(authorizationTokenRedisKey);
  }

  async addTrainer(userId: string | Types.ObjectId, trainerId: string | Types.ObjectId): Promise<void> {
    await this.userModel.updateOne({ _id: userId }, { $push: { trainersIds: trainerId } });
  }

  async removeTrainer(userId: string | Types.ObjectId, trainerId: string | Types.ObjectId): Promise<void> {
    const user = await this.userModel.findOneAndUpdate({ _id: userId, trainersIds: { $eq: trainerId } }, { $pull: { trainersIds: trainerId } });

    if (!user) {
      throw new ForbiddenException();
    }
  }
}
