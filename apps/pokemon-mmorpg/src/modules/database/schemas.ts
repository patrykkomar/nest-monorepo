export { userSchema } from './schemas/user.schema';
export { trainerSchema } from './schemas/trainer.schema';
export { pokemonSchema } from './schemas/pokemon.schema';
export { locationSchema } from './schemas/location.schema';
export { wildPokemonSchema } from './schemas/wild-pokemon.schema';
export { ownedPokemonSchema } from './schemas/owned-pokemon.schema';
