import { Schema } from 'mongoose';

import { mongooseInteger, mongooseObjectReference } from '@nestjs-monorepo/common';

export const locationSubschema = new Schema({
  locationId: mongooseObjectReference(),
  x: mongooseInteger(),
  y: mongooseInteger(),
}, { _id : false });
