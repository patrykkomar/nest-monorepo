import { Schema } from 'mongoose';

import { mongooseString } from '@nestjs-monorepo/common';

import { Name } from '../../../shared/interfaces';

export const nameSubschema = new Schema<Name>({
  english: mongooseString(),
  japanese: mongooseString(),
  chinese: mongooseString(),
}, { _id : false });
