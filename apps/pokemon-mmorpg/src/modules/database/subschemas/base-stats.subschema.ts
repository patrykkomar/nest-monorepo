import { Schema } from 'mongoose';

import { mongooseInteger } from '@nestjs-monorepo/common';

import { BaseStats } from '../../../shared/interfaces';

export const statsSubschema = new Schema<BaseStats>({
  healthPoints: mongooseInteger(),
  attack: mongooseInteger(),
  defense: mongooseInteger(),
  specialAttack: mongooseInteger(),
  specialDefense: mongooseInteger(),
  speed: mongooseInteger(),
}, { _id : false });
