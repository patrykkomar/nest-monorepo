import { Schema } from 'mongoose';

import { mongooseInteger, mongooseObjectReference } from '@nestjs-monorepo/common';

import { Name } from '../../../shared/interfaces';

export const skillSubschema = new Schema<Name>({
  skillId: mongooseObjectReference(),
  level: mongooseInteger(true, 1, Infinity, 1),
}, { _id : false });
