import { Schema } from 'mongoose';

import { mongooseInteger, mongooseString, mongooseSubschema } from '@nestjs-monorepo/common';

export const moveRequirementsSubschema = new Schema({
  type: mongooseString(),
  value: mongooseInteger(),
}, { _id : false });

export const moveSubschema = new Schema({
  name: mongooseString(),
  requirements: mongooseSubschema(moveRequirementsSubschema),
}, { _id : false });
