import { values } from 'lodash';
import { Schema } from 'mongoose';

import { mongooseEnum, mongooseInteger, mongooseMixed, mongooseSubschema } from '@nestjs-monorepo/common';

import { Requirement } from '../../../shared/enums';

export const evolutionRequirementsSubschema = new Schema({
  type: mongooseEnum(true, values(Requirement)),
  value: mongooseMixed(false),
}, { _id : false });

export const evolutionSubschema = new Schema({
  number: mongooseInteger(),
  requirements: mongooseSubschema(evolutionRequirementsSubschema),
}, { _id : false });

