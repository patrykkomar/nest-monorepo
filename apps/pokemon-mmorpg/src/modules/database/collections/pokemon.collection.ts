import { Pokemon } from '../../../shared/interfaces';

export const pokemons: Pokemon[] = [
  {
    _id: 1,
    name: {
      english: 'Bulbasaur',
      japanese: 'フシギダネ',
      chinese: '妙蛙种子',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 45,
      attack: 49,
      defense: 49,
      specialAttack: 65,
      specialDefense: 65,
      speed: 45,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Leech Seed',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Razor Leaf',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Worry Seed',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Synthesis',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Seed Bomb',
      requirements: {
        type: 'level',
        value: 37,
      },
    }],
    evolutions: [{
      number: 2,
      requirements: {
        type: 'level',
        value: 16,
      },
    }],
  }, {
    _id: 2,
    name: {
      english: 'Ivysaur',
      japanese: 'フシギソウ',
      chinese: '妙蛙草',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 60,
      attack: 62,
      defense: 63,
      specialAttack: 80,
      specialDefense: 80,
      speed: 60,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Leech Seed',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Razor Leaf',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Worry Seed',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Synthesis',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Solar Beam',
      requirements: {
        type: 'level',
        value: 44,
      },
    }],
    evolutions: [{
      number: 3,
      requirements: {
        type: 'level',
        value: 36,
      },
    }],
  }, {
    _id: 3,
    name: {
      english: 'Venusaur',
      japanese: 'フシギバナ',
      chinese: '妙蛙花',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 80,
      attack: 82,
      defense: 83,
      specialAttack: 100,
      specialDefense: 100,
      speed: 80,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Leech Seed',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Razor Leaf',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Worry Seed',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Synthesis',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Petal Blizzard',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Solar Beam',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [],
  }, {
    _id: 4,
    name: {
      english: 'Charmander',
      japanese: 'ヒトカゲ',
      chinese: '小火龙',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 39,
      attack: 52,
      defense: 43,
      specialAttack: 60,
      specialDefense: 50,
      speed: 65,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Flame Burst',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Inferno',
      requirements: {
        type: 'level',
        value: 46,
      },
    }],
    evolutions: [{
      number: 5,
      requirements: {
        type: 'level',
        value: 16,
      },
    }],
  }, {
    _id: 5,
    name: {
      english: 'Charmeleon',
      japanese: 'リザード',
      chinese: '火恐龙',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 58,
      attack: 64,
      defense: 58,
      specialAttack: 80,
      specialDefense: 65,
      speed: 80,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Flame Burst',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Inferno',
      requirements: {
        type: 'level',
        value: 54,
      },
    }],
    evolutions: [{
      number: 6,
      requirements: {
        type: 'level',
        value: 36,
      },
    }],
  }, {
    _id: 6,
    name: {
      english: 'Charizard',
      japanese: 'リザードン',
      chinese: '喷火龙',
    },
    type: ['fire', 'flying'],
    baseStats: {
      healthPoints: 78,
      attack: 84,
      defense: 78,
      specialAttack: 109,
      specialDefense: 85,
      speed: 100,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Dragon Claw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Shadow Claw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Flame Burst',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Inferno',
      requirements: {
        type: 'level',
        value: 62,
      },
    }, {
      name: 'Heat Wave',
      requirements: {
        type: 'level',
        value: 71,
      },
    }, {
      name: 'Flare Blitz',
      requirements: {
        type: 'level',
        value: 77,
      },
    }],
    evolutions: [],
  }, {
    _id: 7,
    name: {
      english: 'Squirtle',
      japanese: 'ゼニガメ',
      chinese: '杰尼龟',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 44,
      attack: 48,
      defense: 65,
      specialAttack: 50,
      specialDefense: 64,
      speed: 43,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Rapid Spin',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Skull Bash',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Iron Defense',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 40,
      },
    }],
    evolutions: [{
      number: 8,
      requirements: {
        type: 'level',
        value: 16,
      },
    }],
  }, {
    _id: 8,
    name: {
      english: 'Wartortle',
      japanese: 'カメール',
      chinese: '卡咪龟',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 59,
      attack: 63,
      defense: 80,
      specialAttack: 65,
      specialDefense: 80,
      speed: 58,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Rapid Spin',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Skull Bash',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Iron Defense',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 49,
      },
    }],
    evolutions: [{
      number: 9,
      requirements: {
        type: 'level',
        value: 36,
      },
    }],
  }, {
    _id: 9,
    name: {
      english: 'Blastoise',
      japanese: 'カメックス',
      chinese: '水箭龟',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 79,
      attack: 83,
      defense: 100,
      specialAttack: 85,
      specialDefense: 105,
      speed: 78,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Aqua Jet',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 65,
      },
    }, {
      name: 'Skull Bash',
      requirements: {
        type: 'level',
        value: 75,
      },
    }],
    evolutions: [],
  }, {
    _id: 10,
    name: {
      english: 'Caterpie',
      japanese: 'キャタピー',
      chinese: '绿毛虫',
    },
    type: ['bug'],
    baseStats: {
      healthPoints: 45,
      attack: 30,
      defense: 35,
      specialAttack: 20,
      specialDefense: 20,
      speed: 45,
    },
    moves: [{
      name: 'String Shot',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bug Bite',
      requirements: {
        type: 'level',
        value: 9,
      },
    }],
    evolutions: [{
      number: 11,
      requirements: {
        type: 'level',
        value: 7,
      },
    }],
  }, {
    _id: 11,
    name: {
      english: 'Metapod',
      japanese: 'トランセル',
      chinese: '铁甲蛹',
    },
    type: ['bug'],
    baseStats: {
      healthPoints: 50,
      attack: 20,
      defense: 55,
      specialAttack: 25,
      specialDefense: 25,
      speed: 30,
    },
    moves: [{
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [{
      number: 12,
      requirements: {
        type: 'level',
        value: 10,
      },
    }],
  }, {
    _id: 12,
    name: {
      english: 'Butterfree',
      japanese: 'バタフリー',
      chinese: '巴大蝶',
    },
    type: ['bug', 'flying'],
    baseStats: {
      healthPoints: 60,
      attack: 45,
      defense: 50,
      specialAttack: 90,
      specialDefense: 80,
      speed: 70,
    },
    moves: [{
      name: 'Gust',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Silver Wind',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Whirlwind',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Bug Buzz',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Rage Powder',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Tailwind',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Quiver Dance',
      requirements: {
        type: 'level',
        value: 47,
      },
    }],
    evolutions: [],
  }, {
    _id: 13,
    name: {
      english: 'Weedle',
      japanese: 'ビードル',
      chinese: '独角虫',
    },
    type: ['bug', 'poison'],
    baseStats: {
      healthPoints: 40,
      attack: 35,
      defense: 30,
      specialAttack: 20,
      specialDefense: 20,
      speed: 50,
    },
    moves: [{
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'String Shot',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bug Bite',
      requirements: {
        type: 'level',
        value: 9,
      },
    }],
    evolutions: [{
      number: 14,
      requirements: {
        type: 'level',
        value: 7,
      },
    }],
  }, {
    _id: 14,
    name: {
      english: 'Kakuna',
      japanese: 'コクーン',
      chinese: '铁壳蛹',
    },
    type: ['bug', 'poison'],
    baseStats: {
      healthPoints: 45,
      attack: 25,
      defense: 50,
      specialAttack: 25,
      specialDefense: 25,
      speed: 35,
    },
    moves: [{
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [{
      number: 15,
      requirements: {
        type: 'level',
        value: 10,
      },
    }],
  }, {
    _id: 15,
    name: {
      english: 'Beedrill',
      japanese: 'スピアー',
      chinese: '大针蜂',
    },
    type: ['bug', 'poison'],
    baseStats: {
      healthPoints: 65,
      attack: 90,
      defense: 40,
      specialAttack: 45,
      specialDefense: 80,
      speed: 75,
    },
    moves: [{
      name: 'Twineedle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Venoshock',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Pin Missile',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Fell Stinger',
      requirements: {
        type: 'level',
        value: 44,
      },
    }],
    evolutions: [],
  }, {
    _id: 16,
    name: {
      english: 'Pidgey',
      japanese: 'ポッポ',
      chinese: '波波',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 40,
      attack: 45,
      defense: 40,
      specialAttack: 35,
      specialDefense: 35,
      speed: 56,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Gust',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Whirlwind',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Feather Dance',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Tailwind',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Mirror Move',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [{
      number: 17,
      requirements: {
        type: 'level',
        value: 18,
      },
    }],
  }, {
    _id: 17,
    name: {
      english: 'Pidgeotto',
      japanese: 'ピジョン',
      chinese: '比比鸟',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 63,
      attack: 60,
      defense: 55,
      specialAttack: 50,
      specialDefense: 50,
      speed: 71,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Gust',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Whirlwind',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Feather Dance',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Tailwind',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Mirror Move',
      requirements: {
        type: 'level',
        value: 52,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 62,
      },
    }],
    evolutions: [{
      number: 18,
      requirements: {
        type: 'level',
        value: 36,
      },
    }],
  }, {
    _id: 18,
    name: {
      english: 'Pidgeot',
      japanese: 'ピジョット',
      chinese: '大比鸟',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 83,
      attack: 80,
      defense: 75,
      specialAttack: 70,
      specialDefense: 70,
      speed: 101,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Gust',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Whirlwind',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Feather Dance',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Tailwind',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Mirror Move',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 62,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 68,
      },
    }],
    evolutions: [],
  }, {
    _id: 19,
    name: {
      english: 'Rattata',
      japanese: 'コラッタ',
      chinese: '小拉达',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 30,
      attack: 56,
      defense: 35,
      specialAttack: 25,
      specialDefense: 35,
      speed: 72,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Hyper Fang',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Super Fang',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 34,
      },
    }],
    evolutions: [{
      number: 20,
      requirements: {
        type: 'level',
        value: 20,
      },
    }],
  }, {
    _id: 20,
    name: {
      english: 'Raticate',
      japanese: 'ラッタ',
      chinese: '拉达',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 55,
      attack: 81,
      defense: 60,
      specialAttack: 50,
      specialDefense: 70,
      speed: 97,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Hyper Fang',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Super Fang',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 44,
      },
    }],
    evolutions: [],
  }, {
    _id: 21,
    name: {
      english: 'Spearow',
      japanese: 'オニスズメ',
      chinese: '烈雀',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 40,
      attack: 60,
      defense: 30,
      specialAttack: 31,
      specialDefense: 31,
      speed: 70,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Aerial Ace',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mirror Move',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Drill Peck',
      requirements: {
        type: 'level',
        value: 36,
      },
    }],
    evolutions: [{
      number: 22,
      requirements: {
        type: 'level',
        value: 20,
      },
    }],
  }, {
    _id: 22,
    name: {
      english: 'Fearow',
      japanese: 'オニドリル',
      chinese: '大嘴雀',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 65,
      attack: 90,
      defense: 65,
      specialAttack: 61,
      specialDefense: 61,
      speed: 100,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pluck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Aerial Ace',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mirror Move',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Drill Peck',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Drill Run',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [],
  }, {
    _id: 23,
    name: {
      english: 'Ekans',
      japanese: 'アーボ',
      chinese: '阿柏蛇',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 35,
      attack: 60,
      defense: 44,
      specialAttack: 40,
      specialDefense: 54,
      speed: 55,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Glare',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Spit Up',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Stockpile',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Swallow',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Acid Spray',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Gastro Acid',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Belch',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Coil',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Gunk Shot',
      requirements: {
        type: 'level',
        value: 49,
      },
    }],
    evolutions: [{
      number: 24,
      requirements: {
        type: 'level',
        value: 22,
      },
    }],
  }, {
    _id: 24,
    name: {
      english: 'Arbok',
      japanese: 'アーボック',
      chinese: '阿柏怪',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 60,
      attack: 95,
      defense: 69,
      specialAttack: 65,
      specialDefense: 79,
      speed: 80,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ice Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Glare',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Spit Up',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Stockpile',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Swallow',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Acid Spray',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Gastro Acid',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Belch',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Coil',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Gunk Shot',
      requirements: {
        type: 'level',
        value: 63,
      },
    }],
    evolutions: [],
  }, {
    _id: 25,
    name: {
      english: 'Pikachu',
      japanese: 'ピカチュウ',
      chinese: '皮卡丘',
    },
    type: ['electric'],
    baseStats: {
      healthPoints: 35,
      attack: 55,
      defense: 40,
      specialAttack: 50,
      specialDefense: 50,
      speed: 90,
    },
    moves: [{
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Play Nice',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Electro Ball',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Double Team',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Spark',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Nuzzle',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Thunderbolt',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Wild Charge',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Thunder',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [{
      number: 26,
      requirements: {
        type: 'stone',
        value: 'thunder',
      },
    }],
  }, {
    _id: 26,
    name: {
      english: 'Raichu',
      japanese: 'ライチュウ',
      chinese: '雷丘',
    },
    type: ['electric'],
    baseStats: {
      healthPoints: 60,
      attack: 90,
      defense: 55,
      specialAttack: 90,
      specialDefense: 80,
      speed: 110,
    },
    moves: [{
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunderbolt',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [],
  }, {
    _id: 27,
    name: {
      english: 'Sandshrew',
      japanese: 'サンド',
      chinese: '穿山鼠',
    },
    type: ['ground'],
    baseStats: {
      healthPoints: 50,
      attack: 75,
      defense: 85,
      specialAttack: 20,
      specialDefense: 30,
      speed: 40,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Rapid Spin',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Fury Cutter',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Sand Tomb',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Dig',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Sandstorm',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 46,
      },
    }],
    evolutions: [{
      number: 28,
      requirements: {
        type: 'level',
        value: 22,
      },
    }],
  }, {
    _id: 28,
    name: {
      english: 'Sandslash',
      japanese: 'サンドパン',
      chinese: '穿山王',
    },
    type: ['ground'],
    baseStats: {
      healthPoints: 75,
      attack: 100,
      defense: 110,
      specialAttack: 45,
      specialDefense: 55,
      speed: 65,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Crush Claw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Rapid Spin',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Fury Cutter',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Sand Tomb',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Dig',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Sandstorm',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [],
  }, {
    _id: 29,
    name: {
      english: 'Nidoran♀',
      japanese: 'ニドラン♀',
      chinese: '尼多兰',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 55,
      attack: 47,
      defense: 52,
      specialAttack: 40,
      specialDefense: 40,
      speed: 41,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Flatter',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Poison Fang',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 30,
      requirements: {
        type: 'level',
        value: 16,
      },
    }],
  }, {
    _id: 30,
    name: {
      english: 'Nidorina',
      japanese: 'ニドリーナ',
      chinese: '尼多娜',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 70,
      attack: 62,
      defense: 67,
      specialAttack: 55,
      specialDefense: 55,
      speed: 56,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Flatter',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Poison Fang',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [{
      number: 31,
      requirements: {
        type: 'stone',
        value: 'moon',
      },
    }],
  }, {
    _id: 31,
    name: {
      english: 'Nidoqueen',
      japanese: 'ニドクイン',
      chinese: '尼多后',
    },
    type: ['poison', 'ground'],
    baseStats: {
      healthPoints: 90,
      attack: 92,
      defense: 87,
      specialAttack: 75,
      specialDefense: 85,
      speed: 76,
    },
    moves: [{
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Superpower',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Earth Power',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Superpower',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [],
  }, {
    _id: 32,
    name: {
      english: 'Nidoran♂',
      japanese: 'ニドラン♂',
      chinese: '尼多朗',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 46,
      attack: 57,
      defense: 40,
      specialAttack: 40,
      specialDefense: 40,
      speed: 50,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Flatter',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Horn Drill',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 33,
      requirements: {
        type: 'level',
        value: 16,
      },
    }],
  }, {
    _id: 33,
    name: {
      english: 'Nidorino',
      japanese: 'ニドリーノ',
      chinese: '尼多力诺',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 61,
      attack: 72,
      defense: 57,
      specialAttack: 55,
      specialDefense: 55,
      speed: 65,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Flatter',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Horn Drill',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [{
      number: 34,
      requirements: {
        type: 'stone',
        value: 'moon',
      },
    }],
  }, {
    _id: 34,
    name: {
      english: 'Nidoking',
      japanese: 'ニドキング',
      chinese: '尼多王',
    },
    type: ['poison', 'ground'],
    baseStats: {
      healthPoints: 81,
      attack: 102,
      defense: 77,
      specialAttack: 85,
      specialDefense: 75,
      speed: 85,
    },
    moves: [{
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Earth Power',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Megahorn',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [],
  }, {
    _id: 35,
    name: {
      english: 'Clefairy',
      japanese: 'ピッピ',
      chinese: '皮皮',
    },
    type: ['fairy'],
    baseStats: {
      healthPoints: 70,
      attack: 45,
      defense: 48,
      specialAttack: 60,
      specialDefense: 65,
      speed: 35,
    },
    moves: [{
      name: 'Disarming Voice',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Encore',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spotlight',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sing',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Follow Me',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Bestow',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Minimize',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Stored Power',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Metronome',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Cosmic Power',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Lucky Chant',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Moonlight',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Moonblast',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Gravity',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Meteor Mash',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Healing Wish',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'After You',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [{
      number: 36,
      requirements: {
        type: 'stone',
        value: 'moon',
      },
    }],
  }, {
    _id: 36,
    name: {
      english: 'Clefable',
      japanese: 'ピクシー',
      chinese: '皮可西',
    },
    type: ['fairy'],
    baseStats: {
      healthPoints: 95,
      attack: 70,
      defense: 73,
      specialAttack: 95,
      specialDefense: 90,
      speed: 60,
    },
    moves: [{
      name: 'Disarming Voice',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Metronome',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Minimize',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sing',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spotlight',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [],
  }, {
    _id: 37,
    name: {
      english: 'Vulpix',
      japanese: 'ロコン',
      chinese: '六尾',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 38,
      attack: 41,
      defense: 40,
      specialAttack: 50,
      specialDefense: 65,
      speed: 65,
    },
    moves: [{
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Roar',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Baby-Doll Eyes',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Payback',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Will-O-Wisp',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Feint Attack',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Hex',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Flame Burst',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Extrasensory',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Imprison',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Fire Blast',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Grudge',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Inferno',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 38,
      requirements: {
        type: 'stone',
        value: 'fire',
      },
    }],
  }, {
    _id: 38,
    name: {
      english: 'Ninetales',
      japanese: 'キュウコン',
      chinese: '九尾',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 73,
      attack: 76,
      defense: 75,
      specialAttack: 81,
      specialDefense: 100,
      speed: 100,
    },
    moves: [{
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Imprison',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Nasty Plot',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [],
  }, {
    _id: 39,
    name: {
      english: 'Jigglypuff',
      japanese: 'プリン',
      chinese: '胖丁',
    },
    type: ['normal', 'fairy'],
    baseStats: {
      healthPoints: 115,
      attack: 45,
      defense: 20,
      specialAttack: 45,
      specialDefense: 25,
      speed: 20,
    },
    moves: [{
      name: 'Sing',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Play Nice',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Disarming Voice',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Round',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Spit Up',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Stockpile',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Swallow',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Rest',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Mimic',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Hyper Voice',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 40,
      requirements: {
        type: 'stone',
        value: 'moon',
      },
    }],
  }, {
    _id: 40,
    name: {
      english: 'Wigglytuff',
      japanese: 'プクリン',
      chinese: '胖可丁',
    },
    type: ['normal', 'fairy'],
    baseStats: {
      healthPoints: 140,
      attack: 70,
      defense: 45,
      specialAttack: 85,
      specialDefense: 50,
      speed: 45,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Play Rough',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sing',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [],
  }, {
    _id: 41,
    name: {
      english: 'Zubat',
      japanese: 'ズバット',
      chinese: '超音蝠',
    },
    type: ['poison', 'flying'],
    baseStats: {
      healthPoints: 40,
      attack: 45,
      defense: 35,
      specialAttack: 30,
      specialDefense: 40,
      speed: 55,
    },
    moves: [{
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Astonish',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Air Cutter',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Poison Fang',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Mean Look',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Leech Life',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Venoshock',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Quick Guard',
      requirements: {
        type: 'level',
        value: 43,
      },
    }],
    evolutions: [{
      number: 42,
      requirements: {
        type: 'level',
        value: 22,
      },
    }],
  }, {
    _id: 42,
    name: {
      english: 'Golbat',
      japanese: 'ゴルバット',
      chinese: '大嘴蝠',
    },
    type: ['poison', 'flying'],
    baseStats: {
      healthPoints: 75,
      attack: 80,
      defense: 70,
      specialAttack: 65,
      specialDefense: 75,
      speed: 90,
    },
    moves: [{
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Astonish',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Air Cutter',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Poison Fang',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Mean Look',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Leech Life',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Venoshock',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Quick Guard',
      requirements: {
        type: 'level',
        value: 51,
      },
    }],
    evolutions: [],
  }, {
    _id: 43,
    name: {
      english: 'Oddish',
      japanese: 'ナゾノクサ',
      chinese: '走路草',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 45,
      attack: 50,
      defense: 55,
      specialAttack: 75,
      specialDefense: 65,
      speed: 30,
    },
    moves: [{
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mega Drain',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Lucky Chant',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Moonlight',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Giga Drain',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Toxic',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Natural Gift',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Moonblast',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Grassy Terrain',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Petal Dance',
      requirements: {
        type: 'level',
        value: 51,
      },
    }],
    evolutions: [{
      number: 44,
      requirements: {
        type: 'level',
        value: 21,
      },
    }],
  }, {
    _id: 44,
    name: {
      english: 'Gloom',
      japanese: 'クサイハナ',
      chinese: '臭臭花',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 60,
      attack: 65,
      defense: 70,
      specialAttack: 85,
      specialDefense: 75,
      speed: 40,
    },
    moves: [{
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mega Drain',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Lucky Chant',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Moonlight',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Giga Drain',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Toxic',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Natural Gift',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Petal Blizzard',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Grassy Terrain',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Petal Dance',
      requirements: {
        type: 'level',
        value: 59,
      },
    }],
    evolutions: [{
      number: 45,
      requirements: {
        type: 'stone',
        value: 'leaf',
      },
    }],
  }, {
    _id: 45,
    name: {
      english: 'Vileplume',
      japanese: 'ラフレシア',
      chinese: '霸王花',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 75,
      attack: 80,
      defense: 85,
      specialAttack: 110,
      specialDefense: 90,
      speed: 50,
    },
    moves: [{
      name: 'Aromatherapy',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mega Drain',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Petal Blizzard',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Petal Dance',
      requirements: {
        type: 'level',
        value: 59,
      },
    }, {
      name: 'Solar Beam',
      requirements: {
        type: 'level',
        value: 69,
      },
    }],
    evolutions: [],
  }, {
    _id: 46,
    name: {
      english: 'Paras',
      japanese: 'パラス',
      chinese: '派拉斯',
    },
    type: ['bug', 'grass'],
    baseStats: {
      healthPoints: 35,
      attack: 70,
      defense: 55,
      specialAttack: 45,
      specialDefense: 55,
      speed: 25,
    },
    moves: [{
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Fury Cutter',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Spore',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Giga Drain',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Aromatherapy',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Rage Powder',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'X-Scissor',
      requirements: {
        type: 'level',
        value: 54,
      },
    }],
    evolutions: [{
      number: 47,
      requirements: {
        type: 'level',
        value: 24,
      },
    }],
  }, {
    _id: 47,
    name: {
      english: 'Parasect',
      japanese: 'パラセクト',
      chinese: '派拉斯特',
    },
    type: ['bug', 'grass'],
    baseStats: {
      healthPoints: 60,
      attack: 95,
      defense: 80,
      specialAttack: 60,
      specialDefense: 80,
      speed: 30,
    },
    moves: [{
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Cross Poison',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Fury Cutter',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Spore',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Giga Drain',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Aromatherapy',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Rage Powder',
      requirements: {
        type: 'level',
        value: 59,
      },
    }, {
      name: 'X-Scissor',
      requirements: {
        type: 'level',
        value: 66,
      },
    }],
    evolutions: [],
  }, {
    _id: 48,
    name: {
      english: 'Venonat',
      japanese: 'コンパン',
      chinese: '毛球',
    },
    type: ['bug', 'poison'],
    baseStats: {
      healthPoints: 60,
      attack: 55,
      defense: 50,
      specialAttack: 40,
      specialDefense: 55,
      speed: 45,
    },
    moves: [{
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Foresight',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Signal Beam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Leech Life',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Poison Fang',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 47,
      },
    }],
    evolutions: [{
      number: 49,
      requirements: {
        type: 'level',
        value: 31,
      },
    }],
  }, {
    _id: 49,
    name: {
      english: 'Venomoth',
      japanese: 'モルフォン',
      chinese: '摩鲁蛾',
    },
    type: ['bug', 'poison'],
    baseStats: {
      healthPoints: 70,
      attack: 65,
      defense: 60,
      specialAttack: 90,
      specialDefense: 75,
      speed: 90,
    },
    moves: [{
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Foresight',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Gust',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Silver Wind',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Signal Beam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Leech Life',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Poison Fang',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Bug Buzz',
      requirements: {
        type: 'level',
        value: 59,
      },
    }, {
      name: 'Quiver Dance',
      requirements: {
        type: 'level',
        value: 63,
      },
    }],
    evolutions: [],
  }, {
    _id: 50,
    name: {
      english: 'Diglett',
      japanese: 'ディグダ',
      chinese: '地鼠',
    },
    type: ['ground'],
    baseStats: {
      healthPoints: 10,
      attack: 55,
      defense: 25,
      specialAttack: 35,
      specialDefense: 45,
      speed: 95,
    },
    moves: [{
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Astonish',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Mud-Slap',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Earth Power',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Dig',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Fissure',
      requirements: {
        type: 'level',
        value: 43,
      },
    }],
    evolutions: [{
      number: 51,
      requirements: {
        type: 'level',
        value: 26,
      },
    }],
  }, {
    _id: 51,
    name: {
      english: 'Dugtrio',
      japanese: 'ダグトリオ',
      chinese: '三地鼠',
    },
    type: ['ground'],
    baseStats: {
      healthPoints: 35,
      attack: 100,
      defense: 50,
      specialAttack: 50,
      specialDefense: 70,
      speed: 120,
    },
    moves: [{
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Tomb',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Night Slash',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tri Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Astonish',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Mud-Slap',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Earth Power',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Dig',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Fissure',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [],
  }, {
    _id: 52,
    name: {
      english: 'Meowth',
      japanese: 'ニャース',
      chinese: '喵喵',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 40,
      attack: 45,
      defense: 35,
      specialAttack: 40,
      specialDefense: 40,
      speed: 90,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Fake Out',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Feint Attack',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Taunt',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Pay Day',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Nasty Plot',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Night Slash',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 53,
      requirements: {
        type: 'level',
        value: 28,
      },
    }],
  }, {
    _id: 53,
    name: {
      english: 'Persian',
      japanese: 'ペルシアン',
      chinese: '猫老大',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 65,
      attack: 70,
      defense: 60,
      specialAttack: 65,
      specialDefense: 65,
      speed: 115,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Switcheroo',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Play Rough',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Fake Out',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Feint Attack',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Taunt',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Pay Day',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Nasty Plot',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Captivate',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Night Slash',
      requirements: {
        type: 'level',
        value: 61,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 65,
      },
    }],
    evolutions: [],
  }, {
    _id: 54,
    name: {
      english: 'Psyduck',
      japanese: 'コダック',
      chinese: '可达鸭',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 50,
      attack: 52,
      defense: 48,
      specialAttack: 65,
      specialDefense: 50,
      speed: 55,
    },
    moves: [{
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Soak',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Wonder Room',
      requirements: {
        type: 'level',
        value: 43,
      },
    }],
    evolutions: [{
      number: 55,
      requirements: {
        type: 'level',
        value: 33,
      },
    }],
  }, {
    _id: 55,
    name: {
      english: 'Golduck',
      japanese: 'ゴルダック',
      chinese: '哥达鸭',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 80,
      attack: 82,
      defense: 78,
      specialAttack: 95,
      specialDefense: 80,
      speed: 85,
    },
    moves: [{
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Aqua Jet',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Me First',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Soak',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Wonder Room',
      requirements: {
        type: 'level',
        value: 51,
      },
    }],
    evolutions: [],
  }, {
    _id: 56,
    name: {
      english: 'Mankey',
      japanese: 'マンキー',
      chinese: '猴怪',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 40,
      attack: 80,
      defense: 35,
      specialAttack: 35,
      specialDefense: 45,
      speed: 70,
    },
    moves: [{
      name: 'Covet',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Low Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Karate Chop',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Seismic Toss',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Swagger',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Cross Chop',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Punishment',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Close Combat',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Stomping Tantrum',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Final Gambit',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 57,
      requirements: {
        type: 'level',
        value: 28,
      },
    }],
  }, {
    _id: 57,
    name: {
      english: 'Primeape',
      japanese: 'オコリザル',
      chinese: '火暴猴',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 65,
      attack: 105,
      defense: 60,
      specialAttack: 60,
      specialDefense: 70,
      speed: 95,
    },
    moves: [{
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Low Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fling',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Swipes',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Karate Chop',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Seismic Toss',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Swagger',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Cross Chop',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Punishment',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Close Combat',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Stomping Tantrum',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Final Gambit',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [],
  }, {
    _id: 58,
    name: {
      english: 'Growlithe',
      japanese: 'ガーディ',
      chinese: '卡蒂狗',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 55,
      attack: 70,
      defense: 45,
      specialAttack: 70,
      specialDefense: 50,
      speed: 60,
    },
    moves: [{
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Roar',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Odor Sleuth',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Flame Wheel',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Reversal',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Flame Burst',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Retaliate',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Heat Wave',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Flare Blitz',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 59,
      requirements: {
        type: 'stone',
        value: 'fire',
      },
    }],
  }, {
    _id: 59,
    name: {
      english: 'Arcanine',
      japanese: 'ウインディ',
      chinese: '风速狗',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 90,
      attack: 110,
      defense: 80,
      specialAttack: 100,
      specialDefense: 80,
      speed: 95,
    },
    moves: [{
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Odor Sleuth',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Roar',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Extreme Speed',
      requirements: {
        type: 'level',
        value: 34,
      },
    }],
    evolutions: [],
  }, {
    _id: 60,
    name: {
      english: 'Poliwag',
      japanese: 'ニョロモ',
      chinese: '蚊香蝌蚪',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 40,
      attack: 50,
      defense: 40,
      specialAttack: 40,
      specialDefense: 40,
      speed: 90,
    },
    moves: [{
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Belly Drum',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 41,
      },
    }],
    evolutions: [{
      number: 61,
      requirements: {
        type: 'level',
        value: 25,
      },
    }],
  }, {
    _id: 61,
    name: {
      english: 'Poliwhirl',
      japanese: 'ニョロゾ',
      chinese: '蚊香君',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 65,
      attack: 65,
      defense: 65,
      specialAttack: 50,
      specialDefense: 50,
      speed: 90,
    },
    moves: [{
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Belly Drum',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [{
      number: 62,
      requirements: {
        type: 'stone',
        value: 'water',
      },
    }],
  }, {
    _id: 62,
    name: {
      english: 'Poliwrath',
      japanese: 'ニョロボン',
      chinese: '蚊香泳士',
    },
    type: ['water', 'fighting'],
    baseStats: {
      healthPoints: 90,
      attack: 95,
      defense: 95,
      specialAttack: 70,
      specialDefense: 90,
      speed: 70,
    },
    moves: [{
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Submission',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Dynamic Punch',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Mind Reader',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Circle Throw',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [],
  }, {
    _id: 63,
    name: {
      english: 'Abra',
      japanese: 'ケーシィ',
      chinese: '凯西',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 25,
      attack: 20,
      defense: 15,
      specialAttack: 105,
      specialDefense: 55,
      speed: 90,
    },
    moves: [{
      name: 'Teleport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [{
      number: 64,
      requirements: {
        type: 'level',
        value: 16,
      },
    }],
  }, {
    _id: 64,
    name: {
      english: 'Kadabra',
      japanese: 'ユンゲラー',
      chinese: '勇基拉',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 40,
      attack: 35,
      defense: 30,
      specialAttack: 120,
      specialDefense: 70,
      speed: 105,
    },
    moves: [{
      name: 'Teleport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Kinesis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Miracle Eye',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Reflect',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Psycho Cut',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Recover',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Telekinesis',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Ally Switch',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Role Play',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Future Sight',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Trick',
      requirements: {
        type: 'level',
        value: 46,
      },
    }],
    evolutions: [{
      number: 65,
      requirements: {
        type: 'trade',
        value: null,
      },
    }],
  }, {
    _id: 65,
    name: {
      english: 'Alakazam',
      japanese: 'フーディン',
      chinese: '胡地',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 55,
      attack: 50,
      defense: 45,
      specialAttack: 135,
      specialDefense: 95,
      speed: 120,
    },
    moves: [{
      name: 'Teleport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Kinesis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Miracle Eye',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Reflect',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Psycho Cut',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Recover',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Telekinesis',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Ally Switch',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Role Play',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Future Sight',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Trick',
      requirements: {
        type: 'level',
        value: 46,
      },
    }],
    evolutions: [],
  }, {
    _id: 66,
    name: {
      english: 'Machop',
      japanese: 'ワンリキー',
      chinese: '腕力',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 70,
      attack: 80,
      defense: 50,
      specialAttack: 35,
      specialDefense: 35,
      speed: 35,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Low Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Karate Chop',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Foresight',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Low Sweep',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Seismic Toss',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Revenge',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Vital Throw',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Dual Chop',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Submission',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Bulk Up',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Cross Chop',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Dynamic Punch',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 67,
      requirements: {
        type: 'level',
        value: 28,
      },
    }],
  }, {
    _id: 67,
    name: {
      english: 'Machoke',
      japanese: 'ゴーリキー',
      chinese: '豪力',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 80,
      attack: 100,
      defense: 70,
      specialAttack: 50,
      specialDefense: 60,
      speed: 45,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Low Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Karate Chop',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Foresight',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Low Sweep',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Seismic Toss',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Revenge',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Vital Throw',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Dual Chop',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Submission',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Bulk Up',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Cross Chop',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Dynamic Punch',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [{
      number: 68,
      requirements: {
        type: 'trade',
        value: null,
      },
    }],
  }, {
    _id: 68,
    name: {
      english: 'Machamp',
      japanese: 'カイリキー',
      chinese: '怪力',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 90,
      attack: 130,
      defense: 80,
      specialAttack: 65,
      specialDefense: 85,
      speed: 55,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Low Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Strength',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wide Guard',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Karate Chop',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Foresight',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Low Sweep',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Seismic Toss',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Revenge',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Vital Throw',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Dual Chop',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Submission',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Bulk Up',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Cross Chop',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Dynamic Punch',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [],
  }, {
    _id: 69,
    name: {
      english: 'Bellsprout',
      japanese: 'マダツボミ',
      chinese: '喇叭芽',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 50,
      attack: 75,
      defense: 35,
      specialAttack: 70,
      specialDefense: 30,
      speed: 40,
    },
    moves: [{
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Gastro Acid',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Razor Leaf',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 70,
      requirements: {
        type: 'level',
        value: 21,
      },
    }],
  }, {
    _id: 70,
    name: {
      english: 'Weepinbell',
      japanese: 'ウツドン',
      chinese: '口呆花',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 65,
      attack: 90,
      defense: 50,
      specialAttack: 85,
      specialDefense: 45,
      speed: 55,
    },
    moves: [{
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Gastro Acid',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Razor Leaf',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [{
      number: 71,
      requirements: {
        type: 'stone',
        value: 'leaf',
      },
    }],
  }, {
    _id: 71,
    name: {
      english: 'Victreebel',
      japanese: 'ウツボット',
      chinese: '大食花',
    },
    type: ['grass', 'poison'],
    baseStats: {
      healthPoints: 80,
      attack: 105,
      defense: 65,
      specialAttack: 100,
      specialDefense: 70,
      speed: 70,
    },
    moves: [{
      name: 'Leaf Tornado',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Razor Leaf',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spit Up',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Stockpile',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Swallow',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sweet Scent',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leaf Storm',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Leaf Blade',
      requirements: {
        type: 'level',
        value: 44,
      },
    }],
    evolutions: [],
  }, {
    _id: 72,
    name: {
      english: 'Tentacool',
      japanese: 'メノクラゲ',
      chinese: '玛瑙水母',
    },
    type: ['water', 'poison'],
    baseStats: {
      healthPoints: 40,
      attack: 40,
      defense: 35,
      specialAttack: 50,
      specialDefense: 100,
      speed: 70,
    },
    moves: [{
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Constrict',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Acid Spray',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Barrier',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Hex',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Sludge Wave',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 49,
      },
    }],
    evolutions: [{
      number: 73,
      requirements: {
        type: 'level',
        value: 30,
      },
    }],
  }, {
    _id: 73,
    name: {
      english: 'Tentacruel',
      japanese: 'ドククラゲ',
      chinese: '毒刺水母',
    },
    type: ['water', 'poison'],
    baseStats: {
      healthPoints: 80,
      attack: 70,
      defense: 65,
      specialAttack: 80,
      specialDefense: 120,
      speed: 100,
    },
    moves: [{
      name: 'Poison Sting',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Reflect Type',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Constrict',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Acid',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Acid Spray',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Barrier',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Hex',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Sludge Wave',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 52,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 56,
      },
    }],
    evolutions: [],
  }, {
    _id: 74,
    name: {
      english: 'Geodude',
      japanese: 'イシツブテ',
      chinese: '小拳石',
    },
    type: ['rock', 'ground'],
    baseStats: {
      healthPoints: 40,
      attack: 80,
      defense: 100,
      specialAttack: 30,
      specialDefense: 30,
      speed: 20,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Rock Polish',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Rock Throw',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Smack Down',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Stealth Rock',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Stone Edge',
      requirements: {
        type: 'level',
        value: 42,
      },
    }],
    evolutions: [{
      number: 75,
      requirements: {
        type: 'level',
        value: 25,
      },
    }],
  }, {
    _id: 75,
    name: {
      english: 'Graveler',
      japanese: 'ゴローン',
      chinese: '隆隆石',
    },
    type: ['rock', 'ground'],
    baseStats: {
      healthPoints: 55,
      attack: 95,
      defense: 115,
      specialAttack: 45,
      specialDefense: 45,
      speed: 35,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Rock Polish',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Rock Throw',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Smack Down',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Stealth Rock',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Stone Edge',
      requirements: {
        type: 'level',
        value: 54,
      },
    }],
    evolutions: [{
      number: 76,
      requirements: {
        type: 'trade',
        value: null,
      },
    }],
  }, {
    _id: 76,
    name: {
      english: 'Golem',
      japanese: 'ゴローニャ',
      chinese: '隆隆岩',
    },
    type: ['rock', 'ground'],
    baseStats: {
      healthPoints: 80,
      attack: 120,
      defense: 130,
      specialAttack: 55,
      specialDefense: 65,
      speed: 45,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Rock Polish',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Steamroller',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Magnitude',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Rock Throw',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Smack Down',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Stealth Rock',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Stone Edge',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Heavy Slam',
      requirements: {
        type: 'level',
        value: 60,
      },
    }],
    evolutions: [],
  }, {
    _id: 77,
    name: {
      english: 'Ponyta',
      japanese: 'ポニータ',
      chinese: '小火马',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 50,
      attack: 85,
      defense: 55,
      specialAttack: 65,
      specialDefense: 65,
      speed: 90,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Flame Wheel',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Flame Charge',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Inferno',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Fire Blast',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Bounce',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Flare Blitz',
      requirements: {
        type: 'level',
        value: 49,
      },
    }],
    evolutions: [{
      number: 78,
      requirements: {
        type: 'level',
        value: 40,
      },
    }],
  }, {
    _id: 78,
    name: {
      english: 'Rapidash',
      japanese: 'ギャロップ',
      chinese: '烈焰马',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 65,
      attack: 100,
      defense: 70,
      specialAttack: 80,
      specialDefense: 80,
      speed: 105,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Megahorn',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Flame Wheel',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Flame Charge',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Inferno',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Fire Blast',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Bounce',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Flare Blitz',
      requirements: {
        type: 'level',
        value: 49,
      },
    }],
    evolutions: [],
  }, {
    _id: 79,
    name: {
      english: 'Slowpoke',
      japanese: 'ヤドン',
      chinese: '呆呆兽',
    },
    type: ['water', 'psychic'],
    baseStats: {
      healthPoints: 90,
      attack: 65,
      defense: 65,
      specialAttack: 40,
      specialDefense: 40,
      speed: 15,
    },
    moves: [{
      name: 'Curse',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Yawn',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Slack Off',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Heal Pulse',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [{
      number: 80,
      requirements: {
        type: 'level',
        value: 37,
      },
    }],
  }, {
    _id: 80,
    name: {
      english: 'Slowbro',
      japanese: 'ヤドラン',
      chinese: '呆壳兽',
    },
    type: ['water', 'psychic'],
    baseStats: {
      healthPoints: 95,
      attack: 75,
      defense: 110,
      specialAttack: 100,
      specialDefense: 80,
      speed: 30,
    },
    moves: [{
      name: 'Curse',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Yawn',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Slack Off',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 62,
      },
    }, {
      name: 'Heal Pulse',
      requirements: {
        type: 'level',
        value: 68,
      },
    }],
    evolutions: [],
  }, {
    _id: 81,
    name: {
      english: 'Magnemite',
      japanese: 'コイル',
      chinese: '小磁怪',
    },
    type: ['electric', 'steel'],
    baseStats: {
      healthPoints: 25,
      attack: 35,
      defense: 70,
      specialAttack: 95,
      specialDefense: 55,
      speed: 45,
    },
    moves: [{
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Magnet Bomb',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Sonic Boom',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Spark',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Mirror Shot',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Metal Sound',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Electro Ball',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Flash Cannon',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Lock-On',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Magnet Rise',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Zap Cannon',
      requirements: {
        type: 'level',
        value: 49,
      },
    }],
    evolutions: [{
      number: 82,
      requirements: {
        type: 'level',
        value: 30,
      },
    }],
  }, {
    _id: 82,
    name: {
      english: 'Magneton',
      japanese: 'レアコイル',
      chinese: '三合一磁怪',
    },
    type: ['electric', 'steel'],
    baseStats: {
      healthPoints: 50,
      attack: 60,
      defense: 95,
      specialAttack: 120,
      specialDefense: 70,
      speed: 70,
    },
    moves: [{
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Electric Terrain',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tri Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Magnet Bomb',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Sonic Boom',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Spark',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Mirror Shot',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Metal Sound',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Electro Ball',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Flash Cannon',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Lock-On',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Magnet Rise',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 59,
      },
    }, {
      name: 'Zap Cannon',
      requirements: {
        type: 'level',
        value: 63,
      },
    }],
    evolutions: [],
  }, {
    _id: 83,
    name: {
      english: 'Farfetch\'d',
      japanese: 'カモネギ',
      chinese: '大葱鸭',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 52,
      attack: 90,
      defense: 55,
      specialAttack: 58,
      specialDefense: 62,
      speed: 60,
    },
    moves: [{
      name: 'Fury Cutter',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Aerial Ace',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Air Cutter',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Night Slash',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Acrobatics',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'False Swipe',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Brave Bird',
      requirements: {
        type: 'level',
        value: 55,
      },
    }],
    evolutions: [],
  }, {
    _id: 84,
    name: {
      english: 'Doduo',
      japanese: 'ドードー',
      chinese: '嘟嘟',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 35,
      attack: 85,
      defense: 45,
      specialAttack: 35,
      specialDefense: 35,
      speed: 75,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Pluck',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Double Hit',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Uproar',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Acupressure',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Jump Kick',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Drill Peck',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 85,
      requirements: {
        type: 'level',
        value: 31,
      },
    }],
  }, {
    _id: 85,
    name: {
      english: 'Dodrio',
      japanese: 'ドードリオ',
      chinese: '嘟嘟利',
    },
    type: ['normal', 'flying'],
    baseStats: {
      healthPoints: 60,
      attack: 110,
      defense: 70,
      specialAttack: 60,
      specialDefense: 60,
      speed: 110,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tri Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Pluck',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Double Hit',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Uproar',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Acupressure',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Jump Kick',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Drill Peck',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 52,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 56,
      },
    }],
    evolutions: [],
  }, {
    _id: 86,
    name: {
      english: 'Seel',
      japanese: 'パウワウ',
      chinese: '小海狮',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 65,
      attack: 45,
      defense: 55,
      specialAttack: 45,
      specialDefense: 70,
      speed: 45,
    },
    moves: [{
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Icy Wind',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Encore',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Ice Shard',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Rest',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Aqua Ring',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Aurora Beam',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Aqua Jet',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Dive',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Ice Beam',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Hail',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [{
      number: 87,
      requirements: {
        type: 'level',
        value: 34,
      },
    }],
  }, {
    _id: 87,
    name: {
      english: 'Dewgong',
      japanese: 'ジュゴン',
      chinese: '白海狮',
    },
    type: ['water', 'ice'],
    baseStats: {
      healthPoints: 90,
      attack: 70,
      defense: 80,
      specialAttack: 70,
      specialDefense: 95,
      speed: 70,
    },
    moves: [{
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sheer Cold',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Signal Beam',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Icy Wind',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Encore',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Ice Shard',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Rest',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Aqua Ring',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Aurora Beam',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Aqua Jet',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Dive',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Ice Beam',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 61,
      },
    }, {
      name: 'Hail',
      requirements: {
        type: 'level',
        value: 65,
      },
    }],
    evolutions: [],
  }, {
    _id: 88,
    name: {
      english: 'Grimer',
      japanese: 'ベトベター',
      chinese: '臭泥',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 80,
      attack: 80,
      defense: 50,
      specialAttack: 40,
      specialDefense: 50,
      speed: 25,
    },
    moves: [{
      name: 'Poison Gas',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Mud-Slap',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Sludge',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Minimize',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fling',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Sludge Bomb',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Sludge Wave',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Gunk Shot',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Acid Armor',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Belch',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Memento',
      requirements: {
        type: 'level',
        value: 48,
      },
    }],
    evolutions: [{
      number: 89,
      requirements: {
        type: 'level',
        value: 38,
      },
    }],
  }, {
    _id: 89,
    name: {
      english: 'Muk',
      japanese: 'ベトベトン',
      chinese: '臭臭泥',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 105,
      attack: 105,
      defense: 75,
      specialAttack: 65,
      specialDefense: 100,
      speed: 50,
    },
    moves: [{
      name: 'Poison Gas',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Venom Drench',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Mud-Slap',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Sludge',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mud Bomb',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Minimize',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fling',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Sludge Bomb',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Sludge Wave',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Gunk Shot',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Acid Armor',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Belch',
      requirements: {
        type: 'level',
        value: 52,
      },
    }, {
      name: 'Memento',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [],
  }, {
    _id: 90,
    name: {
      english: 'Shellder',
      japanese: 'シェルダー',
      chinese: '大舌贝',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 30,
      attack: 65,
      defense: 100,
      specialAttack: 45,
      specialDefense: 25,
      speed: 40,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Icicle Spear',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Clamp',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Ice Shard',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Razor Shell',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Aurora Beam',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Whirlpool',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Iron Defense',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Ice Beam',
      requirements: {
        type: 'level',
        value: 52,
      },
    }, {
      name: 'Shell Smash',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [{
      number: 91,
      requirements: {
        type: 'stone',
        value: 'water',
      },
    }],
  }, {
    _id: 91,
    name: {
      english: 'Cloyster',
      japanese: 'パルシェン',
      chinese: '刺甲贝',
    },
    type: ['water', 'ice'],
    baseStats: {
      healthPoints: 50,
      attack: 95,
      defense: 180,
      specialAttack: 85,
      specialDefense: 45,
      speed: 70,
    },
    moves: [{
      name: 'Aurora Beam',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Shell Smash',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Toxic Spikes',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spike Cannon',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Spikes',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Icicle Crash',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [],
  }, {
    _id: 92,
    name: {
      english: 'Gastly',
      japanese: 'ゴース',
      chinese: '鬼斯',
    },
    type: ['ghost', 'poison'],
    baseStats: {
      healthPoints: 30,
      attack: 35,
      defense: 30,
      specialAttack: 100,
      specialDefense: 35,
      speed: 80,
    },
    moves: [{
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Lick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spite',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Mean Look',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Curse',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Night Shade',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Payback',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Shadow Ball',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Dream Eater',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Dark Pulse',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Destiny Bond',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Hex',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Nightmare',
      requirements: {
        type: 'level',
        value: 47,
      },
    }],
    evolutions: [{
      number: 93,
      requirements: {
        type: 'level',
        value: 25,
      },
    }],
  }, {
    _id: 93,
    name: {
      english: 'Haunter',
      japanese: 'ゴースト',
      chinese: '鬼斯通',
    },
    type: ['ghost', 'poison'],
    baseStats: {
      healthPoints: 45,
      attack: 50,
      defense: 45,
      specialAttack: 115,
      specialDefense: 55,
      speed: 95,
    },
    moves: [{
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Lick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Shadow Punch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spite',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Mean Look',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Curse',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Night Shade',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Payback',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Shadow Ball',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Dream Eater',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Dark Pulse',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Destiny Bond',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Hex',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Nightmare',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [{
      number: 94,
      requirements: {
        type: 'trade',
        value: null,
      },
    }],
  }, {
    _id: 94,
    name: {
      english: 'Gengar',
      japanese: 'ゲンガー',
      chinese: '耿鬼',
    },
    type: ['ghost', 'poison'],
    baseStats: {
      healthPoints: 60,
      attack: 65,
      defense: 60,
      specialAttack: 130,
      specialDefense: 75,
      speed: 110,
    },
    moves: [{
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Lick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Shadow Punch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spite',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Mean Look',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Curse',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Night Shade',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Payback',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Shadow Ball',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Dream Eater',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Dark Pulse',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Destiny Bond',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Hex',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Nightmare',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [],
  }, {
    _id: 95,
    name: {
      english: 'Onix',
      japanese: 'イワーク',
      chinese: '大岩蛇',
    },
    type: ['rock', 'ground'],
    baseStats: {
      healthPoints: 35,
      attack: 45,
      defense: 160,
      specialAttack: 30,
      specialDefense: 45,
      speed: 70,
    },
    moves: [{
      name: 'Bind',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mud Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Curse',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Rock Throw',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Rock Tomb',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stealth Rock',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Rock Polish',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Smack Down',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Dragon Breath',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Rock Slide',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Sand Tomb',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Iron Tail',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Dig',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Stone Edge',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Sandstorm',
      requirements: {
        type: 'level',
        value: 52,
      },
    }],
    evolutions: [],
  }, {
    _id: 96,
    name: {
      english: 'Drowzee',
      japanese: 'スリープ',
      chinese: '催眠貘',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 60,
      attack: 48,
      defense: 45,
      specialAttack: 43,
      specialDefense: 90,
      speed: 42,
    },
    moves: [{
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Poison Gas',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Meditate',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Synchronoise',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Swagger',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Nasty Plot',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Psyshock',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Future Sight',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [{
      number: 97,
      requirements: {
        type: 'level',
        value: 26,
      },
    }],
  }, {
    _id: 97,
    name: {
      english: 'Hypno',
      japanese: 'スリーパー',
      chinese: '引梦貘人',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 85,
      attack: 73,
      defense: 70,
      specialAttack: 73,
      specialDefense: 115,
      speed: 67,
    },
    moves: [{
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Switcheroo',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Nightmare',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Poison Gas',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Meditate',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Synchronoise',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Swagger',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Nasty Plot',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Psyshock',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Future Sight',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [],
  }, {
    _id: 98,
    name: {
      english: 'Krabby',
      japanese: 'クラブ',
      chinese: '大钳蟹',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 30,
      attack: 105,
      defense: 90,
      specialAttack: 25,
      specialDefense: 25,
      speed: 50,
    },
    moves: [{
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mud Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Vice Grip',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Metal Claw',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Guillotine',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Crabhammer',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Flail',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 99,
      requirements: {
        type: 'level',
        value: 28,
      },
    }],
  }, {
    _id: 99,
    name: {
      english: 'Kingler',
      japanese: 'キングラー',
      chinese: '巨钳蟹',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 55,
      attack: 130,
      defense: 115,
      specialAttack: 50,
      specialDefense: 50,
      speed: 75,
    },
    moves: [{
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mud Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wide Guard',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Vice Grip',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Metal Claw',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Guillotine',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Crabhammer',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Flail',
      requirements: {
        type: 'level',
        value: 63,
      },
    }],
    evolutions: [],
  }, {
    _id: 100,
    name: {
      english: 'Voltorb',
      japanese: 'ビリリダマ',
      chinese: '霹雳电球',
    },
    type: ['electric'],
    baseStats: {
      healthPoints: 40,
      attack: 30,
      defense: 50,
      specialAttack: 55,
      specialDefense: 55,
      speed: 100,
    },
    moves: [{
      name: 'Charge',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sonic Boom',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Eerie Impulse',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Spark',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Charge Beam',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Electro Ball',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Magnet Rise',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Mirror Move',
      requirements: {
        type: 'level',
        value: 48,
      },
    }],
    evolutions: [{
      number: 101,
      requirements: {
        type: 'level',
        value: 30,
      },
    }],
  }, {
    _id: 101,
    name: {
      english: 'Electrode',
      japanese: 'マルマイン',
      chinese: '顽皮雷弹',
    },
    type: ['electric'],
    baseStats: {
      healthPoints: 60,
      attack: 50,
      defense: 70,
      specialAttack: 80,
      specialDefense: 80,
      speed: 150,
    },
    moves: [{
      name: 'Charge',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Magnetic Flux',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sonic Boom',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Eerie Impulse',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Spark',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Charge Beam',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Electro Ball',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Magnet Rise',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Mirror Coat',
      requirements: {
        type: 'level',
        value: 58,
      },
    }],
    evolutions: [],
  }, {
    _id: 102,
    name: {
      english: 'Exeggcute',
      japanese: 'タマタマ',
      chinese: '蛋蛋',
    },
    type: ['grass', 'psychic'],
    baseStats: {
      healthPoints: 60,
      attack: 40,
      defense: 80,
      specialAttack: 60,
      specialDefense: 45,
      speed: 40,
    },
    moves: [{
      name: 'Barrage',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Uproar',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Reflect',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Leech Seed',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Bullet Seed',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Worry Seed',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Natural Gift',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Solar Beam',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Extrasensory',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Bestow',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 103,
      requirements: {
        type: 'stone',
        value: 'leaf',
      },
    }],
  }, {
    _id: 103,
    name: {
      english: 'Exeggutor',
      japanese: 'ナッシー',
      chinese: '椰蛋树',
    },
    type: ['grass', 'psychic'],
    baseStats: {
      healthPoints: 95,
      attack: 95,
      defense: 85,
      specialAttack: 125,
      specialDefense: 75,
      speed: 55,
    },
    moves: [{
      name: 'Barrage',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Hypnosis',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Seed Bomb',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Psyshock',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Egg Bomb',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Wood Hammer',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Leaf Storm',
      requirements: {
        type: 'level',
        value: 47,
      },
    }],
    evolutions: [],
  }, {
    _id: 104,
    name: {
      english: 'Cubone',
      japanese: 'カラカラ',
      chinese: '卡拉卡拉',
    },
    type: ['ground'],
    baseStats: {
      healthPoints: 50,
      attack: 50,
      defense: 95,
      specialAttack: 40,
      specialDefense: 50,
      speed: 35,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Bone Club',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Bonemerang',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'False Swipe',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Fling',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Stomping Tantrum',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Retaliate',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Bone Rush',
      requirements: {
        type: 'level',
        value: 51,
      },
    }],
    evolutions: [{
      number: 105,
      requirements: {
        type: 'level',
        value: 28,
      },
    }],
  }, {
    _id: 105,
    name: {
      english: 'Marowak',
      japanese: 'ガラガラ',
      chinese: '嘎啦嘎啦',
    },
    type: ['ground'],
    baseStats: {
      healthPoints: 60,
      attack: 80,
      defense: 110,
      specialAttack: 50,
      specialDefense: 80,
      speed: 45,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Bone Club',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Headbutt',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Bonemerang',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'False Swipe',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Fling',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Stomping Tantrum',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Endeavor',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Retaliate',
      requirements: {
        type: 'level',
        value: 59,
      },
    }, {
      name: 'Bone Rush',
      requirements: {
        type: 'level',
        value: 65,
      },
    }],
    evolutions: [],
  }, {
    _id: 106,
    name: {
      english: 'Hitmonlee',
      japanese: 'サワムラー',
      chinese: '飞腿郎',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 50,
      attack: 120,
      defense: 53,
      specialAttack: 35,
      specialDefense: 110,
      speed: 87,
    },
    moves: [{
      name: 'Revenge',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Meditate',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Rolling Kick',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Jump Kick',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Brick Break',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'High Jump Kick',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Mind Reader',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Foresight',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Wide Guard',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Blaze Kick',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Endure',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Mega Kick',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Close Combat',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Reversal',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [],
  }, {
    _id: 107,
    name: {
      english: 'Hitmonchan',
      japanese: 'エビワラー',
      chinese: '快拳郎',
    },
    type: ['fighting'],
    baseStats: {
      healthPoints: 50,
      attack: 105,
      defense: 79,
      specialAttack: 35,
      specialDefense: 110,
      speed: 76,
    },
    moves: [{
      name: 'Comet Punch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Counter',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Revenge',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Bullet Punch',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Mach Punch',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Vacuum Wave',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Quick Guard',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Fire Punch',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Ice Punch',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Thunder Punch',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Sky Uppercut',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Mega Punch',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Detect',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Focus Punch',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Counter',
      requirements: {
        type: 'level',
        value: 61,
      },
    }, {
      name: 'Close Combat',
      requirements: {
        type: 'level',
        value: 66,
      },
    }],
    evolutions: [],
  }, {
    _id: 108,
    name: {
      english: 'Lickitung',
      japanese: 'ベロリンガ',
      chinese: '大舌头',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 90,
      attack: 55,
      defense: 75,
      specialAttack: 60,
      specialDefense: 75,
      speed: 30,
    },
    moves: [{
      name: 'Lick',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Me First',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Refresh',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Power Whip',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [],
  }, {
    _id: 109,
    name: {
      english: 'Koffing',
      japanese: 'ドガース',
      chinese: '瓦斯弹',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 40,
      attack: 65,
      defense: 95,
      specialAttack: 60,
      specialDefense: 45,
      speed: 35,
    },
    moves: [{
      name: 'Poison Gas',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Smog',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Clear Smog',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Sludge',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Sludge Bomb',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Destiny Bond',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Belch',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Memento',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 110,
      requirements: {
        type: 'level',
        value: 35,
      },
    }],
  }, {
    _id: 110,
    name: {
      english: 'Weezing',
      japanese: 'マタドガス',
      chinese: '双弹瓦斯',
    },
    type: ['poison'],
    baseStats: {
      healthPoints: 65,
      attack: 90,
      defense: 120,
      specialAttack: 85,
      specialDefense: 70,
      speed: 60,
    },
    moves: [{
      name: 'Double Hit',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Gas',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Smog',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Assurance',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Clear Smog',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Sludge',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Self-Destruct',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Sludge Bomb',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Explosion',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Destiny Bond',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Belch',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Memento',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [],
  }, {
    _id: 111,
    name: {
      english: 'Rhyhorn',
      japanese: 'サイホーン',
      chinese: '独角犀牛',
    },
    type: ['ground', 'rock'],
    baseStats: {
      healthPoints: 80,
      attack: 85,
      defense: 95,
      specialAttack: 30,
      specialDefense: 30,
      speed: 25,
    },
    moves: [{
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Smack Down',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Drill Run',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Stone Edge',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Megahorn',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Horn Drill',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [{
      number: 112,
      requirements: {
        type: 'level',
        value: 42,
      },
    }],
  }, {
    _id: 112,
    name: {
      english: 'Rhydon',
      japanese: 'サイドン',
      chinese: '钻角犀兽',
    },
    type: ['ground', 'rock'],
    baseStats: {
      healthPoints: 105,
      attack: 130,
      defense: 120,
      specialAttack: 45,
      specialDefense: 45,
      speed: 40,
    },
    moves: [{
      name: 'Hammer Arm',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Smack Down',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Stomp',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Bulldoze',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Drill Run',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Stone Edge',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Earthquake',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Megahorn',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Horn Drill',
      requirements: {
        type: 'level',
        value: 62,
      },
    }],
    evolutions: [],
  }, {
    _id: 113,
    name: {
      english: 'Chansey',
      japanese: 'ラッキー',
      chinese: '吉利蛋',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 250,
      attack: 5,
      defense: 5,
      specialAttack: 35,
      specialDefense: 105,
      speed: 50,
    },
    moves: [{
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Refresh',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Soft-Boiled',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Bestow',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Minimize',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Sing',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Fling',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Heal Pulse',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Egg Bomb',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Healing Wish',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 65,
      },
    }],
    evolutions: [],
  }, {
    _id: 114,
    name: {
      english: 'Tangela',
      japanese: 'モンジャラ',
      chinese: '蔓藤怪',
    },
    type: ['grass'],
    baseStats: {
      healthPoints: 65,
      attack: 55,
      defense: 115,
      specialAttack: 100,
      specialDefense: 40,
      speed: 60,
    },
    moves: [{
      name: 'Constrict',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ingrain',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sleep Powder',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Vine Whip',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Poison Powder',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Bind',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Growth',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Mega Drain',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Knock Off',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Stun Spore',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Natural Gift',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Giga Drain',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Tickle',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Grassy Terrain',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Power Whip',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [],
  }, {
    _id: 115,
    name: {
      english: 'Kangaskhan',
      japanese: 'ガルーラ',
      chinese: '袋兽',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 105,
      attack: 95,
      defense: 80,
      specialAttack: 40,
      specialDefense: 80,
      speed: 90,
    },
    moves: [{
      name: 'Comet Punch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fake Out',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Double Hit',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Mega Punch',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Dizzy Punch',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Endure',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Sucker Punch',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Reversal',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [],
  }, {
    _id: 116,
    name: {
      english: 'Horsea',
      japanese: 'タッツー',
      chinese: '墨海马',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 30,
      attack: 40,
      defense: 70,
      specialAttack: 70,
      specialDefense: 25,
      speed: 60,
    },
    moves: [{
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Dragon Pulse',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Dragon Dance',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 52,
      },
    }],
    evolutions: [{
      number: 117,
      requirements: {
        type: 'level',
        value: 32,
      },
    }],
  }, {
    _id: 117,
    name: {
      english: 'Seadra',
      japanese: 'シードラ',
      chinese: '海刺龙',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 55,
      attack: 65,
      defense: 95,
      specialAttack: 95,
      specialDefense: 45,
      speed: 85,
    },
    moves: [{
      name: 'Bubble',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 38,
      },
    }, {
      name: 'Dragon Pulse',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Dragon Dance',
      requirements: {
        type: 'level',
        value: 52,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 60,
      },
    }],
    evolutions: [],
  }, {
    _id: 118,
    name: {
      english: 'Goldeen',
      japanese: 'トサキント',
      chinese: '角金鱼',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 45,
      attack: 67,
      defense: 60,
      specialAttack: 35,
      specialDefense: 50,
      speed: 63,
    },
    moves: [{
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Flail',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Aqua Ring',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Waterfall',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Horn Drill',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Soak',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Megahorn',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 119,
      requirements: {
        type: 'level',
        value: 33,
      },
    }],
  }, {
    _id: 119,
    name: {
      english: 'Seaking',
      japanese: 'アズマオウ',
      chinese: '金鱼王',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 80,
      attack: 92,
      defense: 65,
      specialAttack: 65,
      specialDefense: 80,
      speed: 68,
    },
    moves: [{
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Sport',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Poison Jab',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Flail',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Aqua Ring',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fury Attack',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Waterfall',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Horn Drill',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Soak',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Megahorn',
      requirements: {
        type: 'level',
        value: 54,
      },
    }],
    evolutions: [],
  }, {
    _id: 120,
    name: {
      english: 'Staryu',
      japanese: 'ヒトデマン',
      chinese: '海星星',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 30,
      attack: 45,
      defense: 55,
      specialAttack: 70,
      specialDefense: 55,
      speed: 85,
    },
    moves: [{
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Rapid Spin',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Recover',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Psywave',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Bubble Beam',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Camouflage',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Gyro Ball',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Minimize',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Reflect Type',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Power Gem',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Cosmic Power',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 53,
      },
    }],
    evolutions: [{
      number: 121,
      requirements: {
        type: 'stone',
        value: 'water',
      },
    }],
  }, {
    _id: 121,
    name: {
      english: 'Starmie',
      japanese: 'スターミー',
      chinese: '宝石海星',
    },
    type: ['water', 'psychic'],
    baseStats: {
      healthPoints: 60,
      attack: 75,
      defense: 85,
      specialAttack: 100,
      specialDefense: 85,
      speed: 115,
    },
    moves: [{
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Rapid Spin',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Recover',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spotlight',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 40,
      },
    }],
    evolutions: [],
  }, {
    _id: 122,
    name: {
      english: 'Mr. Mime',
      japanese: 'バリヤード',
      chinese: '魔墙人偶',
    },
    type: ['psychic', 'fairy'],
    baseStats: {
      healthPoints: 40,
      attack: 45,
      defense: 65,
      specialAttack: 100,
      specialDefense: 120,
      speed: 90,
    },
    moves: [{
      name: 'Barrier',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Guard Swap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Magical Leaf',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Misty Terrain',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Power Swap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Guard',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wide Guard',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Copycat',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Meditate',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Mimic',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Psywave',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Encore',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Reflect',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Substitute',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Recycle',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Trick',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Role Play',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Baton Pass',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [],
  }, {
    _id: 123,
    name: {
      english: 'Scyther',
      japanese: 'ストライク',
      chinese: '飞天螳螂',
    },
    type: ['bug', 'flying'],
    baseStats: {
      healthPoints: 70,
      attack: 110,
      defense: 80,
      specialAttack: 55,
      specialDefense: 80,
      speed: 105,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Vacuum Wave',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'False Swipe',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Fury Cutter',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Razor Wind',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Double Team',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'X-Scissor',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Night Slash',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Double Hit',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [],
  }, {
    _id: 124,
    name: {
      english: 'Jynx',
      japanese: 'ルージュラ',
      chinese: '迷唇姐',
    },
    type: ['ice', 'psychic'],
    baseStats: {
      healthPoints: 65,
      attack: 50,
      defense: 35,
      specialAttack: 115,
      specialDefense: 95,
      speed: 95,
    },
    moves: [{
      name: 'Draining Kiss',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Lick',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Lovely Kiss',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Powder Snow',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Double Slap',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Ice Punch',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Heart Stamp',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Mean Look',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Fake Tears',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Wake-Up Slap',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Avalanche',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Perish Song',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Blizzard',
      requirements: {
        type: 'level',
        value: 60,
      },
    }],
    evolutions: [],
  }, {
    _id: 125,
    name: {
      english: 'Electabuzz',
      japanese: 'エレブー',
      chinese: '电击兽',
    },
    type: ['electric'],
    baseStats: {
      healthPoints: 65,
      attack: 83,
      defense: 57,
      specialAttack: 95,
      specialDefense: 85,
      speed: 105,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Low Kick',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Shock Wave',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Electro Ball',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Thunder Punch',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Screech',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Thunderbolt',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Thunder',
      requirements: {
        type: 'level',
        value: 55,
      },
    }],
    evolutions: [],
  }, {
    _id: 126,
    name: {
      english: 'Magmar',
      japanese: 'ブーバー',
      chinese: '鸭嘴火兽',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 65,
      attack: 95,
      defense: 57,
      specialAttack: 100,
      specialDefense: 85,
      speed: 93,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Smog',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Smokescreen',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Feint Attack',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Clear Smog',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Flame Burst',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Fire Punch',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Lava Plume',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Sunny Day',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Fire Blast',
      requirements: {
        type: 'level',
        value: 55,
      },
    }],
    evolutions: [],
  }, {
    _id: 127,
    name: {
      english: 'Pinsir',
      japanese: 'カイロス',
      chinese: '凯罗斯',
    },
    type: ['bug'],
    baseStats: {
      healthPoints: 65,
      attack: 125,
      defense: 100,
      specialAttack: 55,
      specialDefense: 70,
      speed: 85,
    },
    moves: [{
      name: 'Focus Energy',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Vice Grip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bind',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Seismic Toss',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Revenge',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Vital Throw',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Double Hit',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Brick Break',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'X-Scissor',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Submission',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Storm Throw',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Swords Dance',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Superpower',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Guillotine',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [],
  }, {
    _id: 128,
    name: {
      english: 'Tauros',
      japanese: 'ケンタロス',
      chinese: '肯泰罗',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 75,
      attack: 100,
      defense: 95,
      specialAttack: 40,
      specialDefense: 70,
      speed: 110,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 3,
      },
    }, {
      name: 'Rage',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Horn Attack',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Pursuit',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Rest',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Payback',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Work Up',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Zen Headbutt',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Swagger',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 63,
      },
    }, {
      name: 'Giga Impact',
      requirements: {
        type: 'level',
        value: 71,
      },
    }],
    evolutions: [],
  }, {
    _id: 129,
    name: {
      english: 'Magikarp',
      japanese: 'コイキング',
      chinese: '鲤鱼王',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 20,
      attack: 10,
      defense: 55,
      specialAttack: 15,
      specialDefense: 20,
      speed: 80,
    },
    moves: [{
      name: 'Splash',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Flail',
      requirements: {
        type: 'level',
        value: 30,
      },
    }],
    evolutions: [{
      number: 130,
      requirements: {
        type: 'level',
        value: 20,
      },
    }],
  }, {
    _id: 130,
    name: {
      english: 'Gyarados',
      japanese: 'ギャラドス',
      chinese: '暴鲤龙',
    },
    type: ['water', 'flying'],
    baseStats: {
      healthPoints: 95,
      attack: 125,
      defense: 79,
      specialAttack: 60,
      specialDefense: 100,
      speed: 81,
    },
    moves: [{
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thrash',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 24,
      },
    }, {
      name: 'Ice Fang',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 42,
      },
    }, {
      name: 'Dragon Dance',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 58,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Hyper Beam',
      requirements: {
        type: 'level',
        value: 54,
      },
    }],
    evolutions: [],
  }, {
    _id: 131,
    name: {
      english: 'Lapras',
      japanese: 'ラプラス',
      chinese: '拉普拉斯',
    },
    type: ['water', 'ice'],
    baseStats: {
      healthPoints: 130,
      attack: 85,
      defense: 80,
      specialAttack: 85,
      specialDefense: 95,
      speed: 60,
    },
    moves: [{
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sing',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mist',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Confuse Ray',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Ice Shard',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 14,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Perish Song',
      requirements: {
        type: 'level',
        value: 27,
      },
    }, {
      name: 'Ice Beam',
      requirements: {
        type: 'level',
        value: 32,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Sheer Cold',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [],
  }, {
    _id: 132,
    name: {
      english: 'Ditto',
      japanese: 'メタモン',
      chinese: '百变怪',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 48,
      attack: 48,
      defense: 48,
      specialAttack: 48,
      specialDefense: 48,
      speed: 48,
    },
    moves: [{
      name: 'Transform',
      requirements: {
        type: 'level',
        value: 1,
      },
    }],
    evolutions: [],
  }, {
    _id: 133,
    name: {
      english: 'Eevee',
      japanese: 'イーブイ',
      chinese: '伊布',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 55,
      attack: 55,
      defense: 50,
      specialAttack: 45,
      specialDefense: 65,
      speed: 55,
    },
    moves: [{
      name: 'Covet',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Growl',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Baby-Doll Eyes',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Refresh',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Charm',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Baton Pass',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Double-Edge',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Last Resort',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Trump Card',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [{
      number: 134,
      requirements: {
        type: 'stone',
        value: 'water',
      },
    },           {
      number: 135,
      requirements: {
        type: 'stone',
        value: 'thunder',
      },
    },           {
      number: 136,
      requirements: {
        type: 'stone',
        value: 'fire',
      },
    }],
  }, {
    _id: 134,
    name: {
      english: 'Vaporeon',
      japanese: 'シャワーズ',
      chinese: '水伊布',
    },
    type: ['water'],
    baseStats: {
      healthPoints: 130,
      attack: 65,
      defense: 60,
      specialAttack: 110,
      specialDefense: 95,
      speed: 65,
    },
    moves: [{
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Baby-Doll Eyes',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Water Pulse',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Aurora Beam',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Aqua Ring',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Acid Armor',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Haze',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Muddy Water',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Last Resort',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [],
  }, {
    _id: 135,
    name: {
      english: 'Jolteon',
      japanese: 'サンダース',
      chinese: '雷伊布',
    },
    type: ['electric'],
    baseStats: {
      healthPoints: 65,
      attack: 65,
      defense: 60,
      specialAttack: 110,
      specialDefense: 95,
      speed: 130,
    },
    moves: [{
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Baby-Doll Eyes',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Double Kick',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Thunder Fang',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Pin Missile',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Last Resort',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Thunder',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [],
  }, {
    _id: 136,
    name: {
      english: 'Flareon',
      japanese: 'ブースター',
      chinese: '火伊布',
    },
    type: ['fire'],
    baseStats: {
      healthPoints: 65,
      attack: 130,
      defense: 60,
      specialAttack: 95,
      specialDefense: 110,
      speed: 65,
    },
    moves: [{
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Helping Hand',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tail Whip',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Baby-Doll Eyes',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Quick Attack',
      requirements: {
        type: 'level',
        value: 13,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Smog',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Lava Plume',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Last Resort',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Flare Blitz',
      requirements: {
        type: 'level',
        value: 45,
      },
    }],
    evolutions: [],
  }, {
    _id: 137,
    name: {
      english: 'Porygon',
      japanese: 'ポリゴン',
      chinese: '多边兽',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 65,
      attack: 60,
      defense: 70,
      specialAttack: 85,
      specialDefense: 75,
      speed: 40,
    },
    moves: [{
      name: 'Conversion',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Conversion 2',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Sharpen',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Psybeam',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Recover',
      requirements: {
        type: 'level',
        value: 18,
      },
    }, {
      name: 'Magnet Rise',
      requirements: {
        type: 'level',
        value: 23,
      },
    }, {
      name: 'Signal Beam',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Recycle',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Lock-On',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Tri Attack',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Magic Coat',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Zap Cannon',
      requirements: {
        type: 'level',
        value: 62,
      },
    }],
    evolutions: [],
  }, {
    _id: 138,
    name: {
      english: 'Omanyte',
      japanese: 'オムナイト',
      chinese: '菊石兽',
    },
    type: ['rock', 'water'],
    baseStats: {
      healthPoints: 35,
      attack: 40,
      defense: 100,
      specialAttack: 90,
      specialDefense: 55,
      speed: 35,
    },
    moves: [{
      name: 'Constrict',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Tickle',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Shell Smash',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 55,
      },
    }],
    evolutions: [{
      number: 139,
      requirements: {
        type: 'level',
        value: 40,
      },
    }],
  }, {
    _id: 139,
    name: {
      english: 'Omastar',
      japanese: 'オムスター',
      chinese: '多刺菊石兽',
    },
    type: ['rock', 'water'],
    baseStats: {
      healthPoints: 70,
      attack: 60,
      defense: 125,
      specialAttack: 115,
      specialDefense: 70,
      speed: 55,
    },
    moves: [{
      name: 'Constrict',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Withdraw',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Spike Cannon',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 7,
      },
    }, {
      name: 'Water Gun',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 19,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Brine',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Protect',
      requirements: {
        type: 'level',
        value: 34,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 37,
      },
    }, {
      name: 'Tickle',
      requirements: {
        type: 'level',
        value: 48,
      },
    }, {
      name: 'Rock Blast',
      requirements: {
        type: 'level',
        value: 56,
      },
    }, {
      name: 'Shell Smash',
      requirements: {
        type: 'level',
        value: 67,
      },
    }, {
      name: 'Hydro Pump',
      requirements: {
        type: 'level',
        value: 75,
      },
    }],
    evolutions: [],
  }, {
    _id: 140,
    name: {
      english: 'Kabuto',
      japanese: 'カブト',
      chinese: '化石盔',
    },
    type: ['rock', 'water'],
    baseStats: {
      healthPoints: 30,
      attack: 80,
      defense: 90,
      specialAttack: 55,
      specialDefense: 45,
      speed: 55,
    },
    moves: [{
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Endure',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Aqua Jet',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Mega Drain',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Metal Sound',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 46,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 50,
      },
    }],
    evolutions: [{
      number: 141,
      requirements: {
        type: 'level',
        value: 40,
      },
    }],
  }, {
    _id: 141,
    name: {
      english: 'Kabutops',
      japanese: 'カブトプス',
      chinese: '镰刀盔',
    },
    type: ['rock', 'water'],
    baseStats: {
      healthPoints: 60,
      attack: 115,
      defense: 105,
      specialAttack: 65,
      specialDefense: 70,
      speed: 80,
    },
    moves: [{
      name: 'Harden',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scratch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Feint',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Slash',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Absorb',
      requirements: {
        type: 'level',
        value: 6,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Mud Shot',
      requirements: {
        type: 'level',
        value: 16,
      },
    }, {
      name: 'Sand Attack',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Endure',
      requirements: {
        type: 'level',
        value: 26,
      },
    }, {
      name: 'Aqua Jet',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Mega Drain',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Metal Sound',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 54,
      },
    }, {
      name: 'Wring Out',
      requirements: {
        type: 'level',
        value: 63,
      },
    }, {
      name: 'Night Slash',
      requirements: {
        type: 'level',
        value: 72,
      },
    }],
    evolutions: [],
  }, {
    _id: 142,
    name: {
      english: 'Aerodactyl',
      japanese: 'プテラ',
      chinese: '化石翼龙',
    },
    type: ['rock', 'flying'],
    baseStats: {
      healthPoints: 80,
      attack: 105,
      defense: 65,
      specialAttack: 60,
      specialDefense: 75,
      speed: 130,
    },
    moves: [{
      name: 'Bite',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fire Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Ice Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Scary Face',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Supersonic',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Fang',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Roar',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Take Down',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Sky Drop',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Iron Head',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Hyper Beam',
      requirements: {
        type: 'level',
        value: 65,
      },
    }, {
      name: 'Rock Slide',
      requirements: {
        type: 'level',
        value: 73,
      },
    }, {
      name: 'Giga Impact',
      requirements: {
        type: 'level',
        value: 81,
      },
    }],
    evolutions: [],
  }, {
    _id: 143,
    name: {
      english: 'Snorlax',
      japanese: 'カビゴン',
      chinese: '卡比兽',
    },
    type: ['normal'],
    baseStats: {
      healthPoints: 160,
      attack: 110,
      defense: 65,
      specialAttack: 65,
      specialDefense: 110,
      speed: 30,
    },
    moves: [{
      name: 'Tackle',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Defense Curl',
      requirements: {
        type: 'level',
        value: 4,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 9,
      },
    }, {
      name: 'Lick',
      requirements: {
        type: 'level',
        value: 12,
      },
    }, {
      name: 'Chip Away',
      requirements: {
        type: 'level',
        value: 17,
      },
    }, {
      name: 'Yawn',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Body Slam',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Rest',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Snore',
      requirements: {
        type: 'level',
        value: 28,
      },
    }, {
      name: 'Sleep Talk',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Giga Impact',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Rollout',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Block',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Belly Drum',
      requirements: {
        type: 'level',
        value: 44,
      },
    }, {
      name: 'Crunch',
      requirements: {
        type: 'level',
        value: 49,
      },
    }, {
      name: 'Heavy Slam',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'High Horsepower',
      requirements: {
        type: 'level',
        value: 57,
      },
    }],
    evolutions: [],
  }, {
    _id: 144,
    name: {
      english: 'Articuno',
      japanese: 'フリーザー',
      chinese: '急冻鸟',
    },
    type: ['ice', 'flying'],
    baseStats: {
      healthPoints: 90,
      attack: 85,
      defense: 100,
      specialAttack: 95,
      specialDefense: 125,
      speed: 85,
    },
    moves: [{
      name: 'Gust',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Powder Snow',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mist',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Ice Shard',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Mind Reader',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Freeze-Dry',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Reflect',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Hail',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Tailwind',
      requirements: {
        type: 'level',
        value: 64,
      },
    }, {
      name: 'Ice Beam',
      requirements: {
        type: 'level',
        value: 71,
      },
    }, {
      name: 'Blizzard',
      requirements: {
        type: 'level',
        value: 78,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 85,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 92,
      },
    }, {
      name: 'Sheer Cold',
      requirements: {
        type: 'level',
        value: 99,
      },
    }],
    evolutions: [],
  }, {
    _id: 145,
    name: {
      english: 'Zapdos',
      japanese: 'サンダー',
      chinese: '闪电鸟',
    },
    type: ['electric', 'flying'],
    baseStats: {
      healthPoints: 90,
      attack: 90,
      defense: 85,
      specialAttack: 125,
      specialDefense: 90,
      speed: 100,
    },
    moves: [{
      name: 'Peck',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Shock',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Detect',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Pluck',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Charge',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Discharge',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Rain Dance',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Light Screen',
      requirements: {
        type: 'level',
        value: 64,
      },
    }, {
      name: 'Drill Peck',
      requirements: {
        type: 'level',
        value: 71,
      },
    }, {
      name: 'Thunder',
      requirements: {
        type: 'level',
        value: 78,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 85,
      },
    }, {
      name: 'Magnetic Flux',
      requirements: {
        type: 'level',
        value: 92,
      },
    }, {
      name: 'Zap Cannon',
      requirements: {
        type: 'level',
        value: 99,
      },
    }],
    evolutions: [],
  }, {
    _id: 146,
    name: {
      english: 'Moltres',
      japanese: 'ファイヤー',
      chinese: '火焰鸟',
    },
    type: ['fire', 'flying'],
    baseStats: {
      healthPoints: 90,
      attack: 100,
      defense: 90,
      specialAttack: 125,
      specialDefense: 85,
      speed: 90,
    },
    moves: [{
      name: 'Ember',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Fire Spin',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Endure',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Flamethrower',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Air Slash',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Sunny Day',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Heat Wave',
      requirements: {
        type: 'level',
        value: 64,
      },
    }, {
      name: 'Solar Beam',
      requirements: {
        type: 'level',
        value: 71,
      },
    }, {
      name: 'Sky Attack',
      requirements: {
        type: 'level',
        value: 78,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 85,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 92,
      },
    }, {
      name: 'Burn Up',
      requirements: {
        type: 'level',
        value: 99,
      },
    }],
    evolutions: [],
  }, {
    _id: 147,
    name: {
      english: 'Dratini',
      japanese: 'ミニリュウ',
      chinese: '迷你龙',
    },
    type: ['dragon'],
    baseStats: {
      healthPoints: 41,
      attack: 64,
      defense: 45,
      specialAttack: 50,
      specialDefense: 50,
      speed: 50,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Dragon Tail',
      requirements: {
        type: 'level',
        value: 31,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 35,
      },
    }, {
      name: 'Dragon Rush',
      requirements: {
        type: 'level',
        value: 41,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 45,
      },
    }, {
      name: 'Dragon Dance',
      requirements: {
        type: 'level',
        value: 51,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 55,
      },
    }, {
      name: 'Hyper Beam',
      requirements: {
        type: 'level',
        value: 61,
      },
    }],
    evolutions: [{
      number: 148,
      requirements: {
        type: 'level',
        value: 30,
      },
    }],
  }, {
    _id: 148,
    name: {
      english: 'Dragonair',
      japanese: 'ハクリュー',
      chinese: '哈克龙',
    },
    type: ['dragon'],
    baseStats: {
      healthPoints: 61,
      attack: 84,
      defense: 65,
      specialAttack: 70,
      specialDefense: 70,
      speed: 70,
    },
    moves: [{
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Dragon Tail',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Dragon Rush',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Dragon Dance',
      requirements: {
        type: 'level',
        value: 61,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 67,
      },
    }, {
      name: 'Hyper Beam',
      requirements: {
        type: 'level',
        value: 75,
      },
    }],
    evolutions: [{
      number: 149,
      requirements: {
        type: 'level',
        value: 55,
      },
    }],
  }, {
    _id: 149,
    name: {
      english: 'Dragonite',
      japanese: 'カイリュー',
      chinese: '快龙',
    },
    type: ['dragon', 'flying'],
    baseStats: {
      healthPoints: 91,
      attack: 134,
      defense: 95,
      specialAttack: 100,
      specialDefense: 100,
      speed: 80,
    },
    moves: [{
      name: 'Wrap',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Leer',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Roost',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Punch',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Wing Attack',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Thunder Wave',
      requirements: {
        type: 'level',
        value: 5,
      },
    }, {
      name: 'Twister',
      requirements: {
        type: 'level',
        value: 11,
      },
    }, {
      name: 'Dragon Rage',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Slam',
      requirements: {
        type: 'level',
        value: 21,
      },
    }, {
      name: 'Agility',
      requirements: {
        type: 'level',
        value: 25,
      },
    }, {
      name: 'Dragon Tail',
      requirements: {
        type: 'level',
        value: 33,
      },
    }, {
      name: 'Aqua Tail',
      requirements: {
        type: 'level',
        value: 39,
      },
    }, {
      name: 'Dragon Rush',
      requirements: {
        type: 'level',
        value: 47,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 53,
      },
    }, {
      name: 'Dragon Dance',
      requirements: {
        type: 'level',
        value: 61,
      },
    }, {
      name: 'Outrage',
      requirements: {
        type: 'level',
        value: 67,
      },
    }, {
      name: 'Hyper Beam',
      requirements: {
        type: 'level',
        value: 75,
      },
    }, {
      name: 'Hurricane',
      requirements: {
        type: 'level',
        value: 81,
      },
    }],
    evolutions: [],
  }, {
    _id: 150,
    name: {
      english: 'Mewtwo',
      japanese: 'ミュウツー',
      chinese: '超梦',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 106,
      attack: 110,
      defense: 90,
      specialAttack: 154,
      specialDefense: 90,
      speed: 130,
    },
    moves: [{
      name: 'Confusion',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Disable',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Laser Focus',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Psywave',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Safeguard',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Swift',
      requirements: {
        type: 'level',
        value: 8,
      },
    }, {
      name: 'Future Sight',
      requirements: {
        type: 'level',
        value: 15,
      },
    }, {
      name: 'Psych Up',
      requirements: {
        type: 'level',
        value: 22,
      },
    }, {
      name: 'Miracle Eye',
      requirements: {
        type: 'level',
        value: 29,
      },
    }, {
      name: 'Psycho Cut',
      requirements: {
        type: 'level',
        value: 36,
      },
    }, {
      name: 'Guard Swap',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Power Swap',
      requirements: {
        type: 'level',
        value: 43,
      },
    }, {
      name: 'Recover',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 57,
      },
    }, {
      name: 'Barrier',
      requirements: {
        type: 'level',
        value: 64,
      },
    }, {
      name: 'Aura Sphere',
      requirements: {
        type: 'level',
        value: 70,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 79,
      },
    }, {
      name: 'Mist',
      requirements: {
        type: 'level',
        value: 86,
      },
    }, {
      name: 'Me First',
      requirements: {
        type: 'level',
        value: 93,
      },
    }, {
      name: 'Psystrike',
      requirements: {
        type: 'level',
        value: 100,
      },
    }],
    evolutions: [],
  }, {
    _id: 151,
    name: {
      english: 'Mew',
      japanese: 'ミュウ',
      chinese: '梦幻',
    },
    type: ['psychic'],
    baseStats: {
      healthPoints: 100,
      attack: 100,
      defense: 100,
      specialAttack: 100,
      specialDefense: 100,
      speed: 100,
    },
    moves: [{
      name: 'Pound',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Reflect Type',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Transform',
      requirements: {
        type: 'level',
        value: 1,
      },
    }, {
      name: 'Mega Punch',
      requirements: {
        type: 'level',
        value: 10,
      },
    }, {
      name: 'Metronome',
      requirements: {
        type: 'level',
        value: 20,
      },
    }, {
      name: 'Psychic',
      requirements: {
        type: 'level',
        value: 30,
      },
    }, {
      name: 'Barrier',
      requirements: {
        type: 'level',
        value: 40,
      },
    }, {
      name: 'Ancient Power',
      requirements: {
        type: 'level',
        value: 50,
      },
    }, {
      name: 'Amnesia',
      requirements: {
        type: 'level',
        value: 60,
      },
    }, {
      name: 'Me First',
      requirements: {
        type: 'level',
        value: 70,
      },
    }, {
      name: 'Baton Pass',
      requirements: {
        type: 'level',
        value: 80,
      },
    }, {
      name: 'Nasty Plot',
      requirements: {
        type: 'level',
        value: 90,
      },
    }, {
      name: 'Aura Sphere',
      requirements: {
        type: 'level',
        value: 100,
      },
    }],
    evolutions: [],
  },
];
