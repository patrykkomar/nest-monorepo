import { LocationName } from '../../../shared/enums';
import { Location } from '../../../shared/interfaces';

export const locations: Location[] = [
  {
    name: LocationName.ACCUMULA_TOWN,
    width: 100,
    height: 100,
    neighhbours: [],
  },
  {
    name: LocationName.AMBRETTE_TOWN,
    width: 100,
    height: 100,
    neighhbours: [],
  },
  {
    name: LocationName.ANISTAR_CITY,
    width: 100,
    height: 100,
    neighhbours: [],
  },
  {
    name: LocationName.ANIVILLE_TOWN,
    width: 100,
    height: 100,
    neighhbours: [],
  },
];
