import { Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';

import { JwtManager, KeyValue, RedisService } from '@nestjs-monorepo/common';

import { config } from '../../setup/config';
import { logger } from '../../shared/helpers';
import { AuthenticatedSockets, CurrentPosition, PlayersPositions, PlayersTrainersObjects } from '../../shared/interfaces';
import { UserService, TrainerService } from '../database/services';
import { AuthenticationRequest } from './websocket.gateway';



@Injectable()
export class WebsocketService {
  private redisService: RedisService;
  private authenticatedSockets: AuthenticatedSockets;
  private lastSavedPlayersPositions: PlayersPositions;
  private playersPositions: PlayersPositions;
  private positionUpdateTimeouts: KeyValue;
  private playersTrainersObjects: PlayersTrainersObjects;

  constructor(private readonly jwtManager: JwtManager, private readonly userService: UserService, private readonly trainerService: TrainerService) {
    this.authenticatedSockets = {};
    this.lastSavedPlayersPositions = {};
    this.playersPositions = {};
    this.positionUpdateTimeouts = {};
    this.playersTrainersObjects = {};
    if (config.redis) {
      this.redisService = new RedisService(config.redis);
    }
  }

  async handleAuthenticationRequest({ token, trainerId }: AuthenticationRequest, client: Socket): Promise<CurrentPosition> {
    // let tokenIsValid = this.jwtManager.verifyToken(token, config.jwt.secret);

    // if (tokenIsValid) {
      const userId = this.userService.getUserIdFromAuthorizationToken(token);
      const user = await this.userService.findUserById(userId);
      if (user && user.trainersIds.includes(trainerId)) {
        this.authenticatedSockets[client.id] = trainerId;
        const trainer = await this.trainerService.findTrainerById(trainerId);
        this.lastSavedPlayersPositions[trainerId] = trainer.currentPosition;
        this.playersPositions[trainerId] = trainer.currentPosition;
        this.playersTrainersObjects[trainerId] = await this.trainerService.findTrainerById(trainerId);

        return trainer.currentPosition;
      }
    // }

    return null;
  }

  async handlePositionUpdate(newPosition: CurrentPosition, client: Socket): Promise<void> {
    const socketId = client.id;
    const trainerId = this.authenticatedSockets[socketId];
    if (trainerId) {
      if (this.positionUpdateTimeouts[trainerId]) {
        // console.log('Jest już ustawiony tajmałt, trza usunąć ^^');
        clearTimeout(this.positionUpdateTimeouts[trainerId] as NodeJS.Timeout);
      }

      this.positionUpdateTimeouts[trainerId] = setTimeout(async () => {
        if (newPosition.locationId != this.lastSavedPlayersPositions[trainerId].locationId || newPosition.x !== this.lastSavedPlayersPositions[trainerId].x || newPosition.y !== this.lastSavedPlayersPositions[trainerId].y) {
          await this.trainerService.updateTrainerPosition(trainerId, newPosition);
          this.lastSavedPlayersPositions[trainerId] = newPosition;
        }
      }, 10000);
    }

    return;
  }

  handleClientDisconnect(client: Socket): void {
    const socketId = client.id;
    const trainerId = this.authenticatedSockets[socketId];
    if (trainerId) {
      delete this.authenticatedSockets[socketId];
      delete this.lastSavedPlayersPositions[trainerId];
      delete this.playersPositions[trainerId];
    }
  }
}
