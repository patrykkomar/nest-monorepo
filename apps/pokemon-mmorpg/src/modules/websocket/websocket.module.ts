import { Module } from '@nestjs/common';

import { JwtManager } from '@nestjs-monorepo/common';

import { DatabaseModule } from '../database/database.module';
import { WebsocketGateway } from './websocket.gateway';
import { WebsocketService } from './websocket.service';

@Module({
  imports: [DatabaseModule],
  providers: [JwtManager, WebsocketService, WebsocketGateway],
})
export class WebsocketModule {}
