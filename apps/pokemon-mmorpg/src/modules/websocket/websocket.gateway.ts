import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
  OnGatewayDisconnect,
  OnGatewayConnection,
  MessageBody,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Types } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Server, Socket } from 'socket.io';

import { JwtManager } from '@nestjs-monorepo/common';

import { logger } from '../../shared/helpers';
import { CurrentPosition } from '../../shared/interfaces';
// import { UserService } from '../database/services';
import { WebsocketService } from './websocket.service';

export interface AuthenticationRequest {
  token: string;
  trainerId: string | Types.ObjectId;
}

@WebSocketGateway()
export class WebsocketGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  constructor(private readonly jwtManager: JwtManager, private readonly websocketService: WebsocketService) {}

  // @SubscribeMessage('events')
  // findAll(
  //   @MessageBody() data: string,
  //   @ConnectedSocket() client: Socket,
  // ): Observable<WsResponse<number>> {
  //   return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  // }

  @SubscribeMessage('authentication')
  async handleAuthenticationRequest(@MessageBody() authenticationRequest: AuthenticationRequest, @ConnectedSocket() client: Socket): Promise<CurrentPosition> {
    return this.websocketService.handleAuthenticationRequest(authenticationRequest, client);
  }

  @SubscribeMessage('position')
  async handlePositionUpdate(@MessageBody() newPosition: CurrentPosition, @ConnectedSocket() client: Socket): Promise<void> {
    await this.websocketService.handlePositionUpdate(newPosition, client);
  }

  handleConnection(client: Socket): void {
    logger.info('Websocket client connected.', { id: client.id, openConnections: this.server.listenerCount });
  }

  handleDisconnect(client: Socket): void {
    logger.info('Websocket client disconnected.', { id: client.id, openConnections: this.server.listenerCount });
  }

  // broadcastSpotPrice(): void {
  //   for (const client of this.server.clients) {
  //     client.send('????');
  //   }
  // }
}
