import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as path from 'path';

export enum Environment {
  DEVELOPMENT = 'development',
  PRODUCTION = 'production',
  TEST = 'test',
}

interface Jwt {
  secret: string;
  expiresIn: number;
  expirationRefresh: number;
}

interface Mongo {
  url: string;
}

interface Redis {
  host: string;
  port: number;
}

export interface Config {
  environment: Environment;
  jwt: Jwt;
  mongo: Mongo;
  redis: Redis;
}

const mainConfigFilePath = path.join(__dirname, '../../../resources/pokemon-mmorpg/config.yaml');
const testConfigFilePath = path.join(__dirname, '../../tests/test-config.yaml');

export const config: Config = fs.existsSync(mainConfigFilePath)
  ? yaml.safeLoad(fs.readFileSync(mainConfigFilePath))
  : yaml.safeLoad(fs.readFileSync(testConfigFilePath));

