import { Document, Types } from 'mongoose';

import { Class, Gender, LocationName, WildPokemonStatus } from './enums';

/* Mongo models */
export interface Name {
  english: string;
  japanese: string;
  chinese: string;
}

export interface User extends Document {
  userId?: string | Types.ObjectId;
  email: string;
  password?: string;
  trainersIds?: (string | Types.ObjectId)[];
  communityMember?: boolean;
  referralId?: string | Types.ObjectId;
  registeredAt?: Date | number;
  lastLoginAt?: Date | number;
}

export interface CurrentPosition {
  locationId: string | Types.ObjectId;
  x: number;
  y: number;
}

export interface Trainer extends Document {
  trainerId?: string | Types.ObjectId;
  name: string;
  userId: string | Types.ObjectId;
  gender: Gender;
  class: Class;
  level: number;
  experience: number;
  nextLevelExperience: number;
  pokemonsInPocket: (string | Types.ObjectId)[];
  pokemonsInBackpack: (string | Types.ObjectId)[];
  pokemonsInCenter: (string | Types.ObjectId)[];
  inventory: (string | Types.ObjectId)[];
  currentPosition: CurrentPosition;
  createdAt?: Date | number;
}

export interface LocationNeightbour {
  x: number;
  y: number;
  locationId: string | Types.ObjectId;
}

export interface Location extends Document {
  name: LocationName;
  width: number;
  height: number;
  neighhbours: LocationNeightbour[];
}

export interface WildPokemon extends Document {
  pokemonId: string | Types.ObjectId;
  gender: Gender;
  level: number;
  status: WildPokemonStatus;
  position: CurrentPosition;
  spawnedAt: Date | number;
  defeatedAt?: Date | number;
}

export interface OwnedPokemon extends Document {
  pokemonId: number;
  trainerId: string | Types.ObjectId;
  gender: Gender;
  level: number;
  experience: number;
  /* TODO: nextLevelExperience - different than trainers' ones */
  stats: BaseStats;
  health: number;
  stamina: number;
  caughtAt: Date | number;
}

export interface PokemonName {
  english: string;
  japanese: string;
  chinese: string;
}

export interface BaseStats {
  healthPoints: number;
  attack: number;
  defense: number;
  specialAttack: number;
  specialDefense: number;
  speed: number;
}

export interface PokemonMoveRequirements {
  type: string;
  value: number;
}

export interface PokemonMove {
  name: string;
  requirements: PokemonMoveRequirements;
}

export interface PokemonEvolutionRequirements {
  type: string;
  value: number | string;
}

export interface PokemonEvolution {
  number: number;
  requirements: PokemonEvolutionRequirements;
}

export interface Pokemon extends Document {
  _id?: number;
  number?: number;
  name: PokemonName;
  type: string[];
  baseStats: BaseStats;
  moves: PokemonMove[];
  evolutions: PokemonEvolution[];
}

export interface AuthenticatedSockets {
  [key: string]: string;
}

export interface PlayersPositions {
  [key: string]: CurrentPosition;
}

export interface PlayersTrainersObjects {
  [key: string]: Partial<Trainer>;
}
